<?php
// var_dump($Commande_entetes_client);
// require_once(rot . ds . 'util/fonction_util.php');
// $fonction_utile = new Fonction_util();
// var_dump($fonction_utile->get_date_now__tiree('-'));
// var_dump($fonction_utile->convert_date_to_FR_DMY_heurs__slash($fonction_utile->get_date_now__tiree('-'),'/'));
// var_dump($fonction_utile->convert_date_to_DM($fonction_utile->get_date_now__tiree('-'),'/'));
// var_dump($fonction_utile->convert_date_to_MY($fonction_utile->get_date_now__tiree('-'),'/'));
// var_dump($fonction_utile->convert_date_to_Y($fonction_utile->get_date_now__tiree('-'),'/'));

$cmd_date = new DateTime($Commande_entetes_client['cmd_date']);
$cmd_date = date_format($cmd_date, "d/m/Y H:i:s");
$client_nom_prenom = isset($Commande_entetes_client['client_nom']) ? $Commande_entetes_client['client_nom'] . ' ' . $Commande_entetes_client['client_prenom'] : '';
$adress_client = isset($Commande_entetes_client['client_adress']) ? $Commande_entetes_client['client_adress'] : '';
?>
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header text-uppercase <?php echo $Commande_entetes_client['cmd_valide'] == 0 ? 'text-warning' : 'text-success' ?> ">DETAILLE COMMANDE
                <?php echo $Commande_entetes_client['cmd_num'] ?> </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">

                        <tbody>
                            <tr>
                                <th style="width: 5%;" scope="row">Numero commande :</th>
                                <td><?php echo $Commande_entetes_client['cmd_num'] ?></td>
                                <th style="width: 5%;" scope="row">Client :</th>
                                <td><?php echo $client_nom_prenom ?></td>
                            </tr>
                            <tr>
                                <th style="width: 5%;" scope="row">Date :</th>
                                <td><?php echo $cmd_date ?></td>
                                <th style="width: 5%;" scope="row">Adresse</th>
                                <td><?php echo $adress_client ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead class="<?php echo $Commande_entetes_client['cmd_valide'] == 0 ? 'thead-warning' : 'thead-success' ?>">
                            <tr>
                                <th scope="col">Numero</th>
                                <th scope="col">Produit</th>
                                <th scope="col">Unite mesure</th>
                                <th scope="col">Prix unitaire</th>
                                <th scope="col">Quantite</th>
                                <th scope="col">Prix emballage</th>
                                <th scope="col">Sous total emballage</th>
                                <th scope="col">Sous Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $total = 0.00;
                            $total_emballage = 0.00;
                            foreach ($commande_entetes as $key => $cmd) {
                                $this->loadModel('Produit_unitme');
                                $this->loadModel('Unitemesure');
                                $unite_mes_prod = $this->Produit_unitme->get_Unite_Mesure_by_id_produit_To_Id($cmd['prod_id'], $cmd['unit_mes_id']);
                                $mesure = $this->Unitemesure->get_Unite_Mesure_by_id($cmd['unit_mes_id']);
                                $total = $total + $cmd['cmd_line_montant'];
                                $total_emballage = $total_emballage + $cmd['cmd_line_montant_emballage'];
                            ?>
                                <tr>
                                    <th scope="row"><?php echo $key + 1 ?></th>
                                    <td><?php echo $unite_mes_prod['prod_libell'] ?></td>
                                    <td><?php echo $mesure['unite_mesure_code'] ?></td>
                                    <td><?php echo $cmd['cmd_line_prix_unite'] ?> Ar</td>
                                    <td><?php echo $cmd['cmd_line_qte'] ?></td>
                                    <td><?php echo $cmd['cmd_line_prix_emballage'] ?> Ar</td>
                                    <td><?php echo $cmd['cmd_line_montant_emballage'] ?> Ar</td>
                                    <td><?php echo $cmd['cmd_line_montant'] ?> Ar</td>
                                </tr>
                            <?php
                            }
                            ?>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>Total:<?php echo $total_emballage ?> Ar</td>
                                <td>Total:<?php echo $total ?> Ar</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


    <div class="col-lg-12 ">
        <div class="card border border-info w-100">
            <div class="card-body text-center p-5 d-flex justify-content-between">
                <button type="button" class="btn btn-primary shadow-primary waves-effect waves-light m-1">PRIMARY</button>
                <button type="button" class="btn btn-danger shadow-danger waves-effect waves-light m-1">Suprimer</button>
                <button type="button" data-id="<?php echo $Commande_entetes_client['cmd_id'] ?>" <?php echo $Commande_entetes_client['cmd_valide'] == 0 ? '' : 'disabled' ?> class="btn btn-success shadow-success waves-effect waves-light m-1 kl-valide-commande-client_by_id-cmd">Valide</button>
                <button type="button" class="btn btn-info shadow-info waves-effect waves-light m-1">Modifier</button>
                <button type="button" class="btn btn-warning shadow-warning waves-effect waves-light m-1">WARNING</button>
                <button type="button" data-id="<?php echo $Commande_entetes_client['cmd_id'] ?>" class="btn btn-dark shadow-dark waves-effect waves-light m-1 kl-pdf">pdf</button>
                <button type="button" class="btn btn-secondary shadow-secondary waves-effect waves-light m-1">SECONDARY</button>
            </div>
        </div>


    </div>

</div>
<script src="<?php echo URL; ?>/assets/js/jquery.min.js"></script>
<script>
    var url_commande_tete = "<?php echo parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH); ?>";
</script>
<script>
    $(document).ready(function() {

        var mes = [];
        $(".kl-valide-commande-client_by_id-cmd").click(function() {
            var id = $(this).data('id');
            var data = {};
            data.cmd_id = id;
            data.action = 'valide_on_cmd_client';
            swal({
                    title: "Vous êtes sure de validé cette commande ?",
                    text: "La validation modifie l'état du stock",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        var res = '';
                        $.ajax({
                            url: url_commande_tete,
                            data,
                            method: "POST",
                            dataType: "JSON",
                            beforeSend: function() {},
                            success: function(response) {
                                res = response;
                                console.log(res['status']);
                                location.reload();
                                // mes['res'] = response;
                            }
                        });

                        swal("Validation commande success", {
                            icon: "success",
                            class: '',
                        });

                    } else {
                        swal("Validation annulé!");
                    }
                });

            // console.log(mes);
        });

        $('.kl-pdf').click(function(e) {
            var data = {};
            var id = $(this).data('id');
            data.action = 'pdf';
            data.id_cmd = id;
            $.ajax({
                url: url_commande_tete,
                data,
                method: "POST",
                dataType: "JSON",
                beforeSend: function() {},
                success: function(response) {
                    res = response;
                    console.log(res);
                    // mes['res'] = response;
                }
            });
        });
    });
</script>