<?php
require_once(rot . ds . 'app/Controller.php');
$this->loadModel('client');
$this->loadModel('Produit_unitme');
$clients = $this->client->getAll();
$produits = $this->Produit_unitme->get_all_produit_distict();

?>
<style>
    .kl-btn-add-to-card {
        margin-top: 30px !important;
    }
</style>
<form id="form-mesure_conversion" action="" method="POST">

</form>

<style>
    .wizard.vertical>.content {
        display: inline;
        float: left;
        margin: 0 2.5% 0.5em 2.5%;
        width: 76% !important;
    }

    .wizard.vertical>.steps {
        display: inline;
        float: left;
        width: 18%;
    }
</style>

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header text-uppercase">
                Ajouter nouveau commande client
            </div>
            <div class="card-body">
                <div id="wizard-vertical">
                    <h3>Client</h3>
                    <section>
                        <div class="row">
                            <!-- <button style="float: right;" type="button" class="btn btn-dark shadow-dark waves-effect waves-light m-1">Ajouter nouveau client</button> -->
                        </div>
                        <div class="row mb-3">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>Selectionner le Client</label>
                                    <select class="form-control single-select" id="ID_CLIENT_ID" name="CLIENT_ID">
                                        <option value="0"></option>
                                        <?php foreach ($clients as $client) { ?>
                                            <option value="<?php echo $client['client_id']; ?>"><?php echo $client['client_nom'] . ' ' . $client['client_prenom']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </section>
                    <h3>Produit</h3>
                    <section>
                        <div class="row mb-3">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Produit</label>
                                            <select class="form-control single-select" id="ID_PRODUIT_ID" name="PRODUIT_ID">
                                                <option value="0"></option>
                                                <?php foreach ($produits as $produit) { ?>
                                                    <option value="<?php echo $produit['prod_id']; ?>"><?php echo $produit['prod_libell']; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6" id="id-content-list-unite-mesure">
                                        <div class="form-group">
                                            <label>Unité de mesure correspondant</label>
                                            <select class="form-control single-select" id="ID_UNITEMESURE_ID" name="unite_mesure_id" required>
                                                <option value=""></option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4 kl-qte-restant-produit">
                                        <div class="form-group">
                                            <label>Quantité en stock</label>
                                            <input type="number" id="id-qte-en-stock" class="kl-input_decimal" step="any" value="0.00" disabled="disabled" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>QUANTITE commande</label>
                                            <input id="ID_Quantity_Produit_VALUE" class="kl-input_decimal" type="number" step="any" min='1' value="1.00" name="Quantity_Produit_VALUE" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group kl-btn-form-default">
                                            <button type="submit" name="btn_save_mesure_conversion" class="btn btn-warning shadow-primary w-100 mt-4 px-5 kl-btn-add-to-card"><i class="icon-plus"></i>Ajouter</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /**liste produit selectionnner */ -->
                        <div class="table-responsive kl-content-liste-select-cmd">

                        </div>
                    </section>


                    <h3>Deconsignation</h3>
                    <section>
                        <div class="form-group">
                            <div class="col-lg-12">
                                <!-- Vidiny cageot sy... CONS -->
                                <div class="row">
                                    <div class="col-lg-7">
                                        <div class="form-group">
                                            <label>Avoir(Deconsignation)</label>
                                            <select class="form-control single-select" id="id_consignation" name="consignation">
                                                <option value="0"></option>
                                                <?php foreach ($produits as $produit) { ?>
                                                    <option value="<?php echo $produit['prod_id']; ?>"><?php echo $produit['prod_libell']; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>Quantite</label>
                                            <input id="id_consignation" class="kl-input_decimal" type="number" step="any" min='1' value="1.00" name="Quantity_consignation" required>
                                        </div>
                                    </div>

                                    <div class="col-lg-1">
                                    <button type="button" class="btn btn-info shadow-info waves-effect waves-light m-1 mt-4">Ajouter</button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </section>

                    <!-- <h3>Deconsignation</h3>
                    <section>
                        <div class="form-group">
                            <div class="col-lg-12">
                                mitondra cageot ou (AVOIR)
                            </div>
                        </div>
                    </section> -->

                    <h3>Validation</h3>
                    <section>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="table-responsive kl-content-liste-select-cmd">

                                </div>
                            </div>

                            <div class="col-lg-6">
                                <button type="submit" name="btn_save_mesure_conversion" class="btn btn-primary shadow-primary px-5 kl-save-all-cmd-selected"></i>Enregistrer commande</button>
                            </div>
                            <div class="col-lg-6">
                                <button style="float: right;" type="submit" name="btn_save_mesure_conversion" class="btn btn-danger shadow-primary px-5 kl-annule-all-cmd-selected"></i>Annuler commande</button>

                            </div>
                        </div>

                    </section>
                    <!-- <h3>Finish</h3>
                    <section>
                        <div class="form-group">
                            <div class="col-lg-12">
                                <div class="checkbox checkbox-primary">
                                    <input id="checkbox-v" type="checkbox">
                                    <label for="checkbox-v">
                                        I agree with the Terms and Conditions.
                                    </label>
                                </div>
                            </div>
                        </div>
                    </section> -->

                </div> <!-- End #wizard-vertical -->
            </div>
        </div>
    </div>
</div><!-- End Row-->


<?php
// var_dump($this->object_cmd);