<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header"><i class="fa fa-table">
                </i>Liste des commandes</div>
            <div class="row kl-action-button d-none " style="right: 42px">
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table id="example" class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Numero</th>
                                <th>Client</th>
                                <th>Date</th>
                                <th>Validé</th>
                                <th>Action</th>
                        </thead>
                        <tbody>
                            <?php foreach ($commande_entetes as $commande_entete) {
                                $this->loadModel('client');
                                $clients = $this->client->get_cli($commande_entete['client_id']);
                                $date = new DateTime($commande_entete['cmd_date']);
                                $date = date_format($date, "d/m/Y H:i:s")
                            ?>

                                <tr>
                                    <td><?php echo $commande_entete['cmd_num']; ?></td>
                                     <td><?php echo  $clients['client_nom'].' '.$clients['client_prenom'] ?></td>
                                    <td><?php echo $date; ?></td>
                                   
                                    <td>
                                        <div class="<?php echo $commande_entete['cmd_valide'] == 0 ? 'icheck-material-danger' : 'icheck-material-success '; ?> text-center">
                                            <input type="checkbox" disabled checked="">
                                            <label for="success"></label>
                                        </div>

                                    </td>
                                    <td>
                                    
                                        <a  href="/commande_entetes/detaille_cmd_ligne?cmd_id=<?php echo $commande_entete['cmd_id'] ?>" type="button" data-toggle="tooltip" data-placement="top" title="Voir détaille" data-original-title="Tooltip on top" class="btn btn-warning waves-effect waves-light m-1"> <i class="zmdi zmdi-eye"></i> <span></span> </a>
                                        <button type="button" data-toggle="tooltip" data-placement="top" title="Editer" data-original-title="Tooltip on top" class="btn btn-success waves-effect waves-light m-1"> <i class="zmdi zmdi-edit"></i> <span></span> </button>
                                        <button type="button" data-toggle="tooltip" data-placement="top" title="Suprimer" data-original-title="Tooltip on top" class="btn btn-danger waves-effect waves-light m-1"> <i class="fa fa fa-trash-o"></i> <span></span> </button>
                                    </td>
                                </tr>
                            <?php }
                            ?>

                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo URL; ?>/assets/js/jquery.min.js"></script>
<script src="<?php echo URL; ?>/assets/js/popper.min.js"></script>
<script src="<?php echo URL; ?>/assets/js/bootstrap.min.js"></script>
<script>
    $(document).ready(function() {
        $(".preloader").fadeOut();
        $('#default-datatable').DataTable();
        var table = $('#example').DataTable({
            lengthChange: false,
            buttons: ['copy', 'excel', 'pdf', 'print', 'colvis'],
            <?php echo  OPTION_FR ?>,
        });
        table.buttons().container()
            .appendTo('#example_wrapper .col-md-6:eq(0)');

        $('.buttons-colvis').find('span').text('Filtrer')

    });
</script>

<div class="modal fade " id="largesizemodal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Your modal title here</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias, dicta. Voluptate cumque odit quam velit maiores sint rerum, dolore impedit commodi. Tempora eveniet odit vero rem blanditiis, tenetur laudantium cumque.</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias, dicta. Voluptate cumque odit quam velit maiores sint rerum, dolore impedit commodi. Tempora eveniet odit vero rem blanditiis, tenetur laudantium cumque.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-inverse-primary" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                <button type="button" class="btn btn-primary"><i class="fa fa-check-square-o"></i> Save changes</button>
            </div>
        </div>
    </div>
</div>