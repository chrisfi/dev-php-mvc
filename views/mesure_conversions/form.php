<?php
require_once(rot . ds . 'app/Controller.php');
$this->loadModel('unitemesure');
$nitemesures = $this->unitemesure->get_all();
?>
<form id="form-mesure_conversion" action="" method="POST">
    <div class="row mb-3">
        <div class="col-lg-1">
            <div class="form-group">
                <div class="kl-span-text mt-1 text-center">
                    <span style="font-size: 54px;    color: crimson;">1</span>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group">
                <label></label>
                <select class="form-control single-select" id="id-unit_to_conver" name="unit_to_conver" required>
                    <?php foreach ($nitemesures as $nitemesure) { ?>
                        <option value="<?php echo $nitemesure['unite_mesure_id']; ?>"><?php echo $nitemesure['unite_mesure_libelle']; ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div class="col-lg-1">
            <div class="form-group">
                <div class="kl-span-text mt-1 text-center">
                    <span style="font-size: 54px;    color: crimson;">=</span>
                </div>

            </div>
        </div>

        <div class="col-lg-2">
            <div class="form-group">
                <label></label>
                <input id="id-unite_mes" class="kl-input_decimal" type="number" step="any" min='0' value="" name="value" required>
            </div>
        </div>

        <div class="col-lg-4">
            <div class="form-group">
                <label></label>
                <select class="form-control single-select" id="id-unite_mes" name="unite_mes" required>
                    <?php foreach ($nitemesures as $nitemesure) { ?>
                        <option value="<?php echo $nitemesure['unite_mesure_id']; ?>"><?php echo $nitemesure['unite_mesure_libelle']; ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
    </div>

    <div class="form-group kl-btn-form-default">
        <input type="hidden" id="id-conversion_id" value="0" name="conversion_id">
        <input type="hidden" id="id-user_id" value="<?php echo USER_ID; ?>" name="user_id">
        <button type="submit" name="action" value="add_conversion" class="btn btn-primary shadow-primary px-5 "><i class="icon-lock"></i>Enregistrer</button>
    </div>
</form>