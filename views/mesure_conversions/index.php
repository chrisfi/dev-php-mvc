<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">

                <div class="row">
                    <div class="col">
                    </div>
                    <div class="col"> <i class="fa fa-table"></i> Liste des mesures de conversion</div>
                    <div class="col"><a href="<?php echo b_url ?>/mesure_conversions/add" class="btn btn-dark shadow-dark waves-effect waves-light m-1 float-right">Ajouter nouveau</a></div>
                </div>
                <div class="row">
                <?php if (!empty($res)) {
                           
                           if ($res['status'] == true) {
                       ?>
                               <div class="alert alert-outline-success alert-dismissible w-100" role="alert">
                                   <button type="button" class="close" data-dismiss="alert">×</button>

                                   <div class="alert-icon">
                                       <i class="fa fa-check"></i>
                                   </div>
                                   <div class="alert-message">
                                       <span>Ajout avec success</span>
                                   </div>
                               </div>
                           <?php
                           } else { ?>
                               <div class="alert alert-icon-danger alert-dismissible w-100" role="alert">
                                   <button type="button" class="close" data-dismiss="alert">×</button>
                                   <div class="alert-icon icon-part-danger">
                                       <i class="fa fa-times"></i>
                                   </div>
                                   <div class="alert-message">
                                       <span>Ajout refuse</span>
                                   </div>
                               </div>
                       <?php }
                       } ?>
                </div>
            </div>
            <div class="row kl-action-button" style="right: 42px">
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table id="id_table" class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>1 UNITE DE MESURE</th>
                                <th>VALEUR UNITE DE MESURE</th>
                                <th>UNITE</th>
                                <th>VALEUR</th>
                                <th>Action</th>
                        </thead>
                        <tbody>
                            <?php foreach ($mesure_conversions as $mesure_conversion) {
                                $this->loadModel('unitemesure');
                                $unitemesures_conver = $this->unitemesure->get_Unite_Mesure_by_id($mesure_conversion['unit_to_conver']);
                                $unitemesures_unite = $this->unitemesure->get_Unite_Mesure_by_id($mesure_conversion['unite_mes']);

                            ?>
                                <tr>
                                    <td><?php echo $mesure_conversion['conversion_id']; ?></td>
                                    <td> <strong style="font-size: 25px;">1</strong> <?php echo $unitemesures_conver['unite_mesure_code'] ?></td>
                                    <td>= <strong style="font-size: 25px;"><?php echo $mesure_conversion['value'] ?></strong> <?php echo $unitemesures_unite['unite_mesure_code'] ?></td>
                                    <td><?php echo $unitemesures_unite['unite_mesure_code']; ?></td>
                                    <td><strong style="font-size: 25px;"><?php echo $mesure_conversion['value'] ?></strong></td>
                                    <td>
                                        <button type="button" data-toggle="modal" data-target="#update-unitmesure" class="btn btn-success waves-effect waves-light m-1"> <i class="zmdi zmdi-edit"></i> <span>Edite</span> </button>
                                        <button type="button" data-toggle="modal" data-target="#delete-unitmesure" class="btn btn-danger waves-effect waves-light m-1"> <i class="fa fa fa-trash-o"></i> <span>Suprimer</span> </button>
                                    </td>
                                </tr>
                            <?php }
                            ?>

                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo URL; ?>/assets/js/jquery.min.js"></script>
<script src="<?php echo URL; ?>/assets/js/popper.min.js"></script>
<script src="<?php echo URL; ?>/assets/js/bootstrap.min.js"></script>
<script>
    $(document).ready(function() {
        $(".preloader").fadeOut();
        var table = $('#id_table').DataTable({
            lengthChange: false,
            buttons: ['copy', 'excel', 'pdf', 'print', 'colvis'],
            <?php echo  OPTION_FR ?>,
        });
        table.buttons().container()
            .appendTo('#id_table_wrapper .col-md-6:eq(0)');

        $('.buttons-colvis').find('span').text('Filtrer');

    });
</script>