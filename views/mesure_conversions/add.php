<script src="<?php echo URL; ?>/assets/plugins/inputtags/js/bootstrap-tagsinput.js"></script>
<div class="card-body">
    <div class="card-title text-primary">Ajouter nouveau mesure de conversion</div>
    <hr>
    <?php
    require_once(rot . ds . 'views/mesure_conversions/form.php'); ?>
</div>
<script src="<?php echo URL; ?>/assets/js/jquery.min.js"></script>
<script src="<?php echo URL; ?>/assets/js/popper.min.js"></script>
<script src="<?php echo URL; ?>/assets/js/bootstrap.min.js"></script>
<script>
    var url_mesure_conversions_add_ajax = "<?php echo parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH); ?>";
</script>