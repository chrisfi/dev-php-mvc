<form action="" method="POST">
    <div class="form-group">
        <label for="input-1">Libelle</label>
        <input type="text" class="form-control" id="lib_fournisseur" value="<?php echo isset($fournis) ? $fournis['lib_fournisseur'] : '' ?>" name="lib_fournisseur" required placeholder="Entrer libelle">
    </div>
    <div class="form-group">
        <label for="input-2">Adresse</label>
        <input type="text" class="form-control" id="adress_fournisseur" value="<?php echo isset($fournis) ? $fournis['adress_fournisseur'] : '' ?>" name="adress_fournisseur" placeholder="Entrer adresse">
    </div>
    <div class="form-group">
        <label for="input-3">Phone</label>
        <input type="text" class="form-control" id="phon_fournisseur" value="<?php echo isset($fournis) ? $fournis['phon_fournisseur'] : '' ?>" name="phon_fournisseur" placeholder="Entrer téléphone">
    </div>

    <div class="form-group kl-btn-form-default">
        <input type="hidden" id="id_fournisseur" value="<?php echo isset($fournis) ? $fournis['id_fournisseur'] : 0 ?>" name="id_fournisseur">
        <button type="submit" name="btn_save_fournisseur" class="btn btn-primary shadow-primary px-5 kl-save-fournisseur">Enregistrer</button>
    </div>
</form>