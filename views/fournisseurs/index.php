<?php
// var_dump($fournis);
?>

<link href="<?php echo URL; ?>/assets/css/style.min.css" rel="stylesheet" />
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col"></div>
                    <div class="col">
                        <i class="fa fa-table"></i> Liste des fournisseurs
                    </div>
                    <!--<div class="col"><a href="<?php echo b_url ?>/clients/add" type="button" class="btn btn-dark shadow-dark waves-effect waves-light m-1 float-right">Ajouter nouveau client</a></div> #}
       
       -->
                </div>
            </div>

            <div class="row kl-action-button d-none " style="right: 42px">

                <button class="btn-social btn-social-circle btn-outline-dribbble waves-effect waves-light m-1 kl-delet-client"><i class="fa fa-trash-o"></i></a>


                    <button class="btn-social btn-social-circle btn-outline-skype waves-effect waves-light m-1 kl-edit-client"><i class="zmdi zmdi-edit"></i></a>

            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table id="example" class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Libelle</th>
                                <th>Adresse</th>
                                <th>Téléphone</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($fournis as $fourni) { ?>
                                <tr>
                                    <td><?php echo $fourni['lib_fournisseur']; ?></td>
                                    <td><?php echo $fourni['adress_fournisseur'] ?></td>
                                    <td><?php echo $fourni['phon_fournisseur'] ?></td>
                                    <td>
                                        <a href="/fournisseurs/edit?id_fourni=<?php echo  $fourni['id_fournisseur'] ?>" type="button" data-id="<?php echo $fourni['id_fournisseur'] ?>" class="btn btn-primary shadow-primary waves-effect waves-light m-1 kl-edit-id_fournisseur">Editer</a>
                                        <button type="button" class="btn btn-danger shadow-danger waves-effect waves-light m-1">Suprimer</button>
                                    </td>
                                </tr>
                            <?php }
                            ?>
                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>
</div><!-- End Row-->
<!-- Bootstrap core JavaScript-->


<script src="<?php echo URL; ?>/assets/js/jquery.min.js"></script>
<script>
  $(document).ready(function() {
    $(".preloader").fadeOut();
    $('#default-datatable').DataTable();
    var table = $('#example').DataTable({
      lengthChange: false,
      buttons: ['copy', 'excel', 'pdf', 'print', 'colvis'],
      <?php echo  OPTION_FR ?>,
    });
    table.buttons().container()
      .appendTo('#example_wrapper .col-md-6:eq(0)');

    $('.buttons-colvis').find('span').text('Filtrer')

  });
</script>