                

            </div>
            <!-- End container-fluid-->

        </div>
        <!--End content-wrapper-->
        <!--Start Back To Top Button-->
        <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
        <!--End Back To Top Button-->

        <!--Start footer-->
        <footer class="footer">
            <div class="container">
                <div class="text-center">
                    Copyright © 2018 Admin
                </div>
            </div>
        </footer>
        <!--End footer-->

    </div>
    <!--End wrapper-->

    <!-- Bootstrap core JavaScript-->
    <script src="<?php echo URL; ?>/assets/js/jquery.min.js"></script>
    <script src="<?php echo URL; ?>/assets/js/popper.min.js"></script>
    <script src="<?php echo URL; ?>/assets/js/bootstrap.min.js"></script>
    <!-- simplebar js -->
    <script src="<?php echo URL; ?>/assets/plugins/simplebar/js/simplebar.js"></script>
    <!-- waves effect js -->
    <script src="<?php echo URL; ?>/assets/js/waves.js"></script>
    <!-- sidebar-menu js -->
    <script src="<?php echo URL; ?>/assets/js/sidebar-menu.js"></script>
    <!-- Custom scripts -->
    <script src="<?php echo URL; ?>/assets/js/app-script.js"></script>
    <!-- Chart js -->
    <script src="<?php echo URL; ?>/assets/plugins/Chart.js/Chart.min.js"></script>
    <!--Peity Chart -->
    <script src="<?php echo URL; ?>/assets/plugins/peity/jquery.peity.min.js"></script>
    <!-- Index js -->
    <!-- <script src="assets/js/index.js"></script> -->
    <!--Data Tables js-->

    <script src="<?php echo URL; ?>/assets/plugins/bootstrap-datatable/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo URL; ?>/assets/plugins/bootstrap-datatable/js/dataTables.bootstrap4.min.js"></script>
    <script src="<?php echo URL; ?>/assets/plugins/bootstrap-datatable/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo URL; ?>/assets/plugins/bootstrap-datatable/js/buttons.bootstrap4.min.js"></script>
    <script src="<?php echo URL; ?>/assets/plugins/bootstrap-datatable/js/jszip.min.js"></script>
    <script src="<?php echo URL; ?>/assets/plugins/bootstrap-datatable/js/pdfmake.min.js"></script>
    <script src="<?php echo URL; ?>/assets/plugins/bootstrap-datatable/js/vfs_fonts.js"></script>
    <script src="<?php echo URL; ?>/assets/plugins/bootstrap-datatable/js/buttons.html5.min.js"></script>
    <script src="<?php echo URL; ?>/assets/plugins/bootstrap-datatable/js/buttons.print.min.js"></script>
    <script src="<?php echo URL; ?>/assets/plugins/bootstrap-datatable/js/buttons.colVis.min.js"></script>


</body>

<!-- Mirrored from codervent.com/rukada/color-admin/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 09 Nov 2019 15:04:09 GMT -->

</html>