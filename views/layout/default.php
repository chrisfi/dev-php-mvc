<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Gestion</title>
    <!--favicon-->
    <link href="<?php echo URL; ?>/assets/css/style.min.css" rel="stylesheet" />
    <link rel="icon" href="<?php echo URL; ?>/webroot/assets/images/favicon.ico" type="image/x-icon">
    <!-- simplebar CSS-->
    <link href="<?php echo URL; ?>/assets/plugins/simplebar/css/simplebar.css" rel="stylesheet" />
    <!-- Bootstrap core CSS-->
    <link href="<?php echo URL; ?>/assets/css/bootstrap.min.css" rel="stylesheet" />
    <!-- animate CSS-->
    <link href="<?php echo URL; ?>/assets/css/animate.css" rel="stylesheet" type="text/css" />
    <!-- Icons CSS-->
    <link href="<?php echo URL; ?>/assets/css/icons.css" rel="stylesheet" type="text/css" />
    <!-- Sidebar CSS-->
    <link href="<?php echo URL; ?>/assets/css/sidebar-menu.css" rel="stylesheet" />
    <!-- Custom Style-->
    <link href="<?php echo URL; ?>/assets/css/app-style.css" rel="stylesheet" />

    <link href="<?php echo URL; ?>/assets/plugins/bootstrap-datatable/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo URL; ?>/assets/plugins/bootstrap-datatable/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo URL; ?>/assets/plugins/select2/css/select2.min.css" rel="stylesheet" />
    <!--inputtags-->
    <link href="<?php echo URL; ?>/assets/plugins/inputtags/css/bootstrap-tagsinput.css" rel="stylesheet" />
    <!--multi select-->
    <!--Bootstrap Datepicker-->
    <!--Touchspin-->
    <link href="<?php echo URL; ?>/assets/plugins/bootstrap-touchspin/css/jquery.bootstrap-touchspin.css" rel="stylesheet" type="text/css">

    <!-- notifications css -->
    <link rel="stylesheet" href="<?php echo URL; ?>/assets/plugins/notifications/css/lobibox.min.css" />

    <!--Switchery-->
    <link href="<?php echo URL; ?>/assets/plugins/switchery/css/switchery.min.css" rel="stylesheet" />

    <link href="<?php echo URL; ?>/assets/plugins/bootstrap-switch/bootstrap-switch.min.css" rel="stylesheet">


</head>

<body style="padding-right: 5px !important;">

    <!-- Start wrapper-->
    <div id="wrapper">

        <!--Start sidebar-wrapper-->
        <div id="sidebar-wrapper" data-simplebar="" data-simplebar-auto-hide="true">
            <div class="brand-logo">
                <a href="index.html">
                    <img src="<?php echo URL; ?>/assets/images/logo-icon.png" class="logo-icon" alt="logo icon">
                    <h5 class="logo-text">Gestion de stock</h5>
                </a>
            </div>
            <ul class="sidebar-menu do-nicescrol">
                <li class="sidebar-header">Menu</li>
                <li>
                    <a href="index.html" class="waves-effect">
                        <i class="icon-people icons"></i>
                        <span>Client</span> <i class="fa fa-angle-left pull-right"></i>
                    </a>

                    <ul class="sidebar-submenu">
                        <li><a href="/clients/index"><i class="zmdi zmdi-format-list-bulleted"></i>Liste</a></li>
                        <li><a href="/clients/add"><i class="zmdi zmdi-account-add"></i>Ajouter</a></li>
                     
                    </ul>
                </li>

                <li>
                    <a href="index.html" class="waves-effect">
                        <i class="icon-book-open icons"></i> <span>Mesure</span> <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="sidebar-submenu">
                        <li>
                            <a href="index.html" class="waves-effect">
                                <i class="zmdi zmdi-view-dashboard"></i> <span>Unite de mesure</span> <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="sidebar-submenu">
                                <li><a href="/unitemesures/add"><i class="zmdi zmdi-plus-circle"></i>Ajouter</a>
                                </li>
                                <li><a href="/unitemesures/index"><i class="zmdi zmdi-view-list-alt"></i>Liste</a></li>
                            </ul>
                        </li>

                        <li>
                            <a href="index.html" class="waves-effect">
                                <i class="zmdi zmdi-view-dashboard"></i> <span>Conversion de mesure</span> <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="sidebar-submenu">
                                <li><a href="/mesure_conversions/add"><i class="zmdi zmdi-plus-circle"></i>Ajouter</a>
                                </li>
                                <li><a href="/mesure_conversions/index"><i class="zmdi zmdi-view-list-alt"></i>Liste</a></li>
                            </ul>
                        </li>

                    </ul>
                </li>

                <li>
                    <a href="javaScript:void();" class="waves-effect">
                        <i class="fa fa-cart-plus"></i> <span>Produit</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="sidebar-submenu">

                        <li>
                            <a href="javaScript:void();"><i class="zmdi zmdi-star-outline"></i>Produit<i class="fa fa-angle-left pull-right"></i></a>
                            <ul class="sidebar-submenu">
                                <li><a href="/produits/index"><i class="zmdi zmdi-star-outline"></i>Liste</a></li>
                                <li><a href="/produits/add"><i class="zmdi zmdi-star-outline"></i>Ajouter</a></li>
                                <!--<li><a href="<?php echo b_url ?>/produit_unite_mesure/unite_mesure"><i class="zmdi zmdi-star-outline"></i>Unité de mesure/Prix</a></li>-->

                            </ul>
                        </li>



                        <li>
                            <a href="javaScript:void();"><i class="zmdi zmdi-star-outline"></i>Avoir Consignation<i class="fa fa-angle-left pull-right"></i></a>
                            <ul class="sidebar-submenu">
                                <li><a href="javaScript:void();"><i class="zmdi zmdi-star-outline"></i>Ajot Avoir</a></li>
                            </ul>
                        </li>

                        <li><a href="javaScript:void();" class="waves-effect"><i class="zmdi zmdi-star-outline"></i>liste de Fournisseurs</a></li>
                        <li><a href="javaScript:void();" class="waves-effect"><i class="zmdi zmdi-star-outline"></i>liste de Famille</a></li>
                        <li><a href="javaScript:void();" class="waves-effect"><i class="zmdi zmdi-star-outline"></i>liste des Tarifs</a></li>
                    </ul>
                </li>



                <li>
                    <a href="index.html" class="waves-effect">
                        <i class="icon-book-open icons"></i> <span>Commande</span> <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="sidebar-submenu">
                        <li>
                            <a href="index.html" class="waves-effect">
                                <i class="zmdi zmdi-view-dashboard"></i> <span>B.L</span> <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="sidebar-submenu">
                                <li><a href="/commande_entetes/add"><i class="zmdi zmdi-plus-circle"></i>Creer</a>
                                </li>
                                <li><a href="/commande_entetes/index"><i class="zmdi zmdi-view-list-alt"></i>Liste</a></li>
                                <li><a href="/bl/index"><i class="zmdi zmdi-view-list-alt"></i>B.L Valide</a></li>
                            </ul>
                        </li>

                        <li>
                            <a href="index.html" class="waves-effect">
                                <i class="zmdi zmdi-view-dashboard"></i> <span>Facture</span> <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="sidebar-submenu">
                                <li><a href="/mesure_conversions/add"><i class="zmdi zmdi-plus-circle"></i>Creer</a>
                                </li>
                                <li><a href="/mesure_conversions/index"><i class="zmdi zmdi-view-list-alt"></i>Rechercher</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="index.html" class="waves-effect">
                                <i class="zmdi zmdi-view-dashboard"></i> <span>B.L emballage</span> <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="sidebar-submenu">
                                <li><a href="/mesure_conversions/add"><i class="zmdi zmdi-plus-circle"></i>Creation </a>
                                </li>
                                <li><a href="/mesure_conversions/index"><i class="zmdi zmdi-view-list-alt"></i>Rechercher</a></li>
                            </ul>
                        </li>

                    </ul>
                </li>


                <li>
                    <a href="index.html" class="waves-effect">
                        <i class="icon-people icons"></i>
                        <span>Fournisseur</span> <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="sidebar-submenu">
                        <li><a href="/fournisseurs/add"><i class="zmdi zmdi-account-add"></i>Ajouter</a></li>
                        <li><a href="/fournisseurs/index"><i class="zmdi zmdi-format-list-bulleted"></i>Liste</a></li>
                    </ul>
                </li>


                <li>
                    <a href="index.html" class="waves-effect">
                        <i class="icon-people icons"></i>
                        <span>Utilisateur</span> <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="sidebar-submenu">
                        <li><a href="/Utilisateurs/add"><i class="zmdi zmdi-account-add"></i>Ajouter</a></li>
                        <li><a href="/Utilisateurs/index"><i class="zmdi zmdi-format-list-bulleted"></i>Liste</a></li>
                    </ul>
                </li>



                <li>
                    <a href="index.html" class="waves-effect">
                        <i class="icon-people icons"></i>
                        <span>Entree produit</span> <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="sidebar-submenu">
                        <li><a href="/entre_produits/add"><i class="zmdi zmdi-account-add">Ajouter</i></a></li>
                        <li><a href="/entre_produits/index"><i class="zmdi zmdi-format-list-bulleted"></i>Liste</a></li>
                    </ul>
                </li>




                <!-- <li class="sidebar-header">LABELS</li> -->
                <li><a href="javaScript:void();" class="waves-effect"><i class="zmdi zmdi-coffee text-danger"></i> <span>Important</span></a></li>
                <li><a href="javaScript:void();" class="waves-effect"><i class="zmdi zmdi-chart-donut text-success"></i> <span>Warning</span></a></li>
                <li><a href="javaScript:void();" class="waves-effect"><i class="zmdi zmdi-share text-info"></i> <span>Information</span></a></li>
                <li><a href="javaScript:void();" class="waves-effect"><i class="zmdi zmdi-share text-info"></i> <span>Information</span></a></li>
            </ul>

        </div>
        <!--End sidebar-wrapper-->

        <!--Start topbar header-->
        <header class="topbar-nav">
            <nav class="navbar navbar-expand fixed-top bg-white">
                <ul class="navbar-nav mr-auto align-items-center">
                    <li class="nav-item">
                        <a class="nav-link toggle-menu" href="javascript:void();">
                            <i class="icon-menu menu-icon"></i>
                        </a>
                    </li>
                </ul>

                <ul class="navbar-nav mr-auto align-items-center">

                    <li class="nav-item">
                        <a class="text-success" style="font-size: 44px;" href="javascript:void();"><?php echo VENTE ?></a>
                    </li>

                </ul>

                <ul class="navbar-nav align-items-center right-nav-link">


                    <li class="nav-item">
                        <a class="nav-link dropdown-toggle dropdown-toggle-nocaret" data-toggle="dropdown" href="#">
                            <span class="user-profile"><img src="<?php echo URL; ?>/assets/images/avatars/avatar-13.png" class="img-circle" alt="user avatar"></span>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li class="dropdown-item user-details">
                                <a href="javaScript:void();">
                                    <div class="media">
                                        <div class="avatar"><img class="align-self-start mr-3" src="<?php echo URL; ?>/assets/images/avatars/avatar-13.png" alt="user avatar"></div>
                                        <div class="media-body">
                                            <h6 class="mt-2 user-title"><?php echo (isset($_SESSION['login'])) ? $_SESSION['login'] : ''; ?></h6>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="dropdown-divider"></li>
                            <li class="dropdown-divider"></li>
                            <li class="dropdown-item"><i class="icon-wallet mr-2"></i> Account</li>
                            <li class="dropdown-divider"></li>
                            <li class="dropdown-item"><i class="icon-settings mr-2"></i> Setting</li>
                            <li class="dropdown-divider"></li>
                            <li class="dropdown-item " id="id-logout" style="cursor: pointer;"><i class="icon-power mr-2"></i> Logout</li>
                        </ul>
                    </li>
                </ul>
            </nav>
        </header>
        <!--End topbar header-->

        <div class="clearfix"></div>

        <div class="content-wrapper">
            <div class="container-fluid">
                <div class="preloader">
                    <div class="lds-ripple" style="top: calc(50% - 3.5px);left: calc(51% - -93.5px);position: fixed;">
                        <div class="lds-pos"></div>
                        <div class="lds-pos"></div>
                    </div>
                </div>
                <?= $content ?>

            </div>
            <!-- End container-fluid-->

        </div>
        <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>

        <!--Start footer-->
        <footer class="footer">
            <div class="container">
                <div class="text-center">
                    Copyright © 2021 Admin
                </div>
            </div>
        </footer>
        <!--End footer-->

    </div>
    <!--End wrapper-->

    <!-- Bootstrap core JavaScript-->
    <script src="<?php echo URL; ?>/assets/js/jquery.min.js"></script>
    <script src="<?php echo URL; ?>/assets/js/popper.min.js"></script>
    <script src="<?php echo URL; ?>/assets/js/bootstrap.min.js"></script>
    <!-- simplebar js -->
    <script src="<?php echo URL; ?>/assets/plugins/simplebar/js/simplebar.js"></script>
    <!-- waves effect js -->
    <script src="<?php echo URL; ?>/assets/js/waves.js"></script>
    <!-- sidebar-menu js -->
    <script src="<?php echo URL; ?>/assets/js/sidebar-menu.js"></script>
    <!-- Custom scripts -->
    <script src="<?php echo URL; ?>/assets/js/app-script.js"></script>
    <!-- Chart js -->
    <script src="<?php echo URL; ?>/assets/plugins/Chart.js/Chart.min.js"></script>
    <!--Peity Chart -->
    <script src="<?php echo URL; ?>/assets/plugins/peity/jquery.peity.min.js"></script>
    <!-- Index js -->
    <!-- <script src="assets/js/index.js"></script> -->
    <!--Data Tables js-->

    <script src="<?php echo URL; ?>/assets/plugins/bootstrap-datatable/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo URL; ?>/assets/plugins/bootstrap-datatable/js/dataTables.bootstrap4.min.js"></script>
    <script src="<?php echo URL; ?>/assets/plugins/bootstrap-datatable/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo URL; ?>/assets/plugins/bootstrap-datatable/js/buttons.bootstrap4.min.js"></script>
    <script src="<?php echo URL; ?>/assets/plugins/bootstrap-datatable/js/jszip.min.js"></script>
    <script src="<?php echo URL; ?>/assets/plugins/bootstrap-datatable/js/pdfmake.min.js"></script>
    <script src="<?php echo URL; ?>/assets/plugins/bootstrap-datatable/js/vfs_fonts.js"></script>
    <script src="<?php echo URL; ?>/assets/plugins/bootstrap-datatable/js/buttons.html5.min.js"></script>
    <script src="<?php echo URL; ?>/assets/plugins/bootstrap-datatable/js/buttons.print.min.js"></script>
    <script src="<?php echo URL; ?>/assets/plugins/bootstrap-datatable/js/buttons.colVis.min.js"></script>


    <script src="<?php echo URL; ?>/assets/plugins/alerts-boxes/js/sweetalert.min.js"></script>
    <script src="<?php echo URL; ?>/assets/plugins/alerts-boxes/js/sweet-alert-script.js"></script>

    <script src="<?php echo URL; ?>/assets/plugins/notifications/js/lobibox.min.js"></script>
    <script src="<?php echo URL; ?>/assets/plugins/notifications/js/notifications.min.js"></script>
    <script src="<?php echo URL; ?>/assets/plugins/notifications/js/notification-custom-script.js"></script>

    <script src="<?php echo URL; ?>/assets/plugins/bootstrap-touchspin/js/jquery.bootstrap-touchspin.js"></script>
    <script src="<?php echo URL; ?>/assets/plugins/bootstrap-touchspin/js/bootstrap-touchspin-script.js"></script>

    <!--Select Plugins Js-->
    <script src="<?php echo URL; ?>/assets/plugins/select2/js/select2.min.js"></script>
    <!--Inputtags Js-->
    <script src="<?php echo URL; ?>/assets/plugins/inputtags/js/bootstrap-tagsinput.js"></script>

    <script>
        var url_authentificate = "<?php b_url ?>";
    </script>
    <script src="<?php echo URL; ?>/assets/js_stock/produit.js"></script>
    <script src="<?php echo URL; ?>/assets/js_stock/login.js"></script>
    <script src="<?php echo URL; ?>/assets/js_stock/unitemesures.js"></script>
    <script src="<?php echo URL; ?>/assets/js_stock/mesure_conversion.js"></script>
    <script src="<?php echo URL; ?>/assets/js_stock/commande.js"></script>

    <script>
        $(document).ready(function() {
            $('.single-select').select2();
            $(".preloader").fadeOut();
            $(document).on('click', '.kl-logout', function() {
                $.ajax({
                    url: <?php rot; ?> '/',
                    data: {
                        'logout': 'logout'
                    },
                    method: "POST",
                    dataType: "JSON",
                    beforeSend: function() {},
                    success: function(data) {
                        console.log(data);
                    }
                });
            });
        });
    </script>




</body>

<!-- Mirrored from codervent.com/rukada/color-admin/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 09 Nov 2019 15:04:09 GMT -->

</html>