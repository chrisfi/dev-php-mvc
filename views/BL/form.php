
    <div class="row">
        <div class="col-lg-12 kl-qte-restant-produit">
            <div class="form-group">
                <label>Somme total</label>
                <input type="number" class="kl-input_decimal kl_somme_total" step="any" value="0.00" disabled="disabled" class="form-control">
            </div>
        </div>
        <div class="col-lg-12 kl-qte-restant-produit">
            <div class="form-group">
                <label>Restant</label>
                <input type="number" class="kl-input_decimal kl_somme_restant" step="any" value="0.00" disabled="disabled" class="form-control">
            </div>
        </div>
        <div class="col-lg-12">
            <div class="form-group">
                <label>Montant</label>
                <input id="montant_payer" class="kl-input_decimal" type="number" step="any" min='1' value="1.00" name="montant_A_payer" required>
            </div>
        </div>

        <input type="hidden" id="id_cmd_a_paye" name="id_cmd_A_paye">
        <input type="hidden" class="kl_somme_total" name="somme_total">
        <input type="hidden" class="kl_somme_restant" name="somme_restant">

        <div class="col-lg-12 ">
            <div class="row">
                <div class="col"></div>
                <div class="col">
                    <div class="form-group kl-btn-form-default">
                        <button type="button" class="btn btn-success shadow-primary w-100 mt-4 px-5 kl-save-1-payement "><i class="icon-plus"></i>Valide</button>
                    </div>
                </div>
                <div class="col"></div>
            </div>

        </div>
    </div>
