<?php
// var_dump($Bl_valide); 
?>
<link href="<?php echo URL; ?>/assets/css/style.min.css" rel="stylesheet" />
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col"></div>
                    <div class="col">
                        <i class="fa fa-table"></i> Bon de commande valide
                    </div>
                    <div class="col"></div>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table id="id-tab_Bl" class="table table-bordered">
                        <thead>
                            <tr>

                                <th>Numero</th>
                                <th>Client</th>
                                <th>Adresse</th>
                                <th>Date validation</th>
                                <th>Montant</th>
                                <th>Montant Restant</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($Bl_valide as $key => $Bl) {
                                $this->loadModel('client');

                                $client = $this->client->get_cli($Bl['client_id']);
                                $client_nom_prenom = isset($client['client_nom']) ? $client['client_nom'] . ' ' . $client['client_prenom'] : 'Non detérminé';
                                $adress_client = isset($client['client_adress']) ? $client['client_adress'] : 'Non detérminé';
                            ?>
                                <tr>

                                    <td><?php echo $Bl['cmd_num']; ?></td>
                                    <td><?php echo $client_nom_prenom; ?></td>
                                    <td><?php echo $adress_client; ?></td>
                                    <td><?php echo $Bl['date_validation'] ?></td>
                                    <td class="kl-somme"><?php echo $Bl['somme'] ?> Ar</td>
                                    <td class=kl-reste><?php echo $Bl['reste'] ?> Ar</td>
                                    <td>
                                        <button type="button" data-valide="<?php echo $Bl['reste'] == 0 ? 0 : $Bl['reste'] ?>" data-id="<?php echo $Bl['cmd_id'] ?>" class="btn <?php echo $Bl['reste'] == 0 ? 'btn-success shadow-success' : 'btn-danger shadow-danger' ?>  waves-effect waves-light m-1 kl-payer_reste"><?php echo $Bl['reste'] == 0 ?'Déja Payé':'Non payé'?></button>
                                        <!-- <button type="button" class="btn btn-danger shadow-danger waves-effect waves-light m-1">Suprimer</button> -->
                                    </td>
                                </tr>
                            <?php }
                            ?>
                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>
</div><!-- End Row-->
<script src="<?php echo URL; ?>/assets/js/jquery.min.js"></script>
<script>
    var url_payer = "<?php echo b_url ?>/bl/BonL"
</script>
<script>
    $(document).ready(function() {
        $(".preloader").fadeOut();
        var table = $('#id-tab_Bl').DataTable({
            lengthChange: false,
            buttons: ['copy', 'excel', 'pdf', 'print', 'colvis'],
            <?php echo  OPTION_FR ?>,
        });
        table.buttons().container()
            .appendTo('#id-tab_Bl_wrapper .col-md-6:eq(0)');

        $('.buttons-colvis').find('span').text('Filtrer')

    });
</script>

<script>
    $(document).ready(function() {
        $('.kl-payer_reste').click(function(e) {
            e.preventDefault();
            var id_cmd = $(this).data('id');
            var valide = $(this).data('valide');
            if (valide == 0) {
                Error_notification_danger('Facture déjà payé');
                return;
            }
            var data = {};
            data.id_cmd = id_cmd;
            data.action = 'payer_facture';
            $.ajax({
                url: 'BonL',
                data,
                method: "POST",
                dataType: "html",
                beforeSend: function() {},
                success: function(response) {
                    $('#id-modal-payer_facture .modal-body').html(response);
                    $('#id-modal-payer_facture').modal('show');
                }
            });
        });
    });
</script>

<div class="modal fade" id="id-modal-payer_facture">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Payer facture</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-inverse-primary kl-dismiss-modal" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                <button type="button" name="btn_save_client" class="btn btn-primary kl-save-client"><i class="fa fa-check-square-o"></i>Enregistrer modification</button>
            </div>
        </div>
    </div>
</div>