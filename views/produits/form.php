<?php
require_once(rot . ds . 'app/Controller.php');
$this->loadModel('famille');
$this->loadModel('unitemesure');
$this->loadModel('produit_groupe');
$this->loadModel('produit');
$produit_familles = $this->famille->getAll();
$nitemesures = $this->unitemesure->get_all(); 
$produit_groupes = $this->produit_groupe->getAll();
$produit_cageots = $this->produit->get_produit_by_PRODUIT_ISCAG_1(1);
$id_prod = 0;
$prod_edit = array();
if (isset($_GET['id'])) {
    $id_prod = $_GET['id'];
    $prod_edit = $this->produit->get_produit_by_id($id_prod);
}
// var_dump($prod_edit);

?>
<form id="form-produit" action="" method="POST">
    <div class="card ">

        <div class="card-body">
            <div class="row">
                <div class="col-lg-4">
                    <div class="form-group">
                        <label>Groupe</label>
                        <select class="form-control single-select" id="ID_PRODUIT_GROUPE_ID" name="groupe_id" required>
                            <option value="0"></option>
                            <?php foreach ($produit_groupes as $produit_groupe) {
                                if (!empty($prod_edit)) { ?>
                                    <option <?php echo $produit_groupe['groupe_id'] == $prod_edit['groupe_id'] ? 'selected' : '' ?> value="<?php echo $produit_groupe['groupe_id']; ?>"><?php echo $produit_groupe['groupe_libelle']; ?></option>
                                <?php  } else { ?>
                                    <option value="<?php echo $produit_groupe['groupe_id']; ?>"><?php echo $produit_groupe['groupe_libelle']; ?></option>
                            <?php }
                            } ?>
                        </select>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <label>Famille</label>
                        <select class="form-control single-select" id="ID_PRODUIT_FAMIL_ID" name="famille_id">

                            <?php foreach ($produit_familles as $famille) {
                                if (!empty($prod_edit)) { ?>
                                    <option <?php echo $famille['famille_id'] == $prod_edit['famille_id'] ? 'selected' : '' ?> value="<?php echo $famille['famille_id']; ?>"><?php echo $famille['famille_libelle']; ?></option>
                                <?php } else {
                                ?>
                                    <option value="<?php echo $famille['famille_id']; ?>"><?php echo $famille['famille_libelle']; ?></option>
                                <?php
                                } ?>

                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <label>Unité de mesure(base)</label>
                        <select class="form-control single-select" id="ID_UNITEMESURE_ID" name="unite_mesure_id" required>
                            <?php foreach ($nitemesures as $nitemesure) {
                                if (!empty($prod_edit)) { ?>
                                    <option <?php echo $nitemesure['unite_mesure_id'] == $prod_edit['produit_unit_mes_id'] ? 'selected' : '' ?> value="<?php echo $nitemesure['unite_mesure_id']; ?>"><?php echo $nitemesure['unite_mesure_libelle']; ?></option>
                                <?php
                                } else { ?>
                                    <option value="<?php echo $nitemesure['unite_mesure_id']; ?>"><?php echo $nitemesure['unite_mesure_libelle']; ?></option>
                                <?php
                                }
                                ?>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>

            <div class="row mb-3">
                <div class="col-lg-12">
                    <div class="form-group">
                        <label for="ID_PRODUIT_LIBELLE">Libelle</label>
                        <input type="text" class="form-control" value="<?php echo isset($prod_edit['produit_libelle']) ? $prod_edit['produit_libelle'] : '' ?>" id="ID_PRODUIT_LIBELLE" name="produit_libelle" required placeholder="Entrer nom">
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label>Prix d'achat(AR)</label>
                        <input id="ID_PRODUIT_PA" class="kl-input_decimal_Ar" step="any" type="number" value="<?php echo isset($prod_edit['produit_prix_achat']) ? $prod_edit['produit_prix_achat'] : 0 ?>" min='0' name="produit_prix_achat" required>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label>Prix unitaire(AR) en UCODIS</label>
                        <input id="ID_PRODUIT_PU" class="kl-input_decimal_Ar" step="any" type="number" value="<?php echo isset($prod_edit['produit_prix_unit_ucodis']) ? $prod_edit['produit_prix_unit_ucodis'] : 0 ?>" min='0' name="produit_prix_unit_ucodis" required>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label>Prix unitaire(AR) en DETAILLE</label>
                        <input id="ID_PRODUIT_PU" class="kl-input_decimal_Ar" step="any" type="number" value="<?php echo isset($prod_edit['produit_unit_detaille']) ? $prod_edit['produit_unit_detaille'] : 0 ?>" min='0' name="produit_unit_detaille" required>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label>Stock minimum</label>
                        <input id="ID_PRODUIT_STK_MIN" class="kl-input_decimal" type="number" step="any" min='0' value="<?php echo isset($prod_edit['produit_stock_min']) ? $prod_edit['produit_stock_min'] : 0 ?>" name="produit_stock_min">
                    </div>
                </div>
            </div>

        </div>
    </div>




    <div class="row -100">
        <div class="card w-100">
            <div class="col-lg-12">

                <div class="card-header text-uppercase">GESTION DES EMBALLAGES</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col">

                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group py-2">
                                        <div class="icheck-material-success">
                                            <?php
                                            if (!empty($prod_edit)) {
                                            ?>
                                                <input type="checkbox" <?php echo ($prod_edit['produit_is_emballage']) == 1 ? 'checked' : '' ?> id="id-gerer-emballage-checkbox" name="produit_is_emballage">
                                                <label for="id-gerer-emballage-checkbox">Gerer Emballage</label>
                                            <?php
                                            } else {
                                            ?>
                                                <input type="checkbox" id="id-gerer-emballage-checkbox" name="produit_is_emballage">
                                                <label for="id-gerer-emballage-checkbox">Gerer Emballage</label>
                                            <?php
                                            }
                                            ?>

                                        </div>
                                    </div>
                                </div>


                                <div class="col-lg-6">
                                    <div class="form-group kl-input-prix-unitaire-emballage <?php (!empty($prod_edit) && $prod_edit['produit_is_emballage']==1 )?'':'d-none' ?>">
                                        <label>prix unitaire emballage (ar) :</label>
                                        <input id="ID_PRODUIT_PEMB" class="kl-input_decimal" type="number" step="any" min='0' value="<?php echo isset($prod_edit['produit_prix_emballage']) ? $prod_edit['produit_prix_emballage'] : 0 ?>" name="produit_prix_emballage">
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col">
                            <div class="row">

                                <div class="col-lg-4">
                                    <div class="form-group py-2">
                                        <div class="icheck-material-success">
                                            <?php
                                            if (!empty($prod_edit)) {
                                            ?>
                                                <input <?php echo ($prod_edit['produit_is_cageot']) == 1 ? 'checked' : '' ?> type="checkbox" id="id-gerer-cageot-checkbox" name="produit_is_cageot">
                                                <label for="id-gerer-cageot-checkbox">Gerer Cageot :</label>
                                            <?php
                                            } else {
                                            ?>
                                                <input type="checkbox" id="id-gerer-cageot-checkbox" name="produit_is_cageot">
                                                <label for="id-gerer-cageot-checkbox">Gerer Cageot :</label>
                                            <?php
                                            }

                                            ?>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-lg-8">
                                    <div class="form-group kl-select-cageaot <?php  (isset($prod_edit['produit_cageot_id']))?'':'d-none' ?>">
                                        <label>CAGEOT</label>
                                        <select class="form-control single-select" id="ID_PRODUIT_CAG_ID" name="produit_cageot_id">
                                            <option value="0"></option>
                                            <?php foreach ($produit_cageots as $produit) {
                                                if (!empty($prod_edit)) {
                                            ?>
                                                    <option <?php echo $produit['produit_id'] == $prod_edit['produit_cageot_id'] ? 'selected' : '' ?> value="<?php echo $produit['produit_id']; ?>"><?php echo $produit['produit_libelle']; ?></option>
                                                <?php
                                                } else {
                                                ?>
                                                    <option value="<?php echo $produit['produit_id']; ?>"><?php echo $produit['produit_libelle']; ?></option>
                                            <?php
                                                }
                                            } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="form-group kl-btn-form-default">
        <input type="hidden" id="ID_PRODUIT_ID" value="<?php echo isset($prod_edit['produit_id']) ? $prod_edit['produit_id'] : 0 ?>" name="PRODUIT_ID">
        <input type="hidden" id="ID_PRODUIT_CODE" value="<?php echo isset($prod_edit['produit_code']) ? $prod_edit['produit_code'] : 0 ?>" name="PRODUIT_CODE">
        <input type="hidden" id="ID_UTILISATEUR_ID" value="<?php echo USER_ID; ?>" name="UTILISATEUR_ID">
        <button type="submit" name="action" value="<?php echo isset($prod_edit['produit_id'])?'edit_prod':'add_prod' ?>" class="btn btn-primary shadow-primary px-5 kl-save-produit"><?php echo isset($prod_edit)?'Enregistrer modification':'Enregistrer' ?></button>
    </div>
</form>
