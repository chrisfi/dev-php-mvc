<link href="<?php echo URL; ?>/assets/css/app-style.css" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo URL; ?>/assets/css/mycss/produit.css">
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header"><i class="fa fa-table">
                </i>Liste des produits
                <div class="row">
                    <div class="col-lg-12">
                        <?php

                        if (!empty($mes)) {
                            // var_dump($mes);
                        }
                        ?>
                    </div>
                </div>
            </div>
            <div class="row kl-action-button d-none " style="right: 42px">
                <button class="btn-social btn-social-circle btn-outline-dribbble waves-effect waves-light m-1 kl-delet-client"><i class="fa fa-trash-o"></i></a>
                    <button class="btn-social btn-social-circle btn-outline-skype waves-effect waves-light m-1 kl-edit-client"><i class="zmdi zmdi-edit"></i></a>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table id="example" class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Code</th>
                                <th>Libelle</th>
                                <th>Unité de Base </th>
                                <th>Prix achat</th>
                                <?php
                                if (type_vente == 1) {
                                ?>
                                    <th>Prix unitaire</th>
                                <?php
                                } else {
                                ?>
                                    <th>Prix unitaire</th>
                                <?php
                                }
                                ?>
                                <th>Stock minimum</th>
                                <th>Stock courant</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($produits as $produit) { ?>
                                <tr>
                                    <td><?php echo $produit['produit_code']; ?></td>
                                    <td><?php echo $produit['produit_libelle']; ?></td>
                                    <td><?php echo $produit['unite_mesure_libelle']; ?></td>
                                    <?php
                                    if (type_vente == 1) {
                                    ?>
                                        <td><?php echo $produit['produit_unit_detaille'] ?></td>
                                    <?php
                                    } else {
                                    ?>
                                        <td><?php echo $produit['produit_prix_unit_ucodis'] ?></td>
                                    <?php
                                    }
                                    ?>
                                    <td><?php echo $produit['produit_prix_achat'] ?></td>
                                    <td><?php echo $produit['produit_stock_min'] ?></td>
                                    <td><?php echo $produit['produit_stock_courent'] ?></td>
                                    <td>
                                        <!-- <a href="/produit_unite_mesure/gerer_unite?id_produit=<?php echo $produit['produit_id'] ?>" type="button" class="btn btn-warning shadow-warning waves-effect waves-light m-1">Unité de mesure</a> -->
                                        <div class="btn-group m-1">
                                            <button type="button" class="btn btn-dark waves-effect waves-light"><span>Action</span></button>
                                            <button type="button" class="btn btn-dark split-btn-dark dropdown-toggle dropdown-toggle-split waves-effect waves-light" data-toggle="dropdown">
                                                <span class="caret"></span>
                                            </button>
                                            <div class="dropdown-menu kl-dropdown-menu">
                                                <a href="/produits/edit?id=<?php echo $produit['produit_id'] ?>" data-id="<?php echo $produit['produit_id'];  ?>" class="dropdown-item kl-btn-ahref-edit-produit">edit</a>
                                                <a href="javaScript:void();" data-id="<?php echo $produit['produit_id']; ?>" class="dropdown-item kl-btn-ahref-supprimer-produit">Supprimer</a>
                                                <a href="/produit_unite_mesure/gerer_unite?id_produit=<?php echo $produit['produit_id'] ?>" data-id="<?php echo $produit['produit_id']; ?>" class="dropdown-item ">Unité de mesure</a>
                                                <div class="dropdown-divider"></div>
                                                <!-- <a href="javaScript:void();" class="dropdown-item">Separated link</a> -->
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            <?php }
                            ?>

                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo URL; ?>/assets/js/jquery.min.js"></script>
<script>
    var url_delete_produit = "<?php echo b_url ?>/produits/delete";
    var url_get_produit = "<?php echo b_url ?>/produits/get_Produit_Ajax";
</script>
<script>
    var url_produit_add_ajax = "<?php echo b_url ?>/produits/add";
    var url_produit_delete_ajax = "<?php echo b_url ?>/produits/delete";
    var url_gerer_Unite_Mesure_Produit_Ajax = "<?php echo b_url; ?>/produits/gerer_Unite_Mesure_Produit_Ajax";
    var url_add_mesure_conversions_by_product_ajax = "<?php echo  b_url; ?>/mesure_conversions/add_unite_produit_Or_And_unite_Mesure";
</script>
<script>
    $(document).ready(function() {
        $(".preloader").fadeOut();

        $('#default-datatable').DataTable();
        var table = $('#example').DataTable({
            lengthChange: false,
            buttons: ['copy', 'excel', 'pdf', 'print', 'colvis'],

            <?php echo  OPTION_FR ?>,
        });
        table.buttons().container()
            .appendTo('#example_wrapper .col-md-6:eq(0)');

        $('.buttons-colvis').find('span').text('Filtrer')


    });
</script>

<div class="modal fade" id="id-modal-edit-produits">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Modifier produit</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-inverse-primary kl-dismiss-modal" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                <button type="submit" name="btn_save_produit" class="btn btn-primary shadow-primary px-5 kl-save-produit"><i class="fa fa-check-square-o"></i>Enregistrer modification</button>

            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="id-modal-gerer-unite-mesure-produits">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">gérer unité des mesures</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-inverse-primary kl-dismiss-modal" data-dismiss="modal"><i class="fa fa-times"></i> Fermer</button>
                <!-- <button type="submit" name="btn_save_produit" class="btn btn-primary shadow-primary px-5 kl-save-produit"><i class="fa fa-check-square-o"></i>Enregistrer modification</button> -->

            </div>
        </div>
    </div>
</div>