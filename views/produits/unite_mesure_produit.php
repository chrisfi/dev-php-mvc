<div class="col-lg-12">
    <div class="card">
        <div class="card-header text-uppercase text-secondary">Liste des produits avec les unités de mesure</div>
        <?php
        $totalPage = ceil($totalPage / 5);
        ?>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table">
                    <thead class="thead-secondary">
                        <tr>
                            <th scope="col">Libelle</th>
                            <th scope="col">Unite de mesure</th>
                            <th scope="col">Prix d'achat</th>
                            <th scope="col">Prix Ucodis</th>
                            <th scope="col">Prix Detaille</th>

                        </tr>
                    </thead>
                    <tbody>
                        <?php

                        foreach ($produits as $prod) { ?>
                            <tr>
                                <th scope="row"><?php echo $prod['prod_libell'] ?></th>
                                <td><?php echo $prod['unite_mesure_code'] ?></td>
                                <td><?php echo $prod['prod_unit_prix_achat'] ?></td>
                                <td><?php echo $prod['prod_unit_prix_unit_ucodis'] ?></td>
                                <td><?php echo $prod['prod_unit_prix_unit_detaille'] ?></td>
                            </tr>
                        <?php }
                        ?>

                    </tbody>

                </table>
            </div>

            
        </div>
    </div>

        <div class="row">
            <div class="col-lg-12 ">
                <ul class="pagination pagination-separate pagination-outline-secondary">
                    <?php

                    for ($i = 0; $i < $totalPage; $i++) {
                        if ($i == 0) {
                    ?>
                            <li class="page-item active"><a class="page-link " href="javascript:void();">Previous</a></li>
                        <?php
                        } elseif ($i == $totalPage - 1) {
                        ?>
                            <li class="page-item"><a class="page-link" href="javascript:void();">Suivante</a></li>
                        <?php
                        } else { ?>
                            <li class="page-item"><a class="page-link" href="javascript:void();"><?php echo $i ?></a></li>
                    <?php  }
                    }
                    ?>
                </ul>
            </div>
        </div>
</div>