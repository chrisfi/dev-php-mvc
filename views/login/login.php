<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from codervent.com/rukada/color-admin/authentication-signin.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 09 Nov 2019 15:12:17 GMT -->

<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <meta name="description" content="" />
  <meta name="author" content="" />
  <title>login</title>
  <!--favicon-->
  <link rel="icon" href="<?php echo URL; ?>/assets/images/favicon.ico" type="image/x-icon">
  <!-- Bootstrap core CSS-->
  <link href="<?php echo URL; ?>/assets/css/bootstrap.min.css" rel="stylesheet" />
  <!-- animate CSS-->
  <link href="<?php echo URL; ?>/assets/css/animate.css" rel="stylesheet" type="text/css" />
  <!-- Icons CSS-->
  <link href="<?php echo URL; ?>/assets/css/icons.css" rel="stylesheet" type="text/css" />
  <!-- Custom Style-->
  <link href="<?php echo URL; ?>/assets/css/app-style.css" rel="stylesheet" />

  <link href="<?php echo URL; ?>/assets/css/style.min.css" rel="stylesheet" />

</head>

<body class="bg-dark">

  <div class="preloader">
    <div class="lds-ripple">
      <div class="lds-pos"></div>
      <div class="lds-pos"></div>
    </div>
  </div>
  <!-- Start wrapper-->
  <div id="wrapper">
    <div class="card card-authentication1 mx-auto my-5">
      <div class="card-body">
        <div class="card-content p-2">
          <div class="text-center">
            <img src="<?php echo URL; ?>/assets/images/logo-icon.png" alt="logo icon">
          </div>

          <div class="card-title text-uppercase text-center py-3">Authentification</div>
          <form id="form-login" action="" method="POST" autocomplete="off">
            <div class="form-group">
              <label for="exampleInputUsername" class="">Login</label>
              <div class="position-relative has-icon-right">
                <input type="text" id="id_login" name="name" class="form-control input-shadow" placeholder="Enter Username">
                <div class="form-control-position">
                  <i style="line-height: 4;" class="icon-user"></i>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label for="exampleInputPassword" class="">Mot de passe</label>
              <div class="position-relative has-icon-right">
                <input type="password" id="id_password" name="paassword" class="form-control input-shadow" placeholder="Enter Password">
                <div class="form-control-position">
                  <i style="line-height: 4;" class="icon-lock"></i>
                </div>
              </div>
            </div>
            <div class="card-title  text-center kl-text-choisir-type-vente">Veuillez choisir le type vente</div>
            <div class="form-group d-flex justify-content-between">

              <div class="icheck-material-primary icheck-inline">
                <input type="radio" id="inline-radio-primary" class="kl-ucodus" value="ucodus" name="ucodus_or_detaille">
                <label for="inline-radio-primary">Ucodis</label>
              </div>
              <div class="icheck-material-info icheck-inline">
                <input type="radio" id="inline-radio-info" class="kl-detaille" value="detaille" name="ucodus_or_detaille">
                <label for="inline-radio-info">Détaille</label>
              </div>
            </div>

            <button type="submit" name="login" class="btn btn-primary shadow-primary btn-block waves-effect waves-light kl-authentification">s'authentifier</button>


          </form>
        </div>
      </div>

    </div>

    <!--Start Back To Top Button-->
    <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
    <!--End Back To Top Button-->
  </div>
  <!--wrapper-->

  <!-- Bootstrap core JavaScript-->
  <script src="<?php echo URL; ?>/assets/js/jquery.min.js"></script>
  <script src="<?php echo URL; ?>/assets/js/popper.min.js"></script>
  <script src="<?php echo URL; ?>/assets/js/bootstrap.min.js"></script>
  <script src="<?php echo URL; ?>/assets/plugins/alerts-boxes/js/sweetalert.min.js"></script>
  <script src="<?php echo URL; ?>/assets/plugins/alerts-boxes/js/sweet-alert-script.js"></script>
  <script src="<?php echo URL; ?>/assets/plugins/notifications/js/lobibox.min.js"></script>
  <script src="<?php echo URL; ?>/assets/plugins/notifications/js/notifications.min.js"></script>
  <script src="<?php echo URL; ?>/assets/plugins/notifications/js/notification-custom-script.js"></script>
  <script>
    var url_authentificate = "<?php b_url ?>";
  </script>
  <script src="<?php echo URL; ?>/assets/js_stock/login.js"></script>

</body>

<!-- Mirrored from codervent.com/rukada/color-admin/authentication-signin.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 09 Nov 2019 15:12:17 GMT -->

</html>