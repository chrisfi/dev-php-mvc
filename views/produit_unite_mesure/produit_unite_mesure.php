<!-- <div class="col-lg-12 kl-data-table-unite-mesure"> -->
<?php
// require_once(rot . ds . 'helpers/produit_unite_mesureHelpers.php');
// $produit_unite_mesureHelpers = new produit_unite_mesureHelpers();
// $produit_unite_mesureHelpers->list_pagination(0, 5);
?>
<!-- </div> -->


<div class="card">
    <div class="row">
        <div class="col">
            <div class="card-header text-uppercase text-secondary">Liste des produits avec les unités de mesure</div>
        </div>
        <div class="col">
            <div class="card-header text-uppercase text-secondaryt">
                <ul class="navbar-nav mr-auto align-items-center">

                    <li class="nav-item">
                        <form class="search-bar">
                            <input style="width: 642px!important;" type="text" class="form-control kl-text-input-recherche-prod-unite-mesure" placeholder="Entrer votre recherche">
                            <a href="javascript:void();"><i class="icon-magnifier"></i></a>
                        </form>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="kl-data-table-unite-mesure">
        <?php
        require_once(rot . ds . 'helpers/produit_unite_mesureHelpers.php');
        $produit_unite_mesureHelpers = new produit_unite_mesureHelpers();
        $produit_unite_mesureHelpers->list_pagination(0, 5);
        ?>
    </div>
</div>
<script>
    var produit_unite_mesure_Ajax = "<?php echo b_url ?>/produit_unite_mesure/pagination";
    var produit_unite_mesure_chercher_Ajax = "<?php echo b_url ?>/produit_unite_mesure/chercher";
</script>
<script src="<?php echo URL; ?>/assets/js/jquery.min.js"></script>
<script src="<?php echo URL; ?>/assets/js_stock/produit_unite_mesure.js"></script>