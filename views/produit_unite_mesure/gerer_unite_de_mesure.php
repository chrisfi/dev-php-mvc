<?php
$this->loadModel('produit');
$this->loadModel('unitemesure');
$nitemesures = $this->unitemesure->get_all();
// var_dump($nitemesures);
$produits = $this->produit->get_all_produit_join_unite_de_mesure();

$id_produit = 0;
$unite_mesure_code = 0;
$unite_mesure_base = 0;
$unite_mesure_id = 0;
if (isset($_GET['id_produit'])) {
    $id_produit = $_GET['id_produit'];
    $Prod = $this->produit->get_produit_by_id($_GET['id_produit']);
    $unite_mesure = $this->unitemesure->get_Unite_Mesure_by_id($Prod['produit_unit_mes_id']);
    $unite_mesure_code = $unite_mesure['unite_mesure_code'];
    $unite_mesure_id = $unite_mesure['unite_mesure_id'];
    // var_dump($unite_mesure);
    $unite_mesure_base = $unite_mesure['unite_mesure_id'];
    // var_dump( $mes);
}


?>
<form action="" method="post">
    <div class="row">
        <div class="col-lg-12">
            <div class="card border border-primary">
                <div class="card-body text-center">
                    <h3><span class="text-primary">Gérer unité de mesure/conversion</span></h3>
                </div>
            </div>
        </div>

        <div class="col-lg-12">
            <div class="card border border-info">
                <div class="card-body ">
                    <div class="form-group ">
                        <label>Selecter le produit pour ajouter unite de mesure</label>
                        <select class="form-control single-select kl-select-product" name="produit_id" required>
                            <option value="0"></option>
                            <?php foreach ($produits as $produit) {
                            ?>
                                <option <?php echo ($id_produit == $produit['produit_id']) ? 'selected' : '' ?> value="<?php echo $produit['produit_id']; ?>"><?php echo $produit['produit_libelle'] ?> ( <?php echo $produit['unite_mesure_libelle'] ?>)</option>

                            <?php   } ?>
                        </select>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title text-center"><span class="text-primary">Ajouter unité de conversion </span></h5>

                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th scope="col">Unite de mesure(à converser)</th>
                                    <th scope="col">Valeur</th>
                                    <th scope="col">Unite de mesure(Base)</th>
                                    <th style="width: 5%;" scope="col"></th>

                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td scope="row">
                                        <div class="form-group">
                                            <select class="form-control single-select kl-unite-mesure-to-converse" id="ID_UNITEMESURE_ID" name="unite_mesure_id_a_converter" required>
                                                <?php foreach ($nitemesures as $nitemesure) {
                                                ?>
                                                    <option value="<?php echo $nitemesure['unite_mesure_id']; ?>"><?php echo $nitemesure['unite_mesure_libelle']; ?></option>

                                                <?php } ?>
                                            </select>
                                        </div>
                                    </td>
                                    <td scope="row" style="width: 253px;">
                                        <div class="form-group">
                                            <input class="form-control" id="id-value-of-conversion" step="any" type="number" min='0' name="valeur_converser" required>
                                        </div>
                                    </td>
                                    <td scope="row">
                                        <div class="form-group">
                                            <input type="text" class="form-control" value="<?php echo $unite_mesure_code ?>" id="id_unite_mesure_code" disabled required placeholder="Entrer nom">
                                            <input type="hidden" id="id_unite_mesure_base" data-unite_mesure_base="<?php echo $unite_mesure_base ?>" value="<?php echo $unite_mesure_base ?>" name="unite_mesure_base">

                                        </div>
                                    </td>
                                    <td>
                                        <input type="hidden" value="<?php echo $unite_mesure_id ?>" id="id-unite_mesure_id_unite" name="unite_mesure_id_unite">
                                        <button type="submit" class="btn btn-info shadow-info waves-effect waves-light m-1">Ajouter</button>
                                    </td>

                                </tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>

        <div class="col-lg-12">
            <div class="card border border-info">
                <div class="card-body text-center kl-content-result-product">
                    <?php
                    if (isset($_GET['id_produit'])) {
                        require_once(rot . ds . 'helpers/produit_unite_mesureHelpers.php');
                        $produit_unite_mesureHelpers = new produit_unite_mesureHelpers();
                        $produit_unite_mesureHelpers->get_product_unite($_GET['id_produit']);
                    }
                    ?>
                </div>
            </div>
        </div>

    </div>

</form>


<script src="<?php echo URL; ?>/assets/js/jquery.min.js"></script>
<script>
    var gerer_unite_mesure_produit_Ajax = "<?php echo b_url ?>/produit_unite_mesure/gerer_unite_mesure_produit";
</script>
<script src="<?php echo URL; ?>/assets/js_stock/produit_unite_mesure.js">
</script>