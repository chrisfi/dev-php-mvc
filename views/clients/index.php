<link rel="stylesheet" href="<?php echo URL; ?>/assets/js_stock/css/client.css">
<link href="<?php echo URL; ?>/assets/css/style.min.css" rel="stylesheet" />
<div class="row">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-header">
        <div class="row">
          <div class="col"></div>
          <div class="col">
            <i class="fa fa-table"></i> Liste des clients
          </div>
          <div class="col"><a href="<?php echo b_url ?>/clients/add" type="button" class="btn btn-dark shadow-dark waves-effect waves-light m-1 float-right">Ajouter nouveau client</a></div>
        </div>
      </div>

      <div class="row kl-action-button d-none " style="right: 42px">

        <button class="btn-social btn-social-circle btn-outline-dribbble waves-effect waves-light m-1 kl-delet-client"><i class="fa fa-trash-o"></i></a>


          <button class="btn-social btn-social-circle btn-outline-skype waves-effect waves-light m-1 kl-edit-client"><i class="zmdi zmdi-edit"></i></a>

      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table id="example" class="table table-bordered">
            <thead>
              <tr>
                <th>Id</th>
                <th>Numero</th>
                <th>Nom</th>
                <th>Prenom</th>
                <th>Nif</th>
                <th>Stat</th>
                <th>Adresse</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($clients as $client) { ?>
                <tr>
                  <td><?php echo $client['client_id']; ?></td>
                  <td><?php echo $client['client_num']; ?></td>
                  <td><?php echo $client['client_nom'] ?></td>
                  <td><?php echo $client['client_prenom'] ?></td>
                  <td><?php echo $client['client_nif'] ?></td>
                  <td><?php echo $client['client_stat'] ?></td>
                  <td><?php echo $client['client_adress'] ?></td>
                  <td>
                    <button type="button" data-id="<?php echo $client['client_id'] ?>" class="btn btn-primary shadow-primary waves-effect waves-light m-1 kl-edit-client">Editer</button>
                    <button type="button" class="btn btn-danger shadow-danger waves-effect waves-light m-1">Suprimer</button>
                  </td>
                </tr>
              <?php }
              ?>
            </tbody>

          </table>
        </div>
      </div>
    </div>
  </div>
</div><!-- End Row-->
<!-- Bootstrap core JavaScript-->


<script src="<?php echo URL; ?>/assets/js/jquery.min.js"></script>
<script src="<?php echo URL; ?>/assets/js_stock/client.js"></script>

<script>
  var url_delete = "delete";
  var url_get_cli = "get_cli";
  var url_client_ajax = "add";
</script>

<script>
  $(document).ready(function() {
    $(".preloader").fadeOut();
    $('#default-datatable').DataTable();
    var table = $('#example').DataTable({
      lengthChange: false,
      buttons: ['copy', 'excel', 'pdf', 'print', 'colvis'],
      <?php echo  OPTION_FR ?>,
    });
    table.buttons().container()
      .appendTo('#example_wrapper .col-md-6:eq(0)');

    $('.buttons-colvis').find('span').text('Filtrer')

  });
</script>

<div class="modal fade" id="id-modal-add-edit-client">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Modifier client</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <?php require_once(rot . ds . 'views/clients/form.php'); ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-inverse-primary kl-dismiss-modal" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
        <button type="button" name="btn_save_client" class="btn btn-primary kl-save-client"><i class="fa fa-check-square-o"></i>Enregistrer modification</button>
      </div>
    </div>
  </div>
</div>