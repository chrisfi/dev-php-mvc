
<form  action="" method="POST">
    <div class="form-group">
        <label for="input-1">Nom</label>
        <input type="text" class="form-control" id="ID_CLIENT_NOM" name="CLIENT_NOM" required placeholder="Entrer nom">
    </div>
    <div class="form-group">
        <label for="input-2">Prenom</label>
        <input type="text" class="form-control" id="ID_CLIENT_PRENOM" name="CLIENT_PRENOM" placeholder="Entrer prenom">
    </div>
    <div class="form-group">
        <label for="input-3">NIF</label>
        <input type="text" class="form-control" id="ID_CLIENT_NIF" name="CLIENT_NIF" placeholder="Entrer NIF">
    </div>
    <div class="form-group">
        <label for="input-4">STAT</label>
        <input type="text" class="form-control" id="ID_CLIENT_STAT" name="CLIENT_STAT" placeholder="Entrer STAT">
    </div>
    <div class="form-group">
        <label for="input-5">ADRESSE</label>
        <input type="text" class="form-control" id="ID_CLIENT_ADRESSE" name="CLIENT_ADRESSE" placeholder="Adresse">
    </div>
    <div class="form-group kl-btn-form-default">
        <input type="hidden" id="ID_CLIENT_ID" value="0" name="CLIENT_ID">
        <input type="hidden" id="ID_CLIENT_NUM" value="0" name="CLIENT_NUM">
        <button type="submit" name="btn_save_client" value="add" class="btn btn-primary shadow-primary px-5 kl-save-client"><i class="icon-lock"></i>Enregistrer</button>
    </div>
</form>
