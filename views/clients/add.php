<div class="card-body">
    <div class="card-title text-primary">Ajouter nouveau client</div>
    <hr>
    <?php require_once(rot . ds . 'views/clients/form.php');

    ?>
</div>
<script src="<?php echo URL; ?>/assets/js/jquery.min.js"></script>
<script src="<?php echo URL; ?>/assets/js/popper.min.js"></script>
<script src="<?php echo URL; ?>/assets/js/bootstrap.min.js"></script>
<script>
    var url_client_ajax = "<?php echo parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH); ?>";
</script>
<script src="<?php echo URL; ?>/assets/js_stock/client.js"></script>