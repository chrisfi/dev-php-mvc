<!-- <link rel="stylesheet" href="<?php echo URL; ?>/assets/js_stock/css/unitemesures.css"> -->
<link href="<?php echo URL; ?>/assets/css/style.min.css" rel="stylesheet" />
<div class="row">
    <div class="col-lg-12">
        <?php
        if (!empty($reponse)) {
        ?>
            <!-- <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <div class="alert-icon contrast-alert">
                    <i class="fa fa-check"></i>
                </div>
                <div class="alert-message">
                    <span><strong>Success!</strong> This is a success alert—check it out!</span>
                </div>
            </div> -->
        <?php
        }
        ?>
        <div class="card">
            <div class="card-header"><i class="fa fa-table">
                </i>Liste des unite de mesures
            </div>
            <div class="row kl-action-button d-none " style="right: 42px">
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table id="example" class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Code</th>
                                <th>Nom</th>
                                <th>Description</th>
                                <th>Cageot</th>
                                <th>Action</th>
                        </thead>
                        <tbody>
                            <?php foreach ($unitemesures as $unitemesure) { ?>
                                <tr>
                                    <td><?php echo $unitemesure['unite_mesure_id']; ?></td>
                                    <td><?php echo $unitemesure['unite_mesure_code']; ?></td>
                                    <td><?php echo $unitemesure['unite_mesure_libelle']; ?></td>
                                    <td><?php echo $unitemesure['unite_mesure_description'] ?></td>
                                    <td><?php echo $unitemesure['unite_mesure_cageot'] ?></td>
                                    <td>
                                        <!-- <button type="button" data-toggle="modal" data-target="#update-unitmesure" class="btn btn-success waves-effect waves-light m-1"> <i class="zmdi zmdi-edit"></i> <span>Edite</span> </button>
                                        <button type="button" data-toggle="modal" data-target="#delete-unitmesure" class="btn btn-danger waves-effect waves-light m-1"> <i class="fa fa fa-trash-o"></i> <span>Suprimer</span> </button> -->
                                        <button type="button" data-id="<?php echo $unitemesure['unite_mesure_id'] ?>" class="btn btn-success waves-effect waves-light m-1 kl-edite-unite-mesures"> <i class="zmdi zmdi-edit"></i> <span>Edite</span> </button>
                                        <button type="button" data-id="<?php echo $unitemesure['unite_mesure_id'] ?>" class="btn btn-danger waves-effect waves-light m-1 kl-delete-unite-mesure"> <i class="fa fa fa-trash-o"></i> <span>Suprimer</span> </button>
                                    </td>
                                </tr>
                            <?php }
                            ?>

                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo URL; ?>/assets/js/jquery.min.js"></script>
<!-- <script src="<?php echo URL; ?>/assets/js_stock/unitemesures.js"></script> -->
<script>
    var url_unite_mesure = "<?php echo b_url ?>/unitemesures/get_action";
</script>
<script>
    $(document).ready(function() {
        $(".preloader").fadeOut();
        // $('#default-datatable').DataTable();
        var table = $('#example').DataTable({
            lengthChange: false,
            buttons: ['copy', 'excel', 'pdf', 'print', 'colvis'],
            <?php echo  OPTION_FR ?>,
        });
        table.buttons().container()
            .appendTo('#example_wrapper .col-md-6:eq(0)');

        $('.buttons-colvis').find('span').text('Filtrer')

    });
</script>

<div class="modal fade" id="delete-unitmesure" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated rollIn">
            <div class="modal-header">
                <h5 class="modal-title">Supression</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Etes vous sure</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i>Fermer</button>
                <button type="button" class="btn btn-success"><i class="fa fa-check-square-o"></i>Suprimer</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="update-unitmesure" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Modifier unité de mesure</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="row mb-3">
                        <div class="col">
                            <div class="form-group">
                                <label for="ID_PRODUIT_LIBELLE">Libelle</label>
                                <input type="text" class="form-control" id="ID_UNITEMESURELIB" name="UNITEMESURELIB" required placeholder="Entrer nom">
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label>Code</label>
                                <input id="ID_UNITEMESURECODE" class="form-control" type="text" value="" name="UNITEMESURECODE" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label>Description</label>
                                <input id="ID_UNITEMESURE_DESC" class="form-control" step="any" name="UNITEMESURE_DESC">
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col">
                            <div class="form-group py-2">
                                <div class="icheck-material-success">
                                    <input type="checkbox" id="ID_UNIT_CAG_CHECKBOX" name="UNIT_CAG">
                                    <label for="ID_UNIT_CAG_CHECKBOX">Mesure Cageot :</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="hidden" id="ID_UNITEMESURE_ID" value="0" name="UNITEMESURE_ID">
                        <button type="submit" name="btn_save_unitemesures" class="btn btn-info shadow-info px-5 kl-save-unitemesures"><i class="icon-lock"></i>Modifier</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>