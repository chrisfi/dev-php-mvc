<form id="form-produit" action="" method="POST">

    <div class="row mb-3">
        <div class="col">
            <div class="form-group">
                <label for="ID_PRODUIT_LIBELLE">Libelle</label>
                <input type="text" class="form-control" id="ID_UNITEMESURELIB" name="UNITEMESURELIB" required placeholder="Entrer nom">
            </div>
        </div>
    </div>
    <div class="row mb-3">
        <div class="col">
            <div class="form-group">
                <label>Code</label>
                <input id="ID_UNITEMESURECODE" class="form-control" type="text" value="" name="UNITEMESURECODE" required>
            </div>
        </div>
        <div class="col">
            <div class="form-group">
                <label>Description</label>
                <input id="ID_UNITEMESURE_DESC" class="form-control" step="any" name="UNITEMESURE_DESC">
            </div>
        </div>
    </div>


    <div class="row mb-3">
        <div class="col">
            <div class="form-group py-2">
                <div class="icheck-material-success">
                    <input type="checkbox" id="ID_UNIT_CAG_CHECKBOX" name="UNIT_CAG">
                    <label for="ID_UNIT_CAG_CHECKBOX">Mesure Cageot :</label>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group kl-btn-form-default">
        <input type="hidden" id="ID_UNITEMESURE_ID" value="0" name="UNITEMESURE_ID">
        <button type="submit" name="btn_save_unitemesures" class="btn btn-primary shadow-primary px-5"><i class="icon-lock"></i>Enregistrer</button>
        <!-- <button type="submit" name="btn_save_unitemesures" class="btn btn-primary shadow-primary px-5 kl-save-unitemesures"><i class="icon-lock"></i>Enregistrer</button> -->
    </div>
</form>