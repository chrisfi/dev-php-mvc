<link href="<?php echo URL; ?>/assets/css/style.min.css" rel="stylesheet" />
<link href="<?php echo URL; ?>/assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css">
<div class="card-body">
    <div class="card-title text-primary">Entrée produit en stock</div>
    <hr>
    <?php
    // var_dump($URL__AJAX);
    require_once(rot . ds . 'views/entre_produits/form.php');

    ?>
</div>
<script src="<?php echo URL; ?>/assets/js/jquery.min.js"></script>
<script src="<?php echo URL; ?>/assets/js/popper.min.js"></script>
<script src="<?php echo URL; ?>/assets/js/bootstrap.min.js"></script>

<script>
    var URL__AJAX = "<?php echo $URL__AJAX ?>"
</script>
<script src="<?php echo URL; ?>/assets/js_stock/entre_produits.js"></script>

<!--URL__AJAX-->
<script src="<?php echo URL; ?>/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>

<script>
    $('#id-date_entre').datepicker({
        autoclose: true,
        todayHighlight: true,
        language: 'fr',
    });

</script>