<?php
$this->loadModel('Produit_unitme');
$this->loadModel('Fournisseur');
$produits = $this->Produit_unitme->get_all_produit_distict();
$Fournis = $this->Fournisseur->getAll();

?>

<div class="row justify-content-md-center">

    <div class="card border border-info w-100">
        <div class="card-body">
            <form action="" method="post">

                <div class="row">

                    <div class="col-lg-12">
                        <div class="form-group">
                            <label class="text-info">Fournisseur</label>
                            <select class="form-control single-select" id="fournisseur_id" name="fournisseur_id">
                                <option value="0"></option>
                                <?php foreach ($Fournis as $Fourni) { ?>
                                    <option value="<?php echo $Fourni['id_fournisseur']; ?>"><?php echo $Fourni['lib_fournisseur'] . ' ' . $Fourni['adress_fournisseur'] ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="form-group">
                            <label class="text-info">Produit</label>
                            <select class="form-control single-select" id="id_produit_entre" name="id_produit">
                                <option value="0"></option>
                                <?php foreach ($produits as $produit) { ?>
                                    <option value="<?php echo $produit['prod_id']; ?>"><?php echo $produit['prod_libell']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-12" id="id-content-list-unite-mesure">
                        <div class="form-group">
                            <label class="text-info">Unité de mesure correspondant</label>
                            <select class="form-control single-select kl_UNITEMESURE_ID" id="ID_UNITEMESURE_ID" name="unite_mesure_id" required>
                                <option value=""></option>
                            </select>
                        </div>
                    </div>


                </div>
                <div class="row">
                    <div class="col-lg-4 ">
                        <div class="form-group">
                            <label class="text-info">Prix d'achat</label>
                            <input type="number" id="id-qte-en-stock" name="prix_achat" class="kl-input_decimal" min='1' step="any" value="0.00" required class="form-control">
                        </div>
                    </div>
                    <div class="col-lg-4 kl-qte-restant-produit">
                        <div class="form-group">
                            <label class="text-info">Quantité en stock</label>
                            <input type="number" id="id-qte-en-stock" class="kl-input_decimal" step="any" min='1' value="0.00" disabled="disabled" class="form-control">
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label class="text-info">QUANTITE ENTRANT</label>
                            <input id="qte_entre" class="kl-input_decimal" type="number" step="any" min='1' value="1.00" name="qte_entre" required>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>Date</label>
                            <input type="text" value="<?php echo date('d/m/Y', time()) ?>" id="id-date_entre" name="date_entre" class="form-control">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <button type="submit" name="btn_save_mesure_conversion" id="id-valide-produit-entre" class="btn btn-success shadow-primary w-100 mt-4 px-5 ">Valider</button>
                    </div>
                </div>
            </form>
        </div>
    </div>



</div>