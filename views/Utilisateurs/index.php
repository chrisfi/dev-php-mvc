<?php
// var_dump($users);
// var_dump($acces_ventes);
// var_dump($roles);
?>
<div class="card-body">
    <div class="table-responsive">
        <table id="example" class="table table-bordered">
            <thead>
                <tr>
                    <th class="text-center">Id</th>
                    <th class="text-center">Nom</th>
                    <th class="text-center">Prénoms</th>
                    <th class="text-center">Login</th>
                    <th class="text-center">Rôle</th>
                    <th class="text-center">Vente</th>
                    <th class="text-center">Action</th>
            </thead>
            <tbody>
                <?php foreach ($users as $user) { ?>
                    <tr>
                        <td><?php echo $user['user_id']; ?></td>
                        <td><?php echo $user['user_name']; ?></td>
                        <td><?php echo $user['user_prenom']; ?></td>
                        <td><?php echo $user['user_login']; ?></td>
                        <td>

                            <?php
                            $this->loadModel('User_role');
                            $rols = $this->User_role->get_All_role_by_id_user($user['user_id']);
                            ?>
                            <ul>
                                <?php
                                foreach ($rols as $rol) {
                                ?>
                                    <li><?php echo $rol['role_libelle'] ?></li>
                                <?php
                                }
                                ?>
                            </ul>
                        </td>
                        <td>
                            <?php
                            $this->loadModel('User_access_vente');
                            $User_access_ventes = $this->User_access_vente->get_All_Access_Vente_by_id_user($user['user_id']);
                            ?> <ul><?php
                                    foreach ($User_access_ventes as $vente) {
                                    ?>
                                    <li><?php echo $vente['type'] ?></li>
                                <?php
                                    }
                                ?>
                            </ul>
                        </td>
                        <td class="text-center">
                            <button type="button" data-id="<?php echo $user['user_id'] ?>" class="btn btn-info shadow-info btn-sm waves-effect waves-light m-1 kl-add-role-user">Role</button>
                            <button type="button" data-id="<?php echo $user['user_id'] ?>" class="btn btn-secondary  btn-sm shadow-secondary waves-effect waves-light m-1 btn-add-vente-access">Vente</button>
                            <a href="/Utilisateurs/add?id=<?php echo $user['user_id']; ?>" type="button" data-id="<?php echo $user['user_id'] ?>" class="btn btn-success btn-sm waves-effect waves-light m-1 kl-edite-unite-mesures"> <i class="zmdi zmdi-edit"></i> <span></span> </a>
                            <!-- <button type="button" data-id="<?php echo $user['user_id'] ?>" class="btn btn-danger btn-sm waves-effect waves-light m-1 kl-delete-user"> <i class="fa fa fa-trash-o"></i> <span></span> </button> -->

                        </td>
                    </tr>
                <?php }
                ?>

            </tbody>

        </table>
    </div>
</div>




<div class="modal fade" id="modal-add-role" style="display: none;" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated zoomInUp">
            <div class="modal-header">
                <h5 class="modal-title">Ajouter rôle</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="" method="post">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label>Selectionner le role</label>
                                <select class="form-control single-select" id="id__role" name="role" required>
                                    <option value="0"></option>
                                    <?php foreach ($roles as $role) {
                                    ?>
                                        <option value="<?php echo $role['role_id']; ?>"><?php echo $role['role_libelle']; ?></option>

                                    <?php  } ?>
                                </select>
                            </div>

                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal"><i class="fa fa-times"></i> Fermer</button>
                <button type="button" class="btn btn-success btn-sm kl-save-role-user"><i class="fa fa-check-square-o"></i> Enregistrer</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-add-vente" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated zoomInUp">
            <div class="modal-header">
                <h5 class="modal-title">Autorisation de vente</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="" method="post">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label>Selectionner type de vente</label>
                                <select class="form-control single-select" id="id__type" name="type" required>
                                    <option value="0"></option>
                                    <?php foreach ($acces_ventes as $acces_vente) {
                                    ?>
                                        <option value="<?php echo $acces_vente['id']; ?>"><?php echo $acces_vente['type']; ?></option>

                                    <?php  } ?>
                                </select>
                            </div>

                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal"><i class="fa fa-times"></i> Fermer</button>
                <button type="button" class="btn btn-success btn-sm kl-save-access-vente-user"><i class="fa fa-check-square-o"></i> Enregistrer</button>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo URL; ?>/assets/js/jquery.min.js"></script>
<script src="<?php echo URL; ?>/assets/js/popper.min.js"></script>
<script src="<?php echo URL; ?>/assets/js/bootstrap.min.js"></script>
<script>
    $(document).ready(function() {
        id_user_vente = 0;
        id_user_role = 0;

        $(document).on('click', '.btn-add-vente-access', function() {
            id_user_vente = $(this).data('id');
            $('#modal-add-vente').modal('show');
        });

        $('.kl-add-role-user').click(function(e) {
            id_user_role = $(this).data('id');
            $('#modal-add-role').modal('show');
        });

        $('.kl-save-access-vente-user').click(function(e) {

            var id__type = $('#id__type').val();
            if (id__type == 0) {
                return false;
            } else {
                var data = {};
                data.id_user_vente = id_user_vente;
                data.id__type = id__type;
                $.ajax({
                    url: 'add_user_vente',
                    data,
                    method: "POST",
                    dataType: "JSON",
                    beforeSend: function() {
                        console.log(data);
                        $(".preloader").fadeIn();
                    },
                    success: function(data) {
                        $(".preloader").fadeOut();
                        location.reload();
                    }
                });
            }

        });

        $('.kl-save-role-user').click(function(e) {

            var id__role = $('#id__role').val();
            if (id__role == 0) {
                return false;
            } else {
                var data = {};
                data.id_user_role = id_user_role;
                data.id__role = id__role;
                $.ajax({
                    url: 'add_role',
                    data,
                    method: "POST",
                    dataType: "JSON",
                    beforeSend: function() {
                        console.log(data);
                        $(".preloader").fadeIn();
                    },
                    success: function(data) {
                        $(".preloader").fadeOut();
                        location.reload();
                    }
                });
            }

        });


        // $(".kl-delete-user").click(function() {
        //     id_user = $(this).data('id');
        //     swal({
        //             title: "Vous êtes de suprimer cette utilisateur ?",
        //             text: "",
        //             icon: "warning",
        //             buttons: true,
        //             dangerMode: true,
        //         })
        //         .then((willDelete) => {
        //             if (willDelete) {
        //                 var data = {};
        //                 data.client_id = cli_id;
        //                 data.action = 'validation_cmd';
        //                 $(".preloader").fadeIn();
        //                 $.ajax({
        //                     url: 'delete_user',
        //                     data,
        //                     method: "POST",
        //                     dataType: "JSON",
        //                     beforeSend: function() {

        //                     },
        //                     success: function(response) {
        //                         res = response;

        //                     }
        //                 });

        //                 swal("Enregistration avec succèss!", {
        //                     icon: "success",
        //                     class: 'kl-delete-cli',
        //                 });
        //                 $(".preloader").fadeOut();
        //                 Add_notification_success('Commande enregistré avec succèss');
        //             } else {
        //                 swal("Enregistration annulé!");
        //             }
        //         });

        // });

    });
</script>