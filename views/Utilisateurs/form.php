<?php if (isset($_GET['id'])) {
    $this->loadModel('Users');
    $users = $this->Users->getUserById($_GET['id']);
    // var_dump($users);
} ?>
<div class="card  mx-auto my-5 " style="width: 70%;">
    <div class="card-body">
        <div class="card-content p-2">
            <div class="text-center">
                <img src="<?php echo URL; ?>/assets/images/logo-icon.png" alt="logo icon">
            </div>

            <div class="card-title text-uppercase text-center py-3">Création de nouveau utilisateur</div>
            <form id="formutilisateur" name="formutilisateur" action="" method="POST" autocomplete="off">

                <div class="form-group">
                    <label for="input-1">Nom</label>
                    <input type="text" class="form-control" value="<?php echo isset($users) ? $users['user_name'] : '' ?>" id="id_user_name" name="user_name" autocomplete="off" required placeholder="Entrer nom">
                </div>
                <div class="form-group">
                    <label for="input-2">Prenom</label>
                    <input type="text" class="form-control" id="id_user_prenom" value="<?php echo isset($users) ? $users['user_prenom'] : '' ?>" name="user_prenom" placeholder="Entrer prenom">
                </div>

                <div class="form-group">
                    <label for="exampleInputUsername" class="">Login</label>
                    <div class="position-relative has-icon-right">
                        <input type="text" id="id_user_login" name="user_login" value="<?php echo isset($users) ? $users['user_login'] : '' ?>" class="form-control input-shadow" required placeholder="Entrer votre login">
                        <div class="form-control-position">
                            <i style="line-height: 4;" class="icon-user"></i>
                        </div>
                    </div>
                </div>

                <?php if (isset($users)) {
                ?>

                    <div class="form-group">
                        <label for="exampleInputPassword" class=""> Mot de passe</label>
                        <div class="position-relative has-icon-right">
                            <input type="password" id="id_user_pwd"  name="user_pwd" class="form-control input-shadow" placeholder="Entrer votre mot de passe">
                            <div class="form-control-position">
                                <i style="line-height: 4;" class="icon-lock"></i>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="exampleInputPassword" class="">Confirmer nouveau Mot de passe</label>
                        <div class="position-relative has-icon-right">
                            <input type="password" id="id_confirm_user_pwd" name="confirm_user_pwd"  class="form-control input-shadow" placeholder="Confirme mot de passe">
                            <div class="form-control-position">
                                <i style="line-height: 4;" class="icon-lock"></i>
                            </div>
                        </div>
                    </div>
                <?php
                } else { ?>
                    <div class="form-group">
                        <label for="exampleInputPassword" class="">Mot de passe</label>
                        <div class="position-relative has-icon-right">
                            <input type="password" id="id_user_pwd" name="user_pwd" class="form-control input-shadow" required placeholder="Entrer votre mot de passe">
                            <div class="form-control-position">
                                <i style="line-height: 4;" class="icon-lock"></i>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="exampleInputPassword" class="">Confirmer Mot de passe</label>
                        <div class="position-relative has-icon-right">
                            <input type="password" id="id_confirm_user_pwd" name="confirm_user_pwd" required class="form-control input-shadow" placeholder="Confirme mot de passe">
                            <div class="form-control-position">
                                <i style="line-height: 4;" class="icon-lock"></i>
                            </div>
                        </div>
                    </div>
                <?php } ?>



                <input type="hidden" value="<?php echo isset($users) ? $users['user_id'] : '0' ?>" id="id_user_id" name="user_id">
                <input type="hidden" value="<?php echo isset($users) ? $users['user_id'] : '0' ?>" id="user_pwd_old" name="user_pwd_old">
                <button type="submit" class="btn btn-primary shadow-primary btn-block waves-effect waves-light add-new-utilisateur">Enregistrer</button>


            </form>
        </div>
    </div>

</div>