<link href="<?php echo URL; ?>/assets/css/app-style.css" rel="stylesheet" />
<style>
    .content-wrapper{
        background: #713535;
    }
</style>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="text-center error-pages">
                <h1 class="error-title text-white"> 403</h1>
                <h2 class="error-sub-title text-white">Forbidden error</h2>

                <p class="error-message text-white text-uppercase">You don't have permission to access on this server</p>

                <div class="mt-4">

                </div>

                <div class="mt-4">
                    <p class="text-white">Copyright © 2021  | All rights reserved.</p>
                </div>
                <hr class="w-50">
                <div class="mt-2">
                    
                </div>
            </div>
        </div>
    </div>
</div>