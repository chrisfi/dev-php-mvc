<link href="<?php echo URL; ?>/assets/css/style.min.css" rel="stylesheet" />
<div class="container-fluid">
    
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
 

    <div class="row">
        <div class="col-lg-12">
          <div class="card">
               <div id="carousel-A" class="carousel slide" data-ride="carousel">
                  <ul class="carousel-indicators">
                    <li data-target="#demo-a" data-slide-to="0" class="active"></li>
                    <li data-target="#demo-b" data-slide-to="1" class=""></li>
                    <li data-target="#demo-c" data-slide-to="2" class=""></li>
                  </ul>
                  <div class="carousel-inner">
                    <div class="carousel-item active">
                      <img class="d-block w-100" src="<?php echo URL; ?>/assets/images/gallery/slider-a.jpg" alt="Los Angeles">
                      <div class="carousel-caption">
                        <h3 class="text-white">Los Angeles</h3>
                        <p>We had such a great time in LA!</p>
                      </div>   
                    </div>
                    <div class="carousel-item">
                      <img class="d-block w-100" src="<?php echo URL; ?>/assets/images/gallery/slider-b.jpg" alt="Chicago">
                      <div class="carousel-caption">
                        <h3 class="text-white">Chicago</h3>
                        <p>Thank you, Chicago!</p>
                      </div>   
                    </div>
                    <div class="carousel-item">
                      <img class="d-block w-100" src="<?php echo URL; ?>/assets/images/gallery/slider-c.jpg" alt="New York">
                      <div class="carousel-caption">
                        <h3 class="text-white">New York</h3>
                        <p>We love the Big Apple!</p>
                      </div>   
                    </div>
                  </div>
                  <a class="carousel-control-prev" href="#carousel-A" data-slide="prev">
                    <span class="carousel-control-prev-icon"></span>
                  </a>
                  <a class="carousel-control-next" href="#carousel-A" data-slide="next">
                    <span class="carousel-control-next-icon"></span>
                  </a>
                </div>
          </div>
        </div>
      </div>
</div>
<script src="<?php echo URL; ?>/assets/js/jquery.min.js"></script>
<script>
    $(document).ready(function() {
        $(".preloader").fadeOut();
    });
</script>