$(document).ready(function () {

    $('.add-new-utilisateur').click(function (e) {
        var pass = $('#id_user_pwd').val();
        var confirm_pass = $('#id_confirm_user_pwd').val();
        var user_name = $('#id_user_name').val();
        var user_prenom = $('#id_user_prenom').val();
        var user_login = $('#id_user_login').val();
        var id_user_id = $('#id_user_id').val();
        if (pass != confirm_pass) {
            e.preventDefault();
            Error_notification_danger('Mot de passe invalidé');
            return false;
        } else {
            var myForm = {};
            myForm.pass = pass;
            myForm.confirm_pass = confirm_pass;
            myForm.user_name = user_name;
            myForm.user_prenom = user_prenom;
            myForm.user_login = user_login;
            myForm.user_id = id_user_id;
            e.preventDefault();
            $.ajax({
                url: 'save_new_user',
                data: myForm,
                method: "POST",
                dataType: "html",
                beforeSend: function () {
                    $(".preloader").fadeIn();

                },
                success: function (response) {
                    console.log(response);
                    response = JSON.parse(response);
                    $(".preloader").fadeOut();
                    if (response['status'] == true) {
                        Add_notification_success('Nouveau utilisateur avec succèss');
                    } else {
                        Error_notification_danger("Erreur d'ajouter utilisateur ");
                    }

                    // $(".preloader").fadeIn();
                }
            });
        }
    });


});