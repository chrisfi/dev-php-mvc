
$(document).ready(function () {
    var id_product_selectionee = 0;
    // alert($('.kl-select-product').val());
    $('.kl-select-product').trigger('change');
    // $('.kl-unite-mesure-to-converse').trigger('change');
    $('.kl-a-link-pagination').click(function (e) {
        e.preventDefault();
        var query = $('.kl-text-input-recherche-prod-unite-mesure').val();
        var page_no = $(this).data('id');
        var data = {};
        data.page_no = page_no;
        data.limit = 5;
        data.searche = query;
        $.ajax({
            url: produit_unite_mesure_Ajax,
            data,
            method: "POST",
            dataType: "html",
            beforeSend: function () {
                // $(".preloader").fadeIn();
            },
            success: function (response) {
                $('.kl-data-table-unite-mesure').empty().html(response);
                // $(".preloader").fadeOut();
            }
        });

    });

    $('.kl-text-input-recherche-prod-unite-mesure').keyup(function () {
        var search = $(this).val();
        console.log(search);
        if (search != '') {
            load_data(search);
        }
        else {
            load_data();
        }
    });

    $('.kl-select-product').change(function () {
        var id_produit = $(this).val();
        id_product_selectionee = id_produit;
        get_mesure_unite_by_id(id_produit);
        var data = {};
        data.id_produit = id_produit;
        data.action = 'get_list_unite_mesure_produit';
        $.ajax({
            url: gerer_unite_mesure_produit_Ajax,
            data,
            method: "POST",
            dataType: "html",
            beforeSend: function () {
                $(".preloader").fadeIn();
            },
            success: function (response) {
                $('.kl-content-result-product').empty().html(response);
                $(".preloader").fadeOut();

            }
        });
    });


    $('.kl-unite-mesure-to-converse').change(function (e) {
        var val = $(this).val();
        var id_unite_mesure_code = $('#id_unite_mesure_base').val();
        var data = {};
        data.converser = val;
        data.unite_mesure_code = id_unite_mesure_code;
        $.ajax({
            url: 'get_val_uniteMesureToConverser__unite_base',
            data,
            method: "POST",
            dataType: "json",
            beforeSend: function () {
            },
            success: function (response) {

                // console.log(response['CONVERSION']);
                $('#id-value-of-conversion').val(response['CONVERSION']);
            }
        });

    });

});

function load_data(query) {
    var data = {};
    data.page_no = 10;
    data.limit = 5;
    data.searche = query;
    data.action = 'rechercher';
    console.log(data);
    $.ajax({
        url: produit_unite_mesure_chercher_Ajax,
        data,
        method: "POST",
        dataType: "html",
        beforeSend: function () {
            // $(".preloader").fadeIn();
        },
        success: function (response) {
            console.log(response);
            $('.kl-data-table-unite-mesure').empty().html(response);
            // $(".preloader").fadeOut();
        }
    });
}

function get_mesure_unite_by_id(id) {
    var data = {};
    data.id_produit = id;
    // data.get_unite_mesure_by_id_product = id;
    data.action = 'get_unite_mesure_by_id_product';
    $.ajax({
        url: gerer_unite_mesure_produit_Ajax,
        data,
        method: "POST",
        dataType: "json",
        beforeSend: function () {
            // $(".preloader").fadeIn();
        },
        success: function (response) {
            // $(".preloader").fadeOut();lg
            console.log(response);
            $('#id_unite_mesure_code').val(response['unite_mesure']['unite_mesure_code']);
            $('#id-unite_mesure_id_unite').val(response['unite_mesure']['unite_mesure_id']);
        }
    });
}