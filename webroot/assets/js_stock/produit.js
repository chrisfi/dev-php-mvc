$(document).ready(function () {
    $(document).on('click', '.kl-save-produit', function (e) {
    
        var ID_UNITEMESURE_ID = $('#ID_UNITEMESURE_ID').val();
        var ID_PRODUIT_GROUPE_ID = $('#ID_PRODUIT_GROUPE_ID').val();
        var ID_PRODUIT_FAMIL_ID = $('#ID_PRODUIT_FAMIL_ID').val();
        var ID_PRODUIT_CAG_ID = 'NULL';
        var ID_PRODUIT_LIBELLE = $('#ID_PRODUIT_LIBELLE').val();
        var ID_PRODUIT_PA = $('#ID_PRODUIT_PA').val();
        var ID_PRODUIT_PU = $('#ID_PRODUIT_PU').val();
        var ID_PRODUIT_STK_MIN = $('#ID_PRODUIT_STK_MIN').val();
        var ID_PRODUIT_ID = $('#ID_PRODUIT_ID').val();
        var ID_PRODUIT_CODE = $('#ID_PRODUIT_CODE').val();
        var ID_UTILISATEUR_ID = $('#ID_UTILISATEUR_ID').val();
        var ID_PRODUIT_PEMB = 0;
        var boolean_emballage = 0;
        var boolean_cageot = 0;

        var id_gerer_emballage_checkbox = $('#id-gerer-emballage-checkbox').is(':checked');
        var id_gerer_cageot_checkbox = $('#id-gerer-cageot-checkbox').is(':checked');

        if (id_gerer_emballage_checkbox == true) {
            var ID_PRODUIT_PEMB = $('#ID_PRODUIT_PEMB').val();
            boolean_emballage = 1;
        }
        if (id_gerer_cageot_checkbox == true) {
            var ID_PRODUIT_CAG_ID = $('#ID_PRODUIT_CAG_ID').val();
            boolean_cageot = 1;
        }

        if (ID_UNITEMESURE_ID != ''
           ) {
            
            // var data = {};
            // data.btn_save_produit = 'btn_save_produit';
            // data.UNITEMESURE_ID = ID_UNITEMESURE_ID;
            // data.PRODUIT_GROUPE_ID = ID_PRODUIT_GROUPE_ID;
            // data.PRODUIT_FAMIL_ID = ID_PRODUIT_FAMIL_ID;
            // data.PRODUIT_CAG_ID = ID_PRODUIT_CAG_ID;
            // data.PRODUIT_LIBELLE = ID_PRODUIT_LIBELLE;
            // data.PRODUIT_PA = ID_PRODUIT_PA;
            // data.PRODUIT_PU = ID_PRODUIT_PU;
            // data.PRODUIT_STK_MIN = ID_PRODUIT_STK_MIN;
            // data.PRODUIT_ID = ID_PRODUIT_ID;
            // data.PRODUIT_CODE = ID_PRODUIT_CODE;
            // data.UTILISATEUR_ID = ID_UTILISATEUR_ID;
            // data.PRODUIT_PEMB = ID_PRODUIT_PEMB;
            // data.PRODUIT_DATE_UPD = '';
            // data.PRODUIT_ISDELETE = 0;
            // data.PRODUIT_ISEMBALL = boolean_emballage;
            // data.PRODUIT_ISCAG = boolean_cageot;
            // console.log(data);
            // $.ajax({
            //     url: url_produit_add_ajax,
            //     data,
            //     method: "POST",
            //     dataType: "JSON",
            //     beforeSend: function () {
            //         $(".preloader").fadeIn();
            //     },
            //     success: function (response) {
            //         $(".preloader").fadeOut();
            //         console.log(response);
            //         if (response['status'] == true) {
            //             success_noti_add_produit();
            //             $('#form-produit')[0].reset();
            //         } else {
            //             round_error_noti_add_produit();
            //         }

            //     }
            // });

        } else {
            e.preventDefault();
            error_noti_add_client_champ_vide();
            return;
        }
    });

    $(document).on('click', '#id-gerer-emballage-checkbox', function (e) {
        if ($(this).is(':checked') == true) {
            $('.kl-input-prix-unitaire-emballage').removeClass('d-none');
        } else {
            $('.kl-input-prix-unitaire-emballage').addClass('d-none');
        }
    });
    $(document).on('click', '#id-gerer-cageot-checkbox', function (e) {
        if ($(this).is(':checked') == true) {
            $('.kl-select-cageaot').removeClass('d-none');
        } else {
            $('.kl-select-cageaot').addClass('d-none');
        }
    });


    // $(document).on('click', '.kl-btn-ahref-edit-produit', function (e) {
    //     var id = $(this).data('id');
    //     var data = {};
    //     data.get_produit_by_id = id;
    //     $.ajax({
    //         url: url_get_produit,
    //         data,
    //         method: "POST",
    //         dataType: "html",
    //         beforeSend: function () {
    //             $(".preloader").fadeIn();
    //         },
    //         success: function (response) {
    //             $(".preloader").fadeOut();
    //             $('#id-modal-edit-produits .modal-body').html(response);
    //             $('#id-modal-edit-produits').modal('show');
    //         }
    //     });

    // });

    $(document).on('click', '.kl-btn-ahref-supprimer-produit', function (e) {
        var id = $(this).data('id');
        var data = {};
        data.id_product_to_delete = id;
        swal({
            title: "Vous êtes sure?",
            text: "Supression!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    swal("Supression avec success!", {
                        icon: "success",
                        class: 'kl-delete-cli',
                    });
                    $.ajax({
                        url: url_produit_delete_ajax,
                        data,
                        method: "POST",
                        dataType: "html",
                        beforeSend: function () {
                            $(".preloader").fadeIn();
                        },
                        success: function (response) {
                            $(".preloader").fadeOut();

                        }
                    });

                } else {
                    swal("Supression annulé!");
                }
            });
    });

    $(document).on('click', '.kl-btn-ahref-unite-mesure-produit', function (e) {
        var id = $(this).data('id');
        var id = $(this).data('id');
        var data = {};
        data.id_product = id;
        $.ajax({
            url: url_gerer_Unite_Mesure_Produit_Ajax,
            data,
            method: "POST",
            dataType: "html",
            beforeSend: function () {
                $(".preloader").fadeIn();
            },
            success: function (response) {
                $(".preloader").fadeOut();
                $('#id-modal-gerer-unite-mesure-produits .modal-body').html(response);
                $('#id-modal-gerer-unite-mesure-produits').modal('show');
            }
        });
        
    });

});

/**notification */
function success_noti_add_produit() {
    Lobibox.notify('success', {
        pauseDelayOnHover: true,
        size: 'mini',
        rounded: true,
        icon: 'fa fa-check-circle',
        delayIndicator: false,
        continueDelayOnInactiveTab: false,
        position: 'top right',
        msg: 'Ajout produit avec success'
    });
}

function round_error_noti_add_produit() {
    Lobibox.notify('error', {
        pauseDelayOnHover: true,
        size: 'mini',
        rounded: true,
        delayIndicator: false,
        icon: 'fa fa-times-circle',
        continueDelayOnInactiveTab: false,
        position: 'top right',
        msg: 'Erreur,Verifier le formulaire.'
    });
}