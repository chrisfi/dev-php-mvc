$(document).ready(function () {
    $(".preloader").fadeOut();
    $(document).on('click', '.buttons-excel', function (e) {
        alert('pl')
        e.preventDefault();
        return;
    });
    $(document).on('click', '.kl-save-mesure_conversion', function (e) {
        e.preventDefault();
        var MESURE_CONVERSION_ID = $('#ID_MESURE_CONVERSION_ID').val();
        var UNITEMESURE_FROM_ID = $('#ID_UNITEMESURE_FROM_ID').val();
        var UNITEMESURE_TO_ID = $('#ID_UNITEMESURE_TO_ID').val();
        var UTILISATEUR_ID = $('#ID_UTILISATEUR_ID').val();
        var CONVERSION_VALUE = $('#ID_CONVERSION_VALUE').val();
        if (UNITEMESURE_FROM_ID == '' || UNITEMESURE_TO_ID == '' || UTILISATEUR_ID == '' || CONVERSION_VALUE == '0.00' || CONVERSION_VALUE == '') {
            round_error_noti_add('');
            return;
        } else {
            var data = {};
            data.MESURE_CONVERSION_ID = MESURE_CONVERSION_ID;
            data.UNITEMESURE_FROM_ID = UNITEMESURE_FROM_ID;
            data.UNITEMESURE_TO_ID = UNITEMESURE_TO_ID;
            data.UTILISATEUR_ID = UTILISATEUR_ID;
            data.CONVERSION_VALUE = CONVERSION_VALUE;
            data.btn_save_mesure_conversion = 'btn_save_mesure_conversion';
            console.log(data);
            $.ajax({
                url: url_mesure_conversions_add_ajax,
                data,
                method: "POST",
                dataType: "JSON",
                beforeSend: function () {
                    $(".preloader").fadeIn();
                },
                success: function (response) {
                    $(".preloader").fadeOut();
                    console.log(response);
                    if (response['status'] == true) {
                        success_noti_add('unité de mesure');
                        $('#form-mesure_conversion')[0].reset();
                        return;
                    }
                    round_error_noti_add('Problème ajout');
                    return;
                }
            });

        }
    });


    $(document).on('click', '.kl-save-mesure_conversion_by_produit', function (e) {
        e.preventDefault();
        var MESURE_CONVERSION_ID = $('#ID_MESURE_CONVERSION_ID').val();
        var UNITEMESURE_FROM_ID = $('#ID_UNITEMESURE_FROM_ID').val();
        var UNITEMESURE_TO_ID = $('#ID_UNITEMESURE_TO_ID').val();
        var UTILISATEUR_ID = $('#ID_UTILISATEUR_ID').val();
        var CONVERSION_VALUE = $('#ID_CONVERSION_VALUE').val(); 
        var PRODUIT_ID = $('#ID_PRODUIT_ID').val();
        if (UNITEMESURE_FROM_ID == '' || UNITEMESURE_TO_ID == '' || UTILISATEUR_ID == '' || CONVERSION_VALUE == '0.00' || CONVERSION_VALUE == '') {
            round_error_noti_add('');
            return;
        } else {
            var data = {};
            data.MESURE_CONVERSION_ID = MESURE_CONVERSION_ID;
            data.UNITEMESURE_FROM_ID = UNITEMESURE_FROM_ID;
            data.UNITEMESURE_TO_ID = UNITEMESURE_TO_ID;
            data.UTILISATEUR_ID = UTILISATEUR_ID;
            data.CONVERSION_VALUE = CONVERSION_VALUE;
            data.PRODUIT_ID = PRODUIT_ID;
            data.btn_save_mesure_conversion = 'btn_save_mesure_conversion';
            console.log(data);
            $.ajax({
                url: url_add_mesure_conversions_by_product_ajax,
                data,
                method: "POST",
                dataType: "JSON",
                beforeSend: function () {
                    $(".preloader").fadeIn();
                },
                success: function (response) {
                    $(".preloader").fadeOut();
                    console.log(response);
                    if (response['status'] == true) {
                        success_noti_add('unité de mesure');
                        $('#form-mesure_conversion')[0].reset();
                        return;
                    }
                    round_error_noti_add('Problème ajout');
                    return;
                }
            });

        }
    });


});