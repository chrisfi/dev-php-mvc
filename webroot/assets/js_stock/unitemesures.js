$(document).ready(function () {
    $(document).on('click', '.kl-save-unitemesures', function (e) {
        e.preventDefault();
        var UNITEMESURELIB = $('#ID_UNITEMESURELIB').val();
        var UNITEMESURECODE = $('#ID_UNITEMESURECODE').val();
        var UNITEMESURE_ID = $('#ID_UNITEMESURE_ID').val();
        var UNITEMESURE_DESC = $('#ID_UNITEMESURE_DESC').val();
        var ID_UNIT_CAG_CHECKBOX = 0;
        if ($('#ID_UNIT_CAG_CHECKBOX').is(':checked') == true) {
            ID_UNIT_CAG_CHECKBOX = 1;
        }
        if (UNITEMESURECODE == '' || UNITEMESURELIB == '') {
            round_error_noti_add();
            return;
        } else {
            var data = {};
            data.UNITEMESURELIB = UNITEMESURELIB;
            data.UNITEMESURECODE = UNITEMESURECODE;
            data.UNITEMESURE_ID = UNITEMESURE_ID;
            data.UNITEMESURE_DESC = UNITEMESURE_DESC;
            data.UNIT_CAG = ID_UNIT_CAG_CHECKBOX;
            data.btn_save_unitemesures = 'btn_save_unitemesures';
            data.action = 'save_edite';
            console.log(data);
            $.ajax({
                url: url_unite_mesure,
                data,
                method: "POST",
                dataType: "JSON",
                beforeSend: function () {
                    $(".preloader").fadeIn();
                },
                success: function (response) {
                    $(".preloader").fadeOut();
                    console.log(response);
                    if (response['status'] == true) {
                        success_noti_add('Unité de mesure');
                       
                    } else {
                        round_error_noti_add();
                    }



                }
            });
        }
    });

    $(document).on('click', '.kl-edite-unite-mesures', function () {
        var id = $(this).data('id');
        var data = {};
        data.id = id;
        data.action = 'get_client';
        $.ajax({
            url: url_unite_mesure,
            data,
            method: "POST",
            dataType: "JSON",
            beforeSend: function () {
                $(".preloader").fadeIn();
            },
            success: function (response) {
                response=response['unitemesures'];
                $(".preloader").fadeOut();
                $('#ID_UNIT_CAG_CHECKBOX').prop('checked',false);
                $('#ID_UNITEMESURELIB').val(response['unite_mesure_libelle']);
                $('#ID_UNITEMESURECODE').val(response['unite_mesure_code']);
                $('#ID_UNITEMESURE_ID').val(response['unite_mesure_id']);
                $('#ID_UNITEMESURE_DESC').val(response['unite_mesure_description']);
                if(response['unite_mesure_cageot']==1){
                    $('#ID_UNIT_CAG_CHECKBOX').prop('checked',true);
                }
              
                $('#update-unitmesure').modal('show');
            }
        });

    });
});

$(document).on('click', '.kl-delete-unite-mesure', function () {
                           
    $('#delete-unitmesure').modal('show');


});



function round_error_noti_add() {
    Lobibox.notify('error', {
        pauseDelayOnHover: true,
        size: 'mini',
        rounded: true,
        delayIndicator: false,
        icon: 'fa fa-times-circle',
        continueDelayOnInactiveTab: false,
        position: 'top right',
        msg: 'Erreur,Verifier le formulaire.'
    });
}