
$(document).ready(function () {
    $(".preloader").fadeOut();
    $('.swal-button--confirm').addClass('btn-danger');
    var id_client_select_tableau = 0;
    $(document).on('click', '.kl-save-client', function (e) {
        e.preventDefault();
        var ID_CLIENT_NOM = $('#ID_CLIENT_NOM').val();
        var ID_CLIENT_PRENOM = $('#ID_CLIENT_PRENOM').val();
        var ID_CLIENT_NIF = $('#ID_CLIENT_NIF').val();
        var ID_CLIENT_STAT = $('#ID_CLIENT_STAT').val();
        var ID_CLIENT_ADRESSE = $('#ID_CLIENT_ADRESSE').val();
        var ID_CLIENT_ID = $('#ID_CLIENT_ID').val();
        var ID_CLIENT_NUM = $('#ID_CLIENT_NUM').val();
        var data = {};
        data.btn_save_client = 'edit';
        data.CLIENT_NOM = ID_CLIENT_NOM;
        data.CLIENT_PRENOM = ID_CLIENT_PRENOM;
        data.CLIENT_NIF = ID_CLIENT_NIF;
        data.CLIENT_STAT = ID_CLIENT_STAT;
        data.CLIENT_ADRESSE = ID_CLIENT_ADRESSE;
        data.CLIENT_ID = ID_CLIENT_ID;
        data.CLIENT_NUM = ID_CLIENT_NUM;
        // data.btn_save_client = '1';
        if (ID_CLIENT_NOM == '') {
            error_noti_add_client_champ_vide();
            return;
        } else {
            $.ajax({
                url: url_client_ajax,
                data,
                method: "POST",
                dataType: "JSON",
                beforeSend: function () {
                    $(".preloader").fadeIn();
                },
                success: function (response) {
                    $(".preloader").fadeOut();
                    console.log(response);
                    if (response['save'] == 'success') {
                        if(ID_CLIENT_ID!=0){
                            success_noti_edit_client();
                            location.reload();
                           
                        }else{
                            $('#form-client')[0].reset();
                            success_noti_add_client();
                            
                        }
                       
                       $('.kl-dismiss-modal').trigger('click');
                    } else {

                    }
                }
            });

            return false;
        }

    });
    $(document).on('click', '.kl-checkbox-table-client', function (e) {
        var ckecked = $(this).is(':checked');
        $('.kl-checkbox-table-client').prop('checked', false);
        var id = $(this).data('id');
        id_client_select_tableau = id;
        if (ckecked) {
            $('#' + id).prop('checked', true);
            $('.kl-action-button').removeClass('d-none');

        } else {
            $('#' + id).prop('checked', false);
            $('.kl-action-button').addClass('d-none');

        }


    });
    $(document).on('click', '.kl-delet-client', function (e) {
        $('#confirm-btn-alert-delete-object').trigger('click');
        var data = {};
        data.CLIENT_ID = id_client_select_tableau;
        $.ajax({
            url: url_delete,
            data,
            method: "POST",
            dataType: "JSON",
            beforeSend: function () {
                $(".preloader").fadeIn();
            },
            success: function (response) {
                if (response['delete'] == true) {
                    $("#" + id_client_select_tableau).parent().parent().parent().empty();
                    id_client_select_tableau = 0;
                    $('.kl-action-button').addClass('d-none');
                }

                $(".preloader").fadeOut();


            }
        });
    });

    $(document).on('click', '.kl-edit-client', function (e) {
        var data = {};
        data.CLIENT_ID = $(this).data('id');
        $.ajax({
            url: url_get_cli,
            data,
            method: "POST",
            dataType: "JSON",
            beforeSend: function () {
                $(".preloader").fadeIn();
            },
            success: function (response) {
                if (response['status'] == 'ok') {
                    id_client_select_tableau = 0;
                    $('.kl-action-button').addClass('d-none');
                    var client = response['client'];   
                    $('#ID_CLIENT_NOM').val(client['client_nom']);
                    $('#ID_CLIENT_PRENOM').val(client['client_prenom']);
                    $('#ID_CLIENT_NIF').val(client['client_nif']);
                    $('#ID_CLIENT_STAT').val(client['client_stat']);
                    $('#ID_CLIENT_ADRESSE').val(client['client_adress']);
                    $('#ID_CLIENT_ID').val(client['client_id']);
                    $('#ID_CLIENT_NUM').val(client['client_num']);
                    $('.kl-btn-form-default .kl-save-client').addClass('d-none');
                    $('#id-modal-add-edit-client').modal('show');


                }

                $(".preloader").fadeOut();
            }
        });
    });

    $("#confirm-btn-alert-delete-object").click(function () {

        swal({
            title: "Vous êtes sure?",
            text: "Supression!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    swal("Supression avec success!", {
                        icon: "success",
                        class: 'kl-delete-cli',
                    });
                    var data = {};
                    data.CLIENT_ID = id_client_select_tableau;
                    $.ajax({
                        url: url_delete,
                        data,
                        method: "POST",
                        dataType: "JSON",
                        beforeSend: function () {
                            $(".preloader").fadeIn();
                        },
                        success: function (response) {
                            $(".preloader").fadeOut();
                        }
                    });
                } else {
                    swal("Supression annulé!");
                }
            });

    });



});