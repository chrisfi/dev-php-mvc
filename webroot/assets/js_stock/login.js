$(document).ready(function () {
    // round_error_noti_add('Problème ajout');
    $(".preloader").fadeOut();
    $(document).on('click', '.kl-authentification', function (e) {
        var ucodus = $('.kl-ucodus').is(':checked');
        var detaille = $('.kl-detaille').is(':checked');
        if (ucodus == false && detaille == false) {
           $('.kl-text-choisir-type-vente').addClass('btn-danger');
           return false;
        }

        e.preventDefault();
        var id_login = $('#id_login').val();
        var id_password = $('#id_password').val();
        var data = {};
        data.name = id_login;
        data.password = id_password;
        data.detaille = detaille;
        data.ucodus = ucodus;
        data.login = 'login';
        if (id_login != '' && id_password != '') {
            $.ajax({
                url: url_authentificate,
                data,
                method: "POST",
                dataType: "JSON",
                beforeSend: function () {
                    $(".preloader").fadeIn();
                },
                success: function (response) {
                    console.log(response);
                    $(".preloader").fadeOut();
                    if (response['status'] == 'ok') {
                        location.reload();
                    } else {
                        $('#id_login').css('border', '1px solid red');
                        $('#id_password').css('border', '1px solid red');

                    }

                }
            });

        } else {
            return;
        }

    });

    $(document).on('click', '#id_login', function (e) {
        $('#id_login').css('border', '');
    });
    $(document).on('click', '#id_password', function (e) {
        $('#id_password').css('border', '');
    });

    $(document).on('click', '.kl-ucodus', function (e) {
        $('.kl-text-choisir-type-vente').removeClass('btn-danger');
    });

    $(document).on('click', '.kl-detaille', function (e) {
        $('.kl-text-choisir-type-vente').removeClass('btn-danger');
    });

    $(document).on('click', '#id-logout', function (e) {
        e.preventDefault();
        var data = {};
        data.logout = 'logout';
        $.ajax({
            url: url_authentificate,
            data,
            method: "POST",
            dataType: "JSON",
            beforeSend: function () {
                console.log(data);
                $(".preloader").fadeIn();
            },
            success: function (data) {
                $(".preloader").fadeOut();
                location.reload();
            }
        });



    });

});

