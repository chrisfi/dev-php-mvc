$(document).ready(function () {
    /**select produit */
    $('#id_produit_entre').change(function (e) {
        var id = $(this).val();
        get(id);

    });

    $('.kl_UNITEMESURE_ID').click(function (e) {
        var UNITEMESURE_ID

    });

    $('#id-valide-produit-entre').click(function (e) {
        $(this).val('valide_qte');
        return true;
    });

});

function get(id_prod) {
    var data = {};
    data.id_prod = id_prod;
    data.action = 'ligne_unite_mesure_by_produit';
    $.ajax({
        url: 'ajax',
        data,
        method: "POST",
        dataType: "html",
        beforeSend: function () {
            // $(".preloader").fadeIn();
        },
        success: function (response) {
            $('#id-content-list-unite-mesure').html(response);
            $('.single-select').select2();
            getQte(id_prod);
        }
    });
}

function getQte(id_prod) {
    var data = {};
    data.id_prod = id_prod;
    data.action = 'get_unite_Qte_produit_Id';
    $.ajax({
        url: 'get_unite_Qte_produit_Id',
        data,
        method: "POST",
        dataType: "html",
        beforeSend: function () {
        },
        success: function (response) {
            $('.kl-qte-restant-produit').html(response);
        }
    });
}