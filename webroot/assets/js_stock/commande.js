$(document).ready(function () {
    // alert('ok')
    // error_noti_add_client_champ_vide();

    // $('.actions .menu')
    var cli_id = 0;
    var ids_product_selected = {};
    var ids_unite_produit = {};

    $(document).on('click', '.kl-btn-add-to-card', function (e) {
        e.preventDefault();
        var ID_PRODUIT_ID = $('#ID_PRODUIT_ID').val();
        var val_stock = $('#id-qte-en-stock').val();
        var ID_Quantity_Produit_VALUE = $('#ID_Quantity_Produit_VALUE').val();

        if (ID_PRODUIT_ID == 0) {
            return;
        }
        console.log((val_stock) + '=' + (ID_Quantity_Produit_VALUE));
        if (parseFloat(val_stock) < parseFloat(ID_Quantity_Produit_VALUE)) {
            Error_notification_danger('Quantité en stock(' + val_stock + ') insuffisant');
            return false;

        }

        var ID_UNITEMESURE_ID = $('#ID_UNITEMESURE_ID').val();
        ids_product_selected[ID_PRODUIT_ID] = ID_Quantity_Produit_VALUE;
        ids_unite_produit[ID_PRODUIT_ID] = ID_UNITEMESURE_ID;
        var data = {};
        data.id_produit_cmd = ID_PRODUIT_ID;
        data.unite_mes_cmd = ID_UNITEMESURE_ID;
        data.quantite_cmd = ID_Quantity_Produit_VALUE;

        data.cmd = ids_product_selected;
        data.action = 'lign_cmd';
        $.ajax({
            url: url_all_cmd_add_ajax,
            data,
            method: "POST",
            dataType: "html",
            beforeSend: function () {
                $(".preloader").fadeIn();
            },
            success: function (response) {
                $('.kl-content-liste-select-cmd').html(response);
                $(".preloader").fadeOut();
                $('#ID_Quantity_Produit_VALUE').val('1.00');
            }
        });
    });

    $('#ID_PRODUIT_ID').change(function (e) {
        e.preventDefault();
        var id_prod = $('#ID_PRODUIT_ID').val();
        get_unite_mesure_by_produit_Id(id_prod);
        get_unite_Qte_produit_Id(id_prod);
    });

    $(document).on('click', '.kl-btn-delete-lign-cmd', function (e) {
        var id = $(this).data('id');

        var data = {};
        data.id_ligne = id;
        data.action = 'delete_one_lign_cmd';
        $.ajax({
            url: url_all_cmd_add_ajax,
            data,
            method: "POST",
            dataType: "html",
            beforeSend: function () {
                // $(".preloader").fadeIn();
            },
            success: function (response) {
                $('.kl-content-liste-select-cmd').html(response);
            }
        });

    });



    $(document).on('blur', '#ID_Quantity_Produit_VALUE', function (e) {
        e.preventDefault();
        var val_stock = $('#id-qte-en-stock').val();
        var val = $(this).val();



    });

    $(document).on('change', '#ID_CLIENT_ID', function (e) {
        cli_id = $(this).val();
    });

    $(".kl-save-all-cmd-selected").click(function () {
        cli_id = $('#ID_CLIENT_ID').val();
        var res;
        swal({
            title: "Vous êtes sure d'enregistrer cette commande ?",
            text: "",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    var data = {};
                    data.client_id = cli_id;
                    data.action = 'validation_cmd';
                    $(".preloader").fadeIn();
                    $.ajax({
                        url: url_all_cmd_add_ajax,
                        data,
                        method: "POST",
                        dataType: "JSON",
                        beforeSend: function () {

                        },
                        success: function (response) {
                            res = response;
                            console.log(res);
                        }
                    });

                    swal("Enregistration avec succèss!", {
                        icon: "success",
                        class: 'kl-delete-cli',
                    });
                    $(".preloader").fadeOut();
                    Add_notification_success('Commande enregistré avec succèss');
                } else {
                    swal("Enregistration annulé!");
                }
            });

    });

    $(".kl-annule-all-cmd-selected").click(function () {
        swal({
            title: "Vous êtes sure d'annulé cette commande ?",
            text: "",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    swal("Commande annulé avec success!", {
                        icon: "success",
                        class: '',
                    });
                    var data = {};
                    data.action = 'annule_all_cmd';
                    $.ajax({
                        url: url_all_cmd_add_ajax,
                        data,
                        method: "POST",
                        dataType: "JSON",
                        beforeSend: function () {
                            // $(".preloader").fadeIn();
                        },
                        success: function (response) {
                            // response = JSON.parse(Response);
                            // console.log(Response);
                            Add_notification_success('Commande annulé succèss');
                            // $(".preloader").fadeOut();
                            $('.kl-content-liste-select-cmd').empty();
                            location.reload();
                        }
                    });
                } else {
                    swal("Validation annulé!");
                }
            });
    });

});

function get_unite_mesure_by_produit_Id(id_prod) {
    var data = {};
    data.id_prod = id_prod;
    data.action = 'ligne_unite_mesure_by_produit';
    $.ajax({
        url: url_all_cmd_add_ajax,
        data,
        method: "POST",
        dataType: "html",
        beforeSend: function () {
            // $(".preloader").fadeIn();
        },
        success: function (response) {
            $('#id-content-list-unite-mesure').html(response);
        }
    });
}

function get_unite_Qte_produit_Id(id_prod) {
    var data = {};
    data.id_prod = id_prod;
    data.action = 'get_unite_Qte_produit_Id';
    $.ajax({
        url: url_all_cmd_add_ajax,
        data,
        method: "POST",
        dataType: "html",
        beforeSend: function () {
        },
        success: function (response) {
            $('.kl-qte-restant-produit').html(response);
        }
    });
}

