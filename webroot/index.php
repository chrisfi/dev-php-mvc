
<?php
define('URL', dirname($_SERVER['SCRIPT_NAME']));
define('URL_AJAX', dirname(dirname($_SERVER['SCRIPT_NAME'])));
define('webrots', dirname(__FILE__));
define('rot', dirname(webrots));
define('ds', DIRECTORY_SEPARATOR);
define('CORE', rot . ds);
define('b_url', dirname(dirname($_SERVER['SCRIPT_NAME'])));
define('ROOT', str_replace('index.php', '', $_SERVER['SCRIPT_FILENAME']));
define('WEBROOT', dirname(__FILE__));
define('rots', dirname(WEBROOT));
define('DS', DIRECTORY_SEPARATOR);
define('BASE_URL', dirname($_SERVER['SCRIPT_NAME']));

//  var_dump( URL);
//  var_dump( webrots);
//  var_dump( rot);
//  var_dump( CORE);
//  var_dump( b_url);
//  var_dump( ROOT);
//  var_dump( WEBROOT);
//  var_dump( rots);
//  var_dump( DS);
// die;
require_once(rot . ds . 'app/Model.php');
require_once(rot . ds . 'app/Controller.php');
require_once(rot . ds . 'app/url.php');
require_once (rot.ds.'util/fonction_util.php');
$utile=new Fonction_util();
define('OPTION_FR',$utile->option_fr());
global $val_glob;
$params = explode('/', $_GET['p']);
session_start();
define('USER_ID', isset($_SESSION['user_id']) ? $_SESSION['user_id'] : 0);
$vente_type = '';
$VENTE = 0;
if (isset($_SESSION['vente'])) {

    if ($_SESSION['vente'] == 1) {
        $vente_type = 'DETAILLE';
        $VENTE = 1;
    } else {
        $vente_type = 'UCODIS';
        $VENTE = 2;
    }
}
// $type_vente = $_SESSION['vente'];
define('type_vente',  $VENTE);
define('VENTE',  $vente_type);
if (isset($_POST['logout'])) {
    require_once(rot . ds . 'controllers/logins.php');
    $authent = new Logins();
    $authent->index();
}
// session_destroy();
if (isset($_SESSION['login'])) {
    if ($params[0] != "") {
        $controller = ucfirst($params[0]);
        $ctrl = $controller;
        $action = isset($params[1]) ? $params[1] : 'index';
        $existe = is_file(rot . ds . 'controllers/' . $controller . '.php');
        if ($existe) {
            require_once(rot . ds . 'controllers/' . $controller . '.php');
            $controller = new $controller();
        } else {
            require_once(rot . ds . 'controllers/Main.php');
            $controller = new Main();
            $action = 'page404';
        }

        if (method_exists($controller, $action)) {
            unset($params[0]);
            unset($params[1]);
            call_user_func_array([$controller, $action], $params);
        } else {
            require_once(rot . ds . 'controllers/Main.php');
            http_response_code(404);
            $controller = new Main();
            $controller->page404();

            // echo "La page recherchée n'existe pas";
        }
    } else {
        require_once(rot . ds . 'controllers/Main.php');
        $controller = new Main();
        $controller->index();
    }
} else {
    // var_dump($_POST);
    require_once(rot . ds . 'controllers/logins.php');
    $authent = new Logins();
    $authent->index();
}
