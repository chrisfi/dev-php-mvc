<?php

require_once(rot . ds . 'dompdf/autoload.inc.php');

use Dompdf\Dompdf;

$document = new Dompdf();
$datata = '';
// var_dump($commande_entetes);
// var_dump($Commande_entetes_client);
foreach ($commande_entetes as $key => $cmd) {
    // <td>' . $unite_mes_prod['prod_libell'] . '</td>
    //   <td>' . $mesure['unite_mesure_code'] . '</td>
    $datata .=
        '<tr>

    <td>' . $cmd['cmd_line_prix_unite'] . 'Ar</td>
    <td>' . $cmd['cmd_line_qte'] . '</td>
    <td>' . $cmd['cmd_line_prix_emballage'] . ' Ar</td>
    <td>' . $cmd['cmd_line_montant_emballage'] . ' Ar</td>
    <td>' . $cmd['cmd_line_montant'] . 'Ar</td>
  </tr>';
};

$html = '
	<style>
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
    font-size:11px;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}

tr:nth-child(even) {
    background-color: #dddddd;
}
</style>


<table>
<tr>
<th scope="col">Numero</th>
<th scope="col">Produit</th>
<th scope="col">Unite mesure</th>
<th scope="col">Prix unitaire</th>
<th scope="col">Quantite</th>
<th scope="col">Prix emballage</th>
<th scope="col">Sous total emballage</th>
<th scope="col">Sous Total</th>
</tr>

' .  $datata . '

</table>
';
$tete = '<html>
    <head>
        <style>
            /** 
                Set the margins of the page to 0, so the footer and the header
                can be of the full height and width !
             **/
            @page {
                margin: 0cm 0cm;
            }

            /** Define now the real margins of every page in the PDF **/
            body {
                margin-top: 3cm;
                margin-left: 2cm;
                margin-right: 2cm;
                margin-bottom: 2cm;
            }

            /** Define the header rules **/
            header {
                position: fixed;
                top: 0cm;
                left: 0cm;
                right: 0cm;
                height: 3cm;
            }

            /** Define the footer rules **/
            footer {
                position: fixed; 
                bottom: 0cm; 
                left: 0cm; 
                right: 0cm;
                height: 2cm;
            }
        </style>
    </head>
    <body>
        <!-- Define header and footer blocks before your content -->
        <header>
            <img src="header.png" width="100%" height="100%"/>
        </header>

        <footer>
            <img src="footer.png" width="100%" height="100%"/>
        </footer>

        <!-- Wrap the content of your PDF inside a main tag -->
        <main>
            <h1>Hello World</h1>
        </main>
    </body>
</html>';


// $p = file_get_contents(rot . ds . 'helpers/gerer_all_pdf.php');
$document->loadHtml($html);
//set page size and orientation
$document->setPaper('A6', 'portrait');
//Render the HTML as PDF
$document->render();
//Get output of generated pdf in Browser
$document->stream("Webslesson", array("Attachment" => 0));

$tete = '<html>
    <head>
        <style>
            /** 
                Set the margins of the page to 0, so the footer and the header
                can be of the full height and width !
             **/
            @page {
                margin: 0cm 0cm;
            }

            /** Define now the real margins of every page in the PDF **/
            body {
                margin-top: 3cm;
                margin-left: 2cm;
                margin-right: 2cm;
                margin-bottom: 2cm;
            }

            /** Define the header rules **/
            header {
                position: fixed;
                top: 0cm;
                left: 0cm;
                right: 0cm;
                height: 3cm;
            }

            /** Define the footer rules **/
            footer {
                position: fixed; 
                bottom: 0cm; 
                left: 0cm; 
                right: 0cm;
                height: 2cm;
            }
        </style>
    </head>
    <body>
        <!-- Define header and footer blocks before your content -->
        <header>
            <img src="header.png" width="100%" height="100%"/>
        </header>

        <footer>
            <img src="footer.png" width="100%" height="100%"/>
        </footer>

        <!-- Wrap the content of your PDF inside a main tag -->
        <main>
            <h1>Hello World</h1>
        </main>
    </body>
</html>';
