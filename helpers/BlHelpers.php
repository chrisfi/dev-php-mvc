<?php
class BlHelpers
{
    public function get_Form_Payement($id_cmd)

    {
        require_once(rot . ds . 'models/Somme_cmd_client.php');
        $cmd = new Somme_cmd_client();
        $cmd =  $cmd->get_somme_cmd_by_id($_POST['id_cmd']);
?>
        <form id="id-form-payemet-1" action="" method="POST">
            <div class="row mt-3">

            </div>

            <div class="row">
                <div class="col-lg-12 kl-qte-restant-produit">
                    <div class="form-group">
                        <label>Somme total</label>
                        <input type="number" class="kl-input_decimal kl_somme_total" step="any" value="<?php echo  $cmd['somme'] ?>" disabled="disabled" class="form-control">
                    </div>
                </div>
                <div class="col-lg-12 kl-qte-restant-produit">
                    <div class="form-group">
                        <label>Restant</label>
                        <input type="number" class="kl-input_decimal kl_somme_restant" step="any" value="<?php echo  $cmd['reste'] ?>" disabled="disabled" class="form-control">
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="form-group">
                        <label>Montant</label>
                        <input id="montant_payer" class="kl-input_decimal" type="number" step="any" min='1' value="1.00" name="montant_A_payer" required>
                    </div>
                </div>

                <input type="hidden" id="id_cmd_a_paye" name="id_cmd_A_paye" value="<?php echo  $cmd['id_cmd'] ?>">
                <input type="hidden" class="kl_somme_total" name="somme_total" value="<?php echo  $cmd['somme'] ?>">
                <input type="hidden" class="kl_somme_restant" name="somme_restant" value="<?php echo  $cmd['reste'] ?>">

                <div class="col-lg-12 ">
                    <div class="row">
                        <div class="col"></div>
                        <div class="col">
                            <div class="form-group kl-btn-form-default">
                                <button type="submit" id="IDsave_payemen" class="btn btn-success shadow-primary w-100 mt-4 px-5 "><i class="icon-plus"></i>Valide</button>
                            </div>
                        </div>
                        <div class="col"></div>
                    </div>

                </div>
            </div>
        </form>
        <script src="<?php echo URL; ?>/assets/plugins/bootstrap-touchspin/js/bootstrap-touchspin-script.js"></script>
        <script src="<?php echo URL; ?>/assets/plugins/inputtags/js/bootstrap-tagsinput.js"></script>
        <script>
            $(document).ready(function() {
                $('#IDsave_payemen').click(function(e) {
                    e.preventDefault();
                    var data = {};
                    var restant = $('.kl_somme_restant').val();
                    data.montant_A_payer = $('#montant_payer').val();
                    data.id_cmd_A_paye = $('#id_cmd_a_paye').val();

                    console.log(data);
                    console.log(parseFloat(restant) );
                    console.log(parseFloat($('#montant_payer').val()));
                    data.action = 'save_payement';
                    if (parseFloat(restant) < parseFloat($('#montant_payer').val())) {
                        Error_notification_danger('Montant non valide');
                        return false;
                    }


                    $.ajax({
                        url: url_payer,
                        data,
                        method: "POST",
                        dataType: "JSON",
                        beforeSend: function() {},
                        success: function(response) {
                            location.reload();
                        }
                    });

                });
            });
        </script>
<?php
    }
}
