<?php
class ProduitHelpers
{
    public static function get_form_produit($id)
    {
        require_once(rot . ds . 'app/Controller.php');
        require_once(rot . ds . 'models/famille.php');
        require_once(rot . ds . 'models/unitemesure.php');
        require_once(rot . ds . 'models/Produit_groupe.php');
        require_once(rot . ds . 'models/Produit.php');
        $prod = new produit();
        $produit = $prod->get_produit_by_id($id);
        // var_dump($produit);
        $famille = new Famille();
        $produit_familles = $famille->getAll();
        $unitemesure = new Unitemesure();
        $nitemesures = $unitemesure->getAll();
        $produit_groupe = new Produit_groupe();
        $produit_groupes = $produit_groupe->getAll();
        $produit_cageots = $prod->get_produit_by_PRODUIT_ISCAG_1(1);
?>

<form id="form-produit-edit" action="" method="POST">
    <div class="card ">

        <div class="card-body">
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label>Groupe</label>
                        <select class="form-control single-select" id="ID_PRODUIT_GROUPE_ID" name="PRODUIT_GROUPE_ID"
                            required>
                            <option value="null"></option>
                            <?php foreach ($produit_groupes as $produit_groupe) { ?>
                            <option
                                <?php echo $produit_groupe['PRODUIT_GROUPE_ID'] == $produit['PRODUIT_GROUPE_ID'] ? 'selected' : ''; ?>
                                value="<?php echo $produit_groupe['PRODUIT_GROUPE_ID']; ?>">
                                <?php echo $produit_groupe['PRODUIT_GROUPE']; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label>Famille</label>
                        <select class="form-control single-select" id="ID_PRODUIT_FAMIL_ID" name="PRODUIT_FAMIL_ID">
                            <?php foreach ($produit_familles as $famille) { ?>
                            <option
                                <?php echo $famille['PRODUIT_FAMIL_ID'] == $produit['PRODUIT_FAMIL_ID'] ? 'selected' : ''; ?>
                                value="<?php echo $famille['PRODUIT_FAMIL_ID']; ?>">
                                <?php echo $famille['PRODUIT_FAMIL']; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>

            </div>

            <div class="row mb-3">
                <div class="col">
                    <div class="form-group">
                        <label for="ID_PRODUIT_LIBELLE">Libelle</label>
                        <input type="text" class="form-control" id="ID_PRODUIT_LIBELLE"
                            value="<?php echo $produit['PRODUIT_LIBELLE'] ?>" name="PRODUIT_LIBELLE" required
                            placeholder="Entrer nom">
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label>Unité de mesure(base)</label>
                        <select class="form-control single-select" id="ID_UNITEMESURE_ID" name="UNITEMESURE_ID"
                            required>

                            <?php foreach ($nitemesures as $nitemesure) { ?>
                            <option
                                <?php echo $nitemesure['UNITEMESURE_ID'] == $produit['UNITEMESURE_ID'] ? 'selected' : ''; ?>
                                value="<?php echo $nitemesure['UNITEMESURE_ID']; ?>">
                                <?php echo $nitemesure['UNITEMESURELIB']; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row mb-3">

                <div class="col">
                    <div class="form-group">
                        <label>Prix d'achat(AR)</label>
                        <input id="ID_PRODUIT_PA" class="kl-input_decimal_Ar" step="any" type="number" min='0'
                            name="PRODUIT_PA" value="<?php echo $produit['PRODUIT_PA'] ?>" required>
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label>Prix unitaire(AR)</label>
                        <input id="ID_PRODUIT_PU" class="kl-input_decimal_Ar" step="any" type="number" min='0'
                            name="PRODUIT_PU" value="<?php echo $produit['PRODUIT_PU'] ?>" required>
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label>Stock minimum</label>
                        <input id="ID_PRODUIT_STK_MIN" class="kl-input_decimal" type="number" step="any" min='0'
                            name="PRODUIT_STK_MIN" value="<?php echo $produit['PRODUIT_STK_MIN'] ?>">
                    </div>
                </div>
            </div>

        </div>
    </div>


    <div class="row -100">
        <div class="card w-100">
            <div class="col-lg-12">

                <div class="card-header text-uppercase">GESTION DES EMBALLAGES</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col">

                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group py-2">
                                        <div class="icheck-material-success">
                                            <input type="checkbox" id="id-gerer-emballage-checkbox"
                                                <?php echo $produit['PRODUIT_ISEMBALL'] == 1 ? 'checked' : ''; ?>
                                                name="PRODUIT_ISEMBALL">
                                            <label for="id-gerer-emballage-checkbox">Gerer Emballage</label>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-lg-8">
                                    <div
                                        class="form-group kl-input-prix-unitaire-emballage  <?php echo $produit['PRODUIT_ISEMBALL'] == 1 ? '' : 'd-none'; ?>">
                                        <label>prix unitaire emballage (ar) :</label>
                                        <input id="ID_PRODUIT_PEMB" class="kl-input_decimal" type="number" step="any"
                                            min='0' value="<?php echo $produit['PRODUIT_PEMB']; ?>" name="PRODUIT_PEMB">
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col">
                            <div class="row">

                                <div class="col-lg-4">
                                    <div class="form-group py-2">
                                        <div class="icheck-material-success">
                                            <input type="checkbox" id="id-gerer-cageot-checkbox"
                                                <?php echo $produit['PRODUIT_ISCAG'] == 1 ? 'checked' : ''; ?>
                                                name="PRODUIT_ISCAG">
                                            <label for="id-gerer-cageot-checkbox">Gerer Cageot :</label>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-lg-8">
                                    <div
                                        class="form-group kl-select-cageaot  <?php echo $produit['PRODUIT_ISCAG'] == 1 ? '' : 'd-none'; ?>">
                                        <label>CAGEOT</label>
                                        <select class="form-control single-select" id="ID_PRODUIT_CAG_ID"
                                            name="PRODUIT_CAG_ID">
                                            <?php foreach ($produit_cageots as $prod_c) { ?>
                                            <option
                                                <?php echo $prod_c['PRODUIT_ID'] == $produit['PRODUIT_CAG_ID'] ? 'selected' : ''; ?>
                                                value="<?php echo $prod_c['PRODUIT_ID']; ?>">
                                                <?php echo $prod_c['PRODUIT_LIBELLE']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <input type="hidden" id="ID_PRODUIT_ID" value="<?php echo $produit['PRODUIT_ID']; ?>"
                            name="PRODUIT_ID">
                        <input type="hidden" id="ID_PRODUIT_CODE" value="<?php echo $produit['PRODUIT_CODE']; ?>"
                            name="PRODUIT_CODE">
                        <input type="hidden" id="ID_UTILISATEUR_ID" value="<?php echo USER_ID; ?>"
                            name="UTILISATEUR_ID">


                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="<?php echo URL; ?>/assets/plugins/inputtags/js/bootstrap-tagsinput.js"></script>
    <script src="<?php echo URL; ?>/assets/plugins/bootstrap-touchspin/js/jquery.bootstrap-touchspin.js"></script>
    <script src="<?php echo URL; ?>/assets/plugins/bootstrap-touchspin/js/bootstrap-touchspin-script.js"></script>

    <!--Select Plugins Js-->
    <script src="<?php echo URL; ?>/assets/plugins/select2/js/select2.min.js"></script>
    <!--Inputtags Js-->
    <script src="<?php echo URL; ?>/assets/plugins/inputtags/js/bootstrap-tagsinput.js"></script>
    <script src="<?php echo URL; ?>/assets/plugins/select2/js/select2.min.js"></script>

    <script>
    $(document).ready(function() {
        $('.single-select').select2();
    });
    </script>
    <?php }

    public static function gerer_Unite_mesure_product($id)
    {
        require_once(rot . ds . 'models/Produit.php');
        require_once(rot . ds . 'models/unitemesure.php');
        require_once(rot . ds . 'models/produit_unitme.php');
        require_once(rot . ds . 'models/Mesure_conversion.php');


        $prod = new produit();
        $produit = $prod->get_produit_by_id($id);
        $unite_base = $produit['UNITEMESURE_ID'];
        $unitemesure = new Unitemesure();
        $unitemesures = $unitemesure->get_Unite_Mesure_by_id($produit['UNITEMESURE_ID']);
        $produit_unite_mesures = new Produit_unitme();
        $unite_mesures_produits =  $produit_unite_mesures->get_Unite_Mesure_by_id_produit($id);
        var_dump($unite_base);
        var_dump($unite_mesures_produits);

        ?>
    <h5 class="text-center"><?php echo $produit['PRODUIT_LIBELLE']; ?></h5>
    <div class="row">
        <div class="col-12 col-lg-6 col-xl-3">
            <div class="card gradient-scooter">
                <div class="card-body text-center">
                    <!-- <div class="icon-box"><i class="fa fa-home"></i></div> -->
                    <a href="javascript:void();">
                        <h6 class="text-white mt-2"><?php echo $produit['PRODUIT_PA']; ?></h6>
                    </a>
                    <h6 class="text-white mt-2">Prix d'achat</h6>
                </div>
            </div>
        </div>

        <div class="col-12 col-lg-6 col-xl-3">
            <div class="card gradient-bloody">
                <div class="card-body text-center">
                    <!-- <div class="icon-box"><i class="fa fa-rocket"></i></div> -->
                    <a href="javascript:void();">
                        <h6 class="text-white mt-2"><?php echo $produit['PRODUIT_PU']; ?></h6>
                    </a>
                    <h6 class="text-white mt-2">Prix de vente</h6>
                </div>
            </div>
        </div>

        <div class="col">
            <div class="card gradient-quepal">
                <div class="card-body text-center">
                    <!-- <div class="icon-box"><i class="fa fa-trash-o"></i></div> -->
                    <a href="javascript:void();">
                        <h6 class="text-white mt-2"><?php echo $unitemesures['UNITEMESURELIB']  ?></h6>
                    </a>
                    <h6 class="text-white mt-2">Unité de mesure</h6>
                </div>
            </div>
        </div>

    </div>




    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header bg-warning text-white">Ajouter</div>
                <div class="card-body">
                    <form id="form-mesure_conversion" action="" method="POST">
                        <div class="row mb-3">
                            <div class="col">
                                <div class="form-group">
                                    <label></label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">1</span>
                                        </div>


                                        <select class="form-control single-select" id="ID_UNITEMESURE_FROM_ID"
                                            name="UNITEMESURE_FROM_ID" required>
                                            <?php

                                                    $unitemes = new Unitemesure();
                                                    $nitemesures = $unitemes->getAll();
                                                    foreach ($nitemesures as $nitemesure) { ?>
                                            <option value="<?php echo $nitemesure['UNITEMESURE_ID']; ?>">
                                                <?php echo $nitemesure['UNITEMESURELIB']; ?></option>
                                            <?php } ?>
                                        </select>

                                        <div class="input-group-append">
                                            <span class="input-group-text">=</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label></label>
                                    <input id="ID_CONVERSION_VALUE" class="kl-input_decimal" type="number" step="any"
                                        min='0' value="" name="CONVERSION_VALUE" required>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label></label>
                                    <select class="form-control single-select" id="ID_UNITEMESURE_TO_ID"
                                        name="UNITEMESURE_TO_ID" required>
                                        <option value="<?php echo $unitemesures['UNITEMESURE_ID']; ?>">
                                            <?php echo $unitemesures['UNITEMESURELIB']; ?></option>
                                    </select>
                                </div>

                            </div>
                        </div>

                        <div class="form-group kl-btn-form-default">
                            <input type="hidden" id="ID_MESURE_CONVERSION_ID" value="0" name="MESURE_CONVERSION_ID">
                            <input type="hidden" id="ID_PRODUIT_ID" value="<?php echo $produit['PRODUIT_ID']; ?>"
                                name="PRODUIT_ID">
                            <input type="hidden" id="ID_UTILISATEUR_ID" value="<?php echo USER_ID; ?>"
                                name="UTILISATEUR_ID">
                            <button type="submit" name="btn_save_mesure_conversion"
                                class="btn btn-primary shadow-primary px-5 kl-save-mesure_conversion_by_produit"><i
                                    class="icon-lock"></i>Enregistrer</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>



    <table class="table align-items-center table-flush table-dark">
        <thead>
            <tr>
                <!-- <th>Produit</th> -->
                <th>Unité mesure</th>
                <th>Prix d'achat</th>
                <th>Prix de vente</th>
            </tr>
        </thead>
        <tbody>
            <?php
                    if ($unite_mesures_produits != null) {
                        foreach ($unite_mesures_produits as $produit_unite) {
                            $unite = new Unitemesure();
                            $uniteMesure = $unite->get_Unite_Mesure_by_id($produit_unite['UNITEMESURE_ID']);

                            if ($produit_unite['UNITEMESURE_ID'] == $unite_base) {
                            } else {
                                $conversion_mesure=new Mesure_conversion();
                                $conversions=$conversion_mesure->get_mesure_conversion_by_Id_From_and_Id_To($produit_unite['UNITEMESURE_ID'],$unite_base);
                                var_dump($conversions);
                    ?>
            <tr>
                <!-- <td><?php echo $produit['PRODUIT_LIBELLE']  ?></td> -->
                <td><?php echo 1 .' '. $uniteMesure['UNITEMESURELIB'].' ' ?>=<?php echo ' '. (int)$conversions['CONVERSION_VALUE'];echo '('. $unitemesures['UNITEMESURELIB'].')' ?>
                </td>
                <td><?php echo $produit_unite['PRODUIT_UNITMES_PA']; ?> Ar</td>
                <td><?php echo $produit_unite['PRODUIT_UNITMES_PU']  ?> Ar</td>
            </tr>
            <?php
                            }
                        }
                    } else { ?>
            <tr>
                <td> aucune mesure des unités</td>
            </tr>
            <?php  }

                    ?>

        </tbody>
    </table>

    <script src="<?php echo URL; ?>/assets/plugins/inputtags/js/bootstrap-tagsinput.js"></script>
    <script src="<?php echo URL; ?>/assets/plugins/bootstrap-touchspin/js/jquery.bootstrap-touchspin.js"></script>
    <script src="<?php echo URL; ?>/assets/plugins/bootstrap-touchspin/js/bootstrap-touchspin-script.js"></script>

    <!--Select Plugins Js-->
    <script src="<?php echo URL; ?>/assets/plugins/select2/js/select2.min.js"></script>
    <!--Inputtags Js-->
    <script src="<?php echo URL; ?>/assets/plugins/inputtags/js/bootstrap-tagsinput.js"></script>
    <script src="<?php echo URL; ?>/assets/plugins/select2/js/select2.min.js"></script>

    <script>
    $(document).ready(function() {
        $('.single-select').select2();
    });
    </script>
    <?php
    }
}
