<?php
class CommandeHelpers
{
    // public $cmds=0;
    public static function get_lign_cmd()
    {
        require_once(rot . ds . 'models/Lign_cmd.php');
        $Lign_cmd = new Lign_cmd();
        $Lign_cmds = $Lign_cmd->get_all_cmd_suplement();
?>
        <table class="table align-items-center table-flush table-dark">
            <thead>
                <tr>
                    <th>Produit</th>
                    <th>Unité de mesure</th>
                    <th>PU</th>
                    <th>Quatité</th>
                    <th>Prix total</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php


                $total_ucodus = 0;
                $total_detail = 0;

                foreach ($Lign_cmds as $LIGN) {
                    require_once(rot . ds . 'models/Produit.php');
                    require_once(rot . ds . 'models/unitemesure.php');
                    require_once(rot . ds . 'models/produit_unitme.php');
                    $unitemesure = new Unitemesure();
                    $unitemesure = $unitemesure->get_Unite_Mesure_by_id($LIGN['unit_mes']);

                    $produit = new produit();
                    $produit = $produit->get_produit_by_id($LIGN['prod_id']);
                    $Produit_unitme = new Produit_unitme();
                    $prod_unite_mes = $Produit_unitme->get_Unite_Mesure_by_id_produit_To_Id($LIGN['prod_id'], $LIGN['unit_mes']);
                    $sign = $LIGN['unit_mes'] == -1 ? -1 : 1;
                    $prod_unit_prix_unit_ucodis = $prod_unite_mes['prod_unit_prix_unit_ucodis'] * ($sign * $LIGN['valeur']);
                    $prod_unit_prix_unit_detaille = floatval($prod_unite_mes['prod_unit_prix_unit_detaille'] * ($sign * $LIGN['valeur'])) ;

                    $total_ucodus += $prod_unit_prix_unit_ucodis;
                    $total_detail += $prod_unit_prix_unit_detaille;
                ?>
                    <tr>
                        <td><?php echo $produit['produit_libelle']  ?></td>
                        <td><?php echo $unitemesure['unite_mesure_code']  ?></td>
                        <td><?php echo type_vente == 1 ? $prod_unite_mes['prod_unit_prix_unit_detaille'] : $prod_unite_mes['prod_unit_prix_unit_ucodis'] ?></td>
                        <td><?php echo ' x ' . $LIGN['valeur'] ?></td>
                        <td><?php echo type_vente == 1 ? $prod_unit_prix_unit_detaille : $prod_unit_prix_unit_ucodis ?></td>
                        <td><button data-id="<?php echo $LIGN['id']; ?>" class="btn btn-sm btn-outline-danger kl-btn-delete-lign-cmd">Supprimer</button></td>
                    </tr>
                <?php  }  ?>
                <tr>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th><?php echo type_vente == 1 ? $total_detail : $total_ucodus ?> Ar</th>
                    <th></th>

                </tr>
            </tbody>
        </table>

<?php


    }
}
