<?php

class produit_unite_mesureHelpers
{
    public function list_pagination($page_no, $limit, $chercher = NULL)
    {
        require_once(rot . ds . 'app/Controller.php');
        require_once(rot . ds . 'models/produit_unitme.php');
        $produit_unite_mes = new Produit_unitme();
        $offset = ($page_no) * $limit;
        if ($chercher != NULL) {
            $produits = $produit_unite_mes->chercher($chercher);
            $totalPage = count($produits);
            $totalPage = ceil($totalPage / $limit);
        } else {
            $produits = $produit_unite_mes->get_unite_mesure_pagination($offset, $limit);
            $totalPage = $produit_unite_mes->count_nb_prod_with_unite_mesure();
            $totalPage = ceil($totalPage / $limit);
        }
?>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table">
                    <thead class="thead-secondary">
                        <tr>
                            <th scope="col">Libelle</th>
                            <th scope="col">Unite de mesure</th>
                            <th scope="col">Prix d'achat</th>
                            <th scope="col">Prix Ucodis</th>
                            <th scope="col">Prix Detaille</th>
                            <th class="text-center" scope="col">action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php

                        foreach ($produits as $prod) { ?>
                            <tr>
                                <th scope="row"><?php echo $prod['prod_libell'] ?></th>
                                <td><?php echo $prod['unite_mesure_code'] ?></td>
                                <td><?php echo $prod['prod_unit_prix_achat'] ?></td>
                                <td><?php echo $prod['prod_unit_prix_unit_ucodis'] ?></td>
                                <td><?php echo $prod['prod_unit_prix_unit_detaille'] ?></td>
                                <td>
                                    <button type="button" data-id="4" class="btn btn-primary shadow-primary waves-effect waves-light m-1 kl-edit-client">Editer</button>
                                    <button type="button" data-id="4" class="btn btn-primary shadow-primary waves-effect waves-light m-1 kl-edit-client">unite mesure</button>
                                    <button type="button" class="btn btn-danger shadow-danger waves-effect waves-light m-1">Suprimer</button>
                                </td>
                            </tr>
                        <?php }
                        ?>
                    </tbody>

                </table>
            </div>


            <div class="col-lg-12 ">
                <ul class="pagination pagination-separate pagination-outline-secondary">
                    <?php

                    for ($i = 0; $i < $totalPage; $i++) {
                        if ($i == 0) {
                    ?>
                            <li class="page-item <?php echo $page_no == $i ? 'active' : '' ?>"><a class="page-link kl-a-link-pagination" data-id="<?php echo $i ?>" value="<?php echo $i ?>" href="javascript:void();">1</a></li>
                        <?php
                        } elseif ($i == $totalPage - 1) {
                        ?>
                            <li class="page-item <?php echo $page_no == $i ? 'active' : '' ?>"><a class="page-link kl-a-link-pagination " data-id="<?php echo $i ?>" value="<?php echo $i ?>" href="javascript:void();"><?php echo $i + 1 ?></a></li>
                        <?php
                        } else { ?>
                            <li class="page-item <?php echo $page_no == $i ? 'active' : '' ?>"><a class="page-link kl-a-link-pagination " data-id="<?php echo $i ?>" value="<?php echo $i ?>" href="javascript:void();"><?php echo $i + 1 ?></a></li>
                    <?php  }
                    }
                    ?>
                </ul>
            </div>
        </div>

        <script>
            var produit_unite_mesure_Ajax = "<?php echo b_url ?>/produit_unite_mesure/pagination";
            var produit_unite_mesure_chercher_Ajax = "<?php echo b_url ?>/produit_unite_mesure/chercher";
        </script>
        <script src="<?php echo URL; ?>/assets/js/jquery.min.js"></script>
        <script src="<?php echo URL; ?>/assets/js_stock/produit_unite_mesure.js"></script>
    <?php
    }

    public function get_product_unite($id_produit)
    {
        require_once(rot . ds . 'models/produit_unitme.php');
        require_once(rot . ds . 'models/Produit.php');
        $produit = new Produit();
        $Prod =  $produit->get_produit_by_id($id_produit);
        $produit_unite_mes = new Produit_unitme();
        $produit_unites = $produit_unite_mes->get_Unite_Mesure_by_id_produit($id_produit);
        // var_dump($produit_unites);
    ?>
        <div class="table-responsive">
            <table class="table table-bordered table-striped">
                <thead class="thead-success">
                    <tr>
                        <th>Unite de mesure</th>
                        <th>Prix d'achat</th>
                        <th>Prix ucodis</th>
                        <th>prix détaille</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    // $base = '(' . $Prod['produit_unit_mes_id'] .')';
                    foreach ($produit_unites as $produit_uni) {

                    ?>
                        <tr>
                            <td><?php echo $produit_uni['unite_mesure_code']  ?>
                                <?php echo  $Prod['produit_unit_mes_id'] == $produit_uni['unit_mes_id'] ? '(Unité de Base)' : '' ?></td>
                            <td><?php echo $produit_uni['prod_unit_prix_achat']  ?> Ar</td>
                            <td><?php echo $produit_uni['prod_unit_prix_unit_ucodis']; ?> Ar</td>
                            <td><?php echo $produit_uni['prod_unit_prix_unit_detaille']  ?> Ar</td>
                        </tr>
                    <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
    <?php
    }
    public function get_select_option_unite($id_produit)
    {

        require_once(rot . ds . 'models/produit_unitme.php');
        $Produit_unitme = new Produit_unitme();
        $nitemesures = $Produit_unitme->get_All_unite_mesure_by_produit_Id($id_produit);

    ?>
        <div class="form-group">
            <label class="text-info">Unité de mesure correspondant</label>
            <select class="form-control single-select kl_UNITEMESURE_ID" id="ID_UNITEMESURE_ID" name="unite_mesure_id" required>
                <?php foreach ($nitemesures as $nitemesure) { ?>
                    <option value="<?php echo $nitemesure['unite_mesure_id']; ?>"><?php echo $nitemesure['unite_mesure_code']; ?></option>
                <?php } ?>
            </select>
        </div>

    <?php
    }

    public function get_unite_Qte_produit_Id($id_produit)
    {

        require_once(rot . ds . 'models/Produit.php');
        $pro = new Produit();
        $prods = $pro->get_produit_by_id($_POST['id_prod']);
        $produit_stock_courent = $prods['produit_stock_courent'];
    ?>
        <div class="form-group">
            <label class="text-info">Quantité en stock</label>
            <input type="text" value="<?php echo $produit_stock_courent ?>" disabled="disabled" class="form-control">
            <input type="hidden" id="id-qte-en-stock" class="kl-input_decimal" step="any" value="<?php echo $produit_stock_courent ?>">
        </div>


<?php
    }
}
