-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  lun. 01 fév. 2021 à 14:33
-- Version du serveur :  10.4.10-MariaDB
-- Version de PHP :  7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `db_gest_stock_vent`
--

DELIMITER $$
--
-- Procédures
--
DROP PROCEDURE IF EXISTS `save_lign_cm`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `save_lign_cm` (IN `prod_id` INT, IN `unit_mes` INT, IN `valeur` DECIMAL)  BEGIN
INSERT INTO save_lign_cmd_prod(id,prod_id, unit_mes, valeur) VALUES (NULL,prod_id,unit_mes,valeur);
END$$

DROP PROCEDURE IF EXISTS `test_definer`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `test_definer` (IN `id` INT)  BEGIN
SELECT * FROM unite_mesure where unite_mesure_id=id;
END$$

DROP PROCEDURE IF EXISTS `update_lign_cm`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `update_lign_cm` (IN `id` INT, IN `valeur` DECIMAL)  BEGIN
 UPDATE save_lign_cmd_prod SET valeur=valeur WHERE id=id;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `acces_vente`
--

DROP TABLE IF EXISTS `acces_vente`;
CREATE TABLE IF NOT EXISTS `acces_vente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `type` (`type`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `acces_vente`
--

INSERT INTO `acces_vente` (`id`, `type`) VALUES
(1, 'detaille'),
(2, 'ucodis');

-- --------------------------------------------------------

--
-- Structure de la table `client`
--

DROP TABLE IF EXISTS `client`;
CREATE TABLE IF NOT EXISTS `client` (
  `client_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_num` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `client_nom` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `client_prenom` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `client_nif` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `client_stat` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `client_adress` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'AUCUNE',
  `client_date_update` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`client_id`),
  UNIQUE KEY `client_num` (`client_num`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `client`
--

INSERT INTO `client` (`client_id`, `client_num`, `client_nom`, `client_prenom`, `client_nif`, `client_stat`, `client_adress`, `client_date_update`) VALUES
(1, 'CLI0', 'RABENANDRASANA', 'Fidinisins Christian', '235356456', '456464566', 'Ambany lyceer', '2021-01-24 05:50:57'),
(8, '0', 'FAH ', 'Betrena', '54656456', '456465', 'alakamisy', '2021-01-31 11:28:04'),
(3, 'CLI2', 'df', 'fdg', 'fdg', 'fdg', 'dfg', '2021-01-24 05:43:47'),
(4, 'CLI4', 'fidinia', 'chris', '', '', 'anba', '2021-01-24 05:58:25'),
(5, 'CLI5', 'fidiniawr', 'chris', 'sd', 'sdv', 'anbadfsdf', '2021-01-24 05:57:42'),
(6, 'CLI6', 'fidinia eeee', 'chris', '', '', 'anba', '2021-01-24 06:00:24'),
(7, 'CLI7', 'rika', 'g', 'c', 'v', 'a', '2021-01-24 06:01:05');

-- --------------------------------------------------------

--
-- Structure de la table `cmd_line`
--

DROP TABLE IF EXISTS `cmd_line`;
CREATE TABLE IF NOT EXISTS `cmd_line` (
  `cmd_line_id` int(11) NOT NULL AUTO_INCREMENT,
  `cmd_id` int(11) NOT NULL,
  `prod_id` int(11) NOT NULL,
  `unit_mes_id` int(11) NOT NULL,
  `cmd_line_qte` decimal(50,2) NOT NULL,
  `cmd_line_montant` decimal(50,2) NOT NULL,
  `cmd_line_remise` decimal(50,2) NOT NULL DEFAULT 0.00,
  `cmd_line_date` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `cmd_line_prix_unite` decimal(50,2) NOT NULL,
  `cmd_line_prix_emballage` decimal(50,2) NOT NULL DEFAULT 0.00,
  `cmd_line_montant_emballage` decimal(50,2) NOT NULL DEFAULT 0.00,
  PRIMARY KEY (`cmd_id`,`unit_mes_id`),
  UNIQUE KEY `cmd_line_id` (`cmd_line_id`),
  KEY `cmd_id` (`cmd_id`),
  KEY `prod_id` (`prod_id`),
  KEY `unit_mes_id` (`unit_mes_id`)
) ENGINE=MyISAM AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `cmd_line`
--

INSERT INTO `cmd_line` (`cmd_line_id`, `cmd_id`, `prod_id`, `unit_mes_id`, `cmd_line_qte`, `cmd_line_montant`, `cmd_line_remise`, `cmd_line_date`, `cmd_line_prix_unite`, `cmd_line_prix_emballage`, `cmd_line_montant_emballage`) VALUES
(1, 9, 35, 50, '10.00', '90.00', '0.00', '2021-01-31 09:36:32', '9.00', '8.00', '80.00'),
(2, 10, 33, 50, '0.00', '0.00', '0.00', '2021-01-31 11:17:28', '9.00', '8.00', '0.00'),
(3, 11, 31, 4, '1.00', '10.00', '0.00', '2021-01-31 11:23:47', '10.00', '8.00', '8.00'),
(4, 11, 36, 50, '1.00', '9.00', '0.00', '2021-01-31 11:23:47', '9.00', '8.00', '8.00'),
(5, 11, 38, 72, '1.00', '375.00', '0.00', '2021-01-31 11:23:47', '375.00', '150.00', '150.00'),
(6, 12, 40, 3, '10.00', '110.00', '0.00', '2021-01-31 11:41:57', '11.00', '0.00', '0.00'),
(7, 12, 40, 80, '10.00', '550.00', '0.00', '2021-01-31 11:41:57', '55.00', '0.00', '0.00'),
(8, 13, 40, 3, '16.90', '185.90', '0.00', '2021-01-31 12:36:14', '11.00', '0.00', '0.00'),
(9, 13, 40, 80, '17.00', '935.00', '0.00', '2021-01-31 12:36:14', '55.00', '0.00', '0.00'),
(10, 14, 40, 3, '11.00', '121.00', '0.00', '2021-01-31 12:45:01', '11.00', '0.00', '0.00'),
(11, 14, 40, 80, '5.00', '275.00', '0.00', '2021-01-31 12:45:01', '55.00', '0.00', '0.00'),
(12, 15, 40, 3, '15.00', '165.00', '0.00', '2021-01-31 12:48:17', '11.00', '0.00', '0.00'),
(13, 16, 40, 3, '9.00', '99.00', '0.00', '2021-01-31 12:51:13', '11.00', '0.00', '0.00'),
(14, 16, 40, 80, '10.00', '550.00', '0.00', '2021-01-31 12:51:13', '55.00', '0.00', '0.00'),
(15, 17, 40, 3, '2.00', '22.00', '0.00', '2021-01-31 12:52:28', '11.00', '0.00', '0.00'),
(16, 17, 40, 80, '11.00', '605.00', '0.00', '2021-01-31 12:52:28', '55.00', '0.00', '0.00'),
(17, 18, 40, 3, '8.00', '88.00', '0.00', '2021-01-31 12:55:54', '11.00', '0.00', '0.00'),
(18, 19, 40, 3, '1.00', '11.00', '0.00', '2021-01-31 12:57:33', '11.00', '0.00', '0.00'),
(19, 20, 40, 3, '1.00', '11.00', '0.00', '2021-01-31 12:58:17', '11.00', '0.00', '0.00'),
(20, 21, 40, 3, '1.00', '11.00', '0.00', '2021-01-31 01:01:06', '11.00', '0.00', '0.00'),
(21, 22, 35, 50, '10.00', '80.00', '0.00', '2021-01-31 01:02:58', '8.00', '8.00', '80.00'),
(22, 23, 40, 3, '12.00', '132.00', '0.00', '2021-01-31 03:18:50', '11.00', '0.00', '0.00'),
(23, 23, 40, 80, '8.00', '440.00', '0.00', '2021-01-31 03:18:50', '55.00', '0.00', '0.00'),
(24, 23, 35, 50, '11.00', '88.00', '0.00', '2021-01-31 03:18:50', '8.00', '8.00', '88.00'),
(25, 24, 40, 3, '18.00', '216.00', '0.00', '2021-02-01 09:47:20', '12.00', '0.00', '0.00'),
(26, 24, 40, 80, '1.00', '60.00', '0.00', '2021-02-01 09:47:20', '60.00', '0.00', '0.00'),
(27, 24, 35, 50, '8.00', '72.00', '0.00', '2021-02-01 09:47:20', '9.00', '8.00', '64.00');

-- --------------------------------------------------------

--
-- Structure de la table `commande_client`
--

DROP TABLE IF EXISTS `commande_client`;
CREATE TABLE IF NOT EXISTS `commande_client` (
  `cmd_id` int(11) NOT NULL AUTO_INCREMENT,
  `cmd_num` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'COMMANDE',
  `client_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `cmd_date` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `cmd_valide` int(1) NOT NULL DEFAULT 0,
  `type_vente` int(11) NOT NULL,
  PRIMARY KEY (`cmd_id`),
  UNIQUE KEY `cmd_num` (`cmd_num`),
  KEY `user_id` (`user_id`),
  KEY `client_id` (`client_id`),
  KEY `type_vente` (`type_vente`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `commande_client`
--

INSERT INTO `commande_client` (`cmd_id`, `cmd_num`, `client_id`, `user_id`, `cmd_date`, `cmd_valide`, `type_vente`) VALUES
(1, 'CMD202101310001', 0, 1, '2021-01-31 08:28:49', 0, 2),
(2, 'CMD202101310002', 0, 1, '2021-01-31 08:28:55', 0, 2),
(3, 'CMD202101310003', 0, 1, '2021-01-31 08:30:10', 0, 2),
(4, 'CMD202101310004', 0, 1, '2021-01-31 08:50:41', 0, 2),
(5, 'CMD202101310005', 0, 1, '2021-01-31 08:57:59', 0, 2),
(6, 'CMD202101310006', NULL, 1, '2021-01-31 09:01:12', 0, 2),
(7, 'CMD202101310007', 4, 1, '2021-01-31 09:34:27', 0, 2),
(8, 'CMD202101310008', 4, 1, '2021-01-31 09:35:57', 0, 2),
(9, 'CMD202101310009', 4, 1, '2021-01-31 09:36:32', 0, 2),
(10, 'CMD202101310010', 1, 1, '2021-01-31 11:17:28', 0, 2),
(11, 'CMD202101310011', 5, 1, '2021-01-31 11:23:47', 0, 2),
(12, 'CMD202101310012', 8, 1, '2021-01-31 11:41:57', 0, 1),
(13, 'CMD202101310013', 0, 1, '2021-01-31 12:36:14', 0, 1),
(14, 'CMD202101310014', 8, 1, '2021-01-31 12:45:01', 0, 1),
(15, 'CMD202101310015', 3, 1, '2021-01-31 12:48:17', 0, 1),
(16, 'CMD202101310016', 5, 1, '2021-01-31 12:51:13', 0, 1),
(17, 'CMD202101310017', 8, 1, '2021-01-31 12:52:28', 0, 1),
(18, 'CMD202101310018', 0, 1, '2021-01-31 12:55:54', 0, 1),
(19, 'CMD202101310019', 1, 1, '2021-01-31 12:57:33', 0, 1),
(20, 'CMD202101310020', 0, 1, '2021-01-31 12:58:17', 0, 1),
(21, 'CMD202101310021', 1, 1, '2021-01-31 01:01:06', 0, 1),
(22, 'CMD202101310022', 1, 1, '2021-01-31 01:02:58', 0, 1),
(23, 'CMD202101310023', 1, 1, '2021-01-31 03:18:50', 0, 1),
(24, 'CMD202101310024', 1, 1, '2021-02-01 09:47:20', 0, 2);

-- --------------------------------------------------------

--
-- Structure de la table `conversion`
--

DROP TABLE IF EXISTS `conversion`;
CREATE TABLE IF NOT EXISTS `conversion` (
  `conversion_id` int(11) NOT NULL AUTO_INCREMENT,
  `unit_to_conver` int(11) NOT NULL,
  `unite_mes` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `value` int(11) NOT NULL,
  PRIMARY KEY (`unit_to_conver`,`unite_mes`),
  KEY `conversion_index` (`conversion_id`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `conversion`
--

INSERT INTO `conversion` (`conversion_id`, `unit_to_conver`, `unite_mes`, `user_id`, `value`) VALUES
(4, 28, 5, 1, 10),
(3, -1, -1, 1, 1),
(5, 4, 5, 1, 3),
(6, 77, 77, 1, 8),
(7, 76, 76, 1, 4),
(8, 73, 24, 1, 9),
(9, 75, 50, 1, 7),
(10, 4, 32, 1, 7),
(11, 75, 26, 1, 7),
(12, 1, 1, 1, 7),
(13, 76, 40, 1, 7),
(14, 21, 42, 1, 9),
(15, 16, 3, 1, 13),
(16, 72, 45, 1, 12),
(17, 72, -1, 1, 15),
(18, 64, 50, 1, 20),
(19, 78, 78, 1, 20),
(20, 79, 78, 1, 100),
(21, 80, 3, 1, 5);

-- --------------------------------------------------------

--
-- Structure de la table `famille`
--

DROP TABLE IF EXISTS `famille`;
CREATE TABLE IF NOT EXISTS `famille` (
  `famille_id` int(11) NOT NULL AUTO_INCREMENT,
  `famille_libelle` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`famille_id`),
  UNIQUE KEY `famille_libelle` (`famille_libelle`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `famille`
--

INSERT INTO `famille` (`famille_id`, `famille_libelle`) VALUES
(1, 'famille 1'),
(2, 'FAM 1'),
(3, 'FAM 2');

-- --------------------------------------------------------

--
-- Structure de la table `groupe`
--

DROP TABLE IF EXISTS `groupe`;
CREATE TABLE IF NOT EXISTS `groupe` (
  `groupe_id` int(11) NOT NULL AUTO_INCREMENT,
  `groupe_libelle` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`groupe_id`),
  UNIQUE KEY `groupe_libelle` (`groupe_libelle`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `groupe`
--

INSERT INTO `groupe` (`groupe_id`, `groupe_libelle`) VALUES
(1, 'groupe 1'),
(2, 'BOMBON\r\n'),
(3, 'TEST\r\n');

-- --------------------------------------------------------

--
-- Structure de la table `lign_payement`
--

DROP TABLE IF EXISTS `lign_payement`;
CREATE TABLE IF NOT EXISTS `lign_payement` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_somme_cmd` int(11) NOT NULL,
  `montant` decimal(50,2) NOT NULL DEFAULT 0.00,
  `date_paye` datetime DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `id_somme_cmd` (`id_somme_cmd`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `lign_payement`
--

INSERT INTO `lign_payement` (`id`, `id_somme_cmd`, `montant`, `date_paye`) VALUES
(1, 24, '20.00', '2021-02-01 13:19:10'),
(2, 24, '50.00', '2021-02-01 13:23:38');

--
-- Déclencheurs `lign_payement`
--
DROP TRIGGER IF EXISTS `add_lign_payement`;
DELIMITER $$
CREATE TRIGGER `add_lign_payement` AFTER INSERT ON `lign_payement` FOR EACH ROW UPDATE somme_cmd_client SET reste=reste-new.montant WHERE id_cmd=new.id_somme_cmd
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `produit`
--

DROP TABLE IF EXISTS `produit`;
CREATE TABLE IF NOT EXISTS `produit` (
  `produit_id` int(11) NOT NULL AUTO_INCREMENT,
  `produit_unit_mes_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `famille_id` int(11) NOT NULL DEFAULT 0,
  `groupe_id` int(11) NOT NULL,
  `produit_cageot_id` int(11) NOT NULL DEFAULT 0,
  `produit_code` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `produit_libelle` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `produit_date_update` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `produit_prix_achat` decimal(50,2) NOT NULL,
  `produit_prix_unit_ucodis` decimal(50,2) NOT NULL,
  `produit_unit_detaille` decimal(50,2) NOT NULL,
  `produit_stock_min` decimal(50,2) NOT NULL,
  `produit_stock_courent` decimal(50,2) NOT NULL,
  `produit_is_emballage` int(1) NOT NULL DEFAULT 0,
  `produit_prix_emballage` decimal(50,2) NOT NULL DEFAULT 0.00,
  `produit_is_cageot` int(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`produit_id`),
  UNIQUE KEY `produit_code` (`produit_code`),
  KEY `produit_unit_mes_id` (`produit_unit_mes_id`),
  KEY `user_id` (`user_id`),
  KEY `famille_id` (`famille_id`),
  KEY `groupe_id` (`groupe_id`),
  KEY `produit_cageot_id` (`produit_cageot_id`),
  KEY `groupe_id_2` (`groupe_id`),
  KEY `groupe_id_3` (`groupe_id`)
) ENGINE=MyISAM AUTO_INCREMENT=41 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `produit`
--

INSERT INTO `produit` (`produit_id`, `produit_unit_mes_id`, `user_id`, `famille_id`, `groupe_id`, `produit_cageot_id`, `produit_code`, `produit_libelle`, `produit_date_update`, `produit_prix_achat`, `produit_prix_unit_ucodis`, `produit_unit_detaille`, `produit_stock_min`, `produit_stock_courent`, `produit_is_emballage`, `produit_prix_emballage`, `produit_is_cageot`) VALUES
(1, 1, 1, 0, 1, 0, 'PRO1', 'pecto', '2021-01-24 16:06:40', '500.00', '550.00', '600.00', '1000.00', '0.00', 1, '0.00', 0),
(2, 2, 1, 0, 1, 0, 'PRO2', 'gfh', '2021-01-24 16:07:07', '77.00', '77.00', '77.00', '600.00', '0.00', 0, '0.00', 0),
(3, 5, 1, 0, 1, 0, 'PRO3', 'BOUTAKA', '2021-01-24 16:07:03', '500.00', '450.00', '600.00', '100.00', '1000.00', 0, '0.00', 0),
(4, 1, 1, 1, 1, 1, 'PRO4', '456', '2021-01-24 02:06:56', '456.00', '456.00', '456.00', '456.00', '0.00', 1, '1546.00', 1),
(5, 1, 1, 1, 1, 1, 'PRO5', '456', '2021-01-24 02:08:53', '456.00', '456.00', '456.00', '456.00', '0.00', 1, '1546.00', 1),
(6, 1, 1, 1, 1, 1, 'PRO6', '456', '2021-01-24 02:09:35', '456.00', '456.00', '456.00', '456.00', '0.00', 1, '1546.00', 1),
(7, 2, 1, 1, 1, 4, 'PRO7', 'test', '2021-01-24 02:10:56', '400.00', '500.00', '600.00', '50.00', '0.00', 1, '80.00', 1),
(8, 42, 1, 3, 2, 5, 'PRO8', 'METY LE MODIF okay', '2021-01-25 01:03:30', '1002.00', '2000.00', '3000.00', '100.00', '0.00', 1, '92.00', 1),
(9, 68, 1, 3, 2, 0, 'PR1', 'biscuit', '2021-01-25 00:57:51', '50.00', '90.00', '80.00', '14.00', '0.00', 0, '0.00', 0),
(10, 4, 1, 2, 1, 6, 'PR2', 'sed', '2021-01-25 01:14:02', '8.00', '12.00', '11.00', '11.00', '0.00', 1, '8.00', 1),
(11, 4, 1, 2, 1, 6, 'PR3', 'sed', '2021-01-25 01:14:24', '8.00', '12.00', '11.00', '11.00', '0.00', 1, '8.00', 1),
(12, 3, 1, 2, 3, 0, 'PR4', 'andao koa', '2021-01-25 01:15:38', '12.00', '14.00', '13.00', '37.00', '0.00', 0, '0.00', 0),
(13, 45, 1, 2, 2, 8, 'PR5', 'PRODUIT PROCEDURE', '2021-01-25 01:27:40', '7000.00', '8000.00', '7500.00', '800.00', '0.00', 1, '12.00', 1),
(14, 16, 1, 2, 2, 5, 'PR6', 'produit', '2021-01-25 02:51:17', '6.00', '3.00', '8.00', '8.00', '0.00', 1, '7.00', 1),
(15, 34, 1, 1, 3, 5, 'PR7', 'produit', '2021-01-25 02:51:26', '6.00', '3.00', '8.00', '8.00', '0.00', 1, '7.00', 1),
(16, 34, 1, 1, 3, 5, 'PR8', 'produit sd', '2021-01-25 02:51:36', '8.00', '5.00', '8.00', '8.00', '0.00', 1, '7.00', 1),
(17, 36, 1, 2, 1, 5, 'PR9', 'produit sd', '2021-01-25 02:51:48', '15.00', '5.00', '8.00', '8.00', '0.00', 1, '11.00', 1),
(18, 36, 1, 2, 1, 5, 'PS0', 'produit sdasf', '2021-01-25 02:51:52', '15.00', '5.00', '8.00', '8.00', '0.00', 1, '11.00', 1),
(19, 39, 1, 3, 1, 5, 'PS1', 'produit sdasf', '2021-01-25 02:51:58', '15.00', '5.00', '8.00', '8.00', '0.00', 1, '11.00', 1),
(20, 63, 1, 2, 2, 5, 'PS2', 'produit sdasf', '2021-01-25 02:52:09', '15.00', '11.00', '15.00', '8.00', '0.00', 1, '11.00', 1),
(21, 63, 1, 2, 2, 0, 'PS3', 'produit sllll', '2021-01-25 02:52:45', '15.00', '11.00', '15.00', '8.00', '0.00', 0, '0.00', 0),
(22, 63, 1, 2, 2, 0, 'PS4', 'produit vvvv', '2021-01-25 02:52:50', '15.00', '11.00', '15.00', '8.00', '0.00', 0, '0.00', 0),
(23, 65, 1, 1, 2, 0, 'PS5', 'produit vrty', '2021-01-25 02:52:59', '15.00', '11.00', '15.00', '8.00', '0.00', 0, '0.00', 0),
(24, 16, 1, 2, 3, 0, 'PS6', 'BEX', '2021-01-26 00:55:48', '1.00', '0.00', '0.00', '0.00', '0.00', 0, '0.00', 0),
(25, 63, 1, 1, 2, 0, 'PS7', 'BIN', '2021-01-26 00:56:14', '11.00', '8.00', '13.00', '9.00', '0.00', 0, '0.00', 0),
(26, 62, 1, 3, 1, 18, 'PS8', 'VOTABIA', '2021-01-26 00:56:51', '10.00', '10.00', '10.00', '9.00', '0.00', 1, '12.30', 1),
(27, 36, 1, 1, 1, 6, 'PS9', 'VANM', '2021-01-26 01:00:50', '14.00', '17.00', '13.00', '13.00', '0.00', 1, '10.00', 1),
(28, 47, 1, 3, 1, 6, 'PT0', 'VANM', '2021-01-26 01:01:10', '14.00', '17.00', '13.00', '13.00', '0.00', 1, '10.00', 1),
(29, 47, 1, 3, 1, 6, 'PT1', 'VANMASD', '2021-01-26 01:05:32', '14.00', '17.00', '13.00', '13.00', '0.00', 1, '10.00', 1),
(30, 4, 1, 2, 2, 5, 'PT2', 'PLAND', '2021-01-26 01:06:18', '7.00', '10.00', '9.00', '14.00', '0.00', 1, '0.00', 1),
(31, 4, 1, 2, 2, 5, 'PT3', 'OKAY OKAY', '2021-01-26 01:06:32', '7.00', '10.00', '9.00', '14.00', '0.00', 1, '8.00', 1),
(32, 50, 1, 2, 2, 30, 'PT4', 'METY T', '2021-01-26 01:07:12', '7.00', '9.00', '8.00', '9.00', '0.00', 1, '8.00', 1),
(33, 50, 1, 2, 2, 30, 'PT5', 'METY B', '2021-01-26 01:07:16', '7.00', '9.00', '8.00', '9.00', '0.00', 1, '8.00', 1),
(34, 50, 1, 2, 2, 30, 'PT6', 'METY C', '2021-01-26 01:07:19', '7.00', '9.00', '8.00', '9.00', '0.00', 1, '8.00', 1),
(35, 50, 1, 2, 2, 30, 'PT7', 'METY F', '2021-01-31 04:12:08', '7.00', '9.00', '8.00', '10.00', '20.00', 1, '8.00', 1),
(36, 50, 1, 2, 2, 30, 'PT8', 'METY G', '2021-01-26 01:07:26', '7.00', '9.00', '8.00', '9.00', '0.00', 1, '8.00', 1),
(37, 50, 1, 2, 2, 30, 'PT9', 'METY U', '2021-01-26 01:07:29', '7.00', '9.00', '8.00', '9.00', '0.00', 1, '8.00', 1),
(38, -1, 1, 2, 2, 36, 'PU0', 'TEST UNITE MESURE PROD', '2021-01-28 01:21:47', '10.00', '25.00', '12.00', '17.00', '0.00', 1, '10.00', 1),
(39, 78, 1, 3, 2, 0, 'PU1', 'BUDON FIDY', '2021-01-28 01:20:00', '500.00', '600.00', '650.00', '19.00', '0.00', 1, '200.00', 0),
(40, 3, 1, 2, 2, 0, 'PU2', 'labougie', '2021-01-31 11:35:55', '13.00', '12.00', '11.00', '11.00', '100.00', 0, '0.00', 0);

--
-- Déclencheurs `produit`
--
DROP TRIGGER IF EXISTS `add_produit_unite_mesure_procedure`;
DELIMITER $$
CREATE TRIGGER `add_produit_unite_mesure_procedure` AFTER INSERT ON `produit` FOR EACH ROW INSERT INTO produit_unite_mesure(prod_unit_mes_id, prod_id,prod_libell, unit_mes_id, user_id, prod_unit_prix_achat, prod_unit_prix_unit_ucodis, prod_unit_prix_unit_detaille, prod_comment, prod_unit_prix_emballage)
VALUES (NULL,NEW.produit_id,NEW.produit_libelle,NEW.produit_unit_mes_id,NEW.user_id,
  NEW.produit_prix_achat,NEW.produit_prix_unit_ucodis,NEW.produit_unit_detaille,'Créer lors de la création du produit',NEW.produit_prix_emballage)
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `produit_unite_mesure`
--

DROP TABLE IF EXISTS `produit_unite_mesure`;
CREATE TABLE IF NOT EXISTS `produit_unite_mesure` (
  `prod_unit_mes_id` int(11) NOT NULL AUTO_INCREMENT,
  `prod_id` int(11) NOT NULL,
  `prod_libell` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `unit_mes_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `prod_unit_prix_achat` decimal(50,2) NOT NULL,
  `prod_unit_prix_unit_ucodis` decimal(50,2) NOT NULL,
  `prod_unit_prix_unit_detaille` decimal(50,2) NOT NULL,
  `prod_comment` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `prod_unit_prix_emballage` decimal(50,2) NOT NULL DEFAULT 0.00,
  PRIMARY KEY (`prod_id`,`unit_mes_id`),
  UNIQUE KEY `prod_unit_mes_id` (`prod_unit_mes_id`)
) ENGINE=MyISAM AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `produit_unite_mesure`
--

INSERT INTO `produit_unite_mesure` (`prod_unit_mes_id`, `prod_id`, `prod_libell`, `unit_mes_id`, `user_id`, `prod_unit_prix_achat`, `prod_unit_prix_unit_ucodis`, `prod_unit_prix_unit_detaille`, `prod_comment`, `prod_unit_prix_emballage`) VALUES
(1, 1, 'aaa', 1, 1, '80000.00', '789000.00', '60000.00', 'yt', '0.00'),
(2, 13, 'aaa', 45, 1, '7000.00', '8000.00', '7500.00', 'Créer lors de la création du produit', '12.00'),
(3, 14, 'ewr', 16, 1, '6.00', '3.00', '8.00', 'Créer lors de la création du produit', '7.00'),
(4, 15, 'fdg', 34, 1, '6.00', '3.00', '8.00', 'Créer lors de la création du produit', '7.00'),
(5, 16, 'dfg', 34, 1, '8.00', '5.00', '8.00', 'Créer lors de la création du produit', '7.00'),
(6, 17, 'fg', 36, 1, '15.00', '5.00', '8.00', 'Créer lors de la création du produit', '11.00'),
(7, 18, 'jhk', 36, 1, '15.00', '5.00', '8.00', 'Créer lors de la création du produit', '11.00'),
(8, 19, 'hjk', 39, 1, '15.00', '5.00', '8.00', 'Créer lors de la création du produit', '11.00'),
(9, 20, 'hjk', 63, 1, '15.00', '11.00', '15.00', 'Créer lors de la création du produit', '11.00'),
(10, 21, 'kl;', 63, 1, '15.00', '11.00', '15.00', 'Créer lors de la création du produit', '0.00'),
(11, 22, 'lghj', 63, 1, '15.00', '11.00', '15.00', 'Créer lors de la création du produit', '0.00'),
(12, 23, 'qewq', 65, 1, '15.00', '11.00', '15.00', 'Créer lors de la création du produit', '0.00'),
(13, 29, 'VANMASD', 47, 1, '14.00', '17.00', '13.00', 'Créer lors de la création du produit', '10.00'),
(14, 30, 'PLAND', 4, 1, '7.00', '10.00', '9.00', 'Créer lors de la création du produit', '0.00'),
(15, 31, 'OKAY OKAY', 4, 1, '7.00', '10.00', '9.00', 'Créer lors de la création du produit', '8.00'),
(16, 32, 'METY T', 50, 1, '7.00', '9.00', '8.00', 'Créer lors de la création du produit', '8.00'),
(17, 33, 'METY B', 50, 1, '7.00', '9.00', '8.00', 'Créer lors de la création du produit', '8.00'),
(18, 34, 'METY C', 50, 1, '7.00', '9.00', '8.00', 'Créer lors de la création du produit', '8.00'),
(19, 35, 'METY F', 50, 1, '7.00', '9.00', '8.00', 'Créer lors de la création du produit', '8.00'),
(20, 36, 'METY G', 50, 1, '7.00', '9.00', '8.00', 'Créer lors de la création du produit', '8.00'),
(21, 37, 'METY U', 50, 1, '7.00', '9.00', '8.00', 'Créer lors de la création du produit', '8.00'),
(22, 37, 'METY U', 69, 1, '80000.00', '789000.00', '60000.00', 'creer unite', '0.00'),
(23, 38, 'TEST UNITE MESURE PROD', -1, 1, '10.00', '25.00', '12.00', 'Créer lors de la création du produit', '10.00'),
(24, 13, 'PRODUIT PROCEDURE', 72, 1, '84000.00', '96000.00', '90000.00', 'Creer lors de creation conversion', '144.00'),
(25, 38, 'TEST UNITE MESURE PROD', 72, 1, '150.00', '375.00', '180.00', 'Creer lors de creation conversion', '150.00'),
(26, 37, 'METY U', 64, 1, '140.00', '180.00', '160.00', 'Creer lors de creation conversion', '160.00'),
(27, 39, 'BUDON FIDY', 78, 1, '500.00', '600.00', '650.00', 'Créer lors de la création du produit', '200.00'),
(28, 39, 'BUDON FIDY', 79, 1, '50000.00', '60000.00', '65000.00', 'Creer lors de creation conversion', '20000.00'),
(29, 40, 'labougie', 3, 1, '13.00', '12.00', '11.00', 'Créer lors de la création du produit', '0.00'),
(30, 40, 'labougie', 80, 1, '65.00', '60.00', '55.00', 'Creer lors de creation conversion', '0.00');

-- --------------------------------------------------------

--
-- Structure de la table `role`
--

DROP TABLE IF EXISTS `role`;
CREATE TABLE IF NOT EXISTS `role` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_libelle` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `role_is_admin` int(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`role_id`),
  UNIQUE KEY `role_libelle` (`role_libelle`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `role`
--

INSERT INTO `role` (`role_id`, `role_libelle`, `role_is_admin`) VALUES
(1, 'Super-Administrateur', 1),
(2, 'Administrateur', 1),
(3, 'Caissier', 0);

-- --------------------------------------------------------

--
-- Structure de la table `save_lign_cmd_prod`
--

DROP TABLE IF EXISTS `save_lign_cmd_prod`;
CREATE TABLE IF NOT EXISTS `save_lign_cmd_prod` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prod_id` int(11) NOT NULL,
  `unit_mes` int(11) NOT NULL,
  `valeur` decimal(50,2) NOT NULL,
  PRIMARY KEY (`prod_id`,`unit_mes`),
  KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `somme_cmd_client`
--

DROP TABLE IF EXISTS `somme_cmd_client`;
CREATE TABLE IF NOT EXISTS `somme_cmd_client` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_cmd` int(11) NOT NULL,
  `somme` decimal(50,2) NOT NULL DEFAULT 0.00,
  `reste` decimal(50,2) NOT NULL DEFAULT 0.00,
  `date_cmd` datetime NOT NULL,
  PRIMARY KEY (`id_cmd`),
  KEY `id` (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `somme_cmd_client`
--

INSERT INTO `somme_cmd_client` (`id`, `id_cmd`, `somme`, `reste`, `date_cmd`) VALUES
(1, 24, '6000.00', '5930.00', '2021-02-01 00:00:00');

-- --------------------------------------------------------

--
-- Structure de la table `unite_mesure`
--

DROP TABLE IF EXISTS `unite_mesure`;
CREATE TABLE IF NOT EXISTS `unite_mesure` (
  `unite_mesure_id` int(11) NOT NULL AUTO_INCREMENT,
  `unite_mesure_code` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `unite_mesure_libelle` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `unite_mesure_description` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `unite_mesure_cageot` int(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`unite_mesure_id`),
  UNIQUE KEY `unit_mesure_index` (`unite_mesure_code`)
) ENGINE=MyISAM AUTO_INCREMENT=81 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `unite_mesure`
--

INSERT INTO `unite_mesure` (`unite_mesure_id`, `unite_mesure_code`, `unite_mesure_libelle`, `unite_mesure_description`, `unite_mesure_cageot`) VALUES
(1, 'CONS', 'Consignation', 'Consignation', 1),
(2, 'UNITE', 'UNITE', 'UNITE', 0),
(3, 'BOUTEILLE', 'BOUTEILLE', 'BOUTEILLE', 0),
(4, 'BOUT-25CL', 'BOUT 25 CL', 'BOUT 25 CL', 0),
(5, 'BOUT-30CL', 'BOUT 30 CL', 'BOUT 30 CL', 0),
(6, 'BOUT-33CL', 'BOUT 33 CL', 'BOUT 33 CL', 0),
(7, 'BOUT-34CL', 'BOUT 34 CL', 'BOUT 34 CL', 0),
(8, 'BOUT-35CL', 'BOUT 35 CL', 'BOUT 35 CL', 0),
(9, 'BOUT-37CL', 'BOUT 37 CL', 'BOUT 37 CL', 0),
(10, 'BOUT-45CL', 'BOUT 45 CL', 'BOUT 45 CL', 0),
(-1, 'AVOIR', 'Avoir-Décons', 'Avoir-Déconsignation', 1),
(12, 'BOUT-50CL', 'BOUT 50 CL', 'BOUT 50 CL', 0),
(13, 'BOUT-65CL', 'BOUT 65 CL', 'BOUT 65 CL', 1),
(14, 'BOUT-70CL', 'BOUT 70 CL', 'BOUT 70 CL', 0),
(15, 'BOUT-100CL', 'BOUT 100 CL', 'BOUT 100 CL', 0),
(16, 'BOUT-150CL', 'BOUT 150 CL', 'BOUT 150 CL', 0),
(17, 'BOUT-300CL', 'BOUT 300 CL', 'BOUT 300 CL', 0),
(18, 'CRT-48-25CL', 'CRT 48 25CL', 'CRT 48 25CL', 0),
(19, 'CRT-50-25CL', 'CRT 50 25CL', 'CRT 5 25CL', 0),
(20, 'CRT-6-30CL', 'CRT 6 30CL', 'CRT 6 30CL', 0),
(21, 'CRT-8-30CL', 'CRT 8 30CL', 'CRT 8 30CL', 0),
(22, 'CRT-10-30CL', 'CRT 10 30CL', 'CRT 10 30CL', 0),
(23, 'CRT-12-30CL', 'CRT 12 30CL', 'CRT 12 30CL', 0),
(24, 'CRT-20-30CL', 'CRT 20 30CL', 'CRT 20 30CL', 0),
(25, 'CRT-24-30CL', 'CRT 24 30CL', 'CRT 24 30CL', 0),
(26, 'PAQ-6-30CL', 'PAQ 6 30CL', 'PAQ 6 30CL', 0),
(27, 'PAQ-8-30CL', 'PAQ 8 30CL', 'PAQ 8 30CL', 0),
(28, 'PAQ-10-30CL', 'PAQ 10 30CL', 'PAQ 10 30CL', 0),
(29, 'PAQ-12-30CL', 'PAQ 12 30CL', 'PAQ 12 30CL', 0),
(30, 'PAQ-20-30CL', 'PAQ 20 30CL', 'PAQ 20 30CL', 0),
(31, 'PAQ-24-30CL', 'PAQ 24 30CL', 'PAQ 24 30CL', 0),
(32, 'CRT-6-33CL', 'CRT 6 33CL', 'CRT 6 33CL', 0),
(33, 'CRT-8-33CL', 'CRT 8 33CL', 'CRT 8 33CL', 0),
(34, 'CRT-10-33CL', 'CRT 10 33CL', 'CRT 10 33CL', 0),
(35, 'CRT-12-33CL', 'CRT 12 33CL', 'CRT 12 33CL', 0),
(36, 'CRT-20-33CL', 'CRT 20 33CL', 'CRT 20 33CL', 0),
(37, 'CRT-24-33CL', 'CRT 24 33CL', 'CRT 24 33CL', 0),
(38, 'PAQ-6-33CL', 'PAQ 6 33CL', 'PAQ 6 33CL', 0),
(39, 'PAQ-8-33CL', 'PAQ 8 33CL', 'PAQ 8 33CL', 0),
(40, 'PAQ-10-33CL', 'PAQ 10 33CL', 'PAQ 10 33CL', 0),
(41, 'PAQ-12-33CL', 'PAQ 12 33CL', 'PAQ 12 33CL', 0),
(42, 'PAQ-20-33CL', 'PAQ 20 33CL', 'PAQ 20 33CL', 0),
(43, 'PAQ-24-33CL', 'PAQ 24 33CL', 'PAQ 24 33CL', 0),
(44, 'CRT-6-34CL', 'CRT 6 34CL', 'CRT 6 34CL', 0),
(45, 'CRT-8-34CL', 'CRT 8 34CL', 'CRT 8 34CL', 0),
(46, 'CRT-10-34CL', 'CRT 10 34CL', 'CRT 10 34CL', 0),
(47, 'CRT-12-34CL', 'CRT 12 34CL', 'CRT 12 34CL', 0),
(48, 'CRT-20-34CL', 'CRT 20 34CL', 'CRT 20 34CL', 0),
(49, 'CRT-24-34CL', 'CRT 24 34CL', 'CRT 24 34CL', 0),
(50, 'PAQ-6-34CL', 'PAQ 6 34CL', 'PAQ 6 34CL', 0),
(51, 'PAQ-8-34CL', 'PAQ 8 34CL', 'PAQ 8 34CL', 0),
(52, 'PAQ-10-34CL', 'PAQ 10 34CL', 'PAQ 10 34CL', 0),
(53, 'PAQ-12-34CL', 'PAQ 12 34CL', 'PAQ 12 34CL', 0),
(54, 'PAQ-20-34CL', 'PAQ 20 34CL', 'PAQ 20 34CL', 0),
(55, 'PAQ-24-34CL', 'PAQ 24 34CL', 'PAQ 24 34CL', 0),
(56, 'CRT-6-35CL', 'CRT 6 35CL', 'CRT 6 35CL', 0),
(57, 'CRT-8-35CL', 'CRT 8 35CL', 'CRT 8 35CL', 0),
(58, 'CRT-10-35CL', 'CRT 10 35CL', 'CRT 10 35CL', 0),
(59, 'CRT-12-35CL', 'CRT 12 35CL', 'CRT 12 35CL', 0),
(60, 'CRT-20-35CL', 'CRT 20 35CL', 'CRT 20 35CL', 0),
(61, 'CRT-24-35CL', 'CRT 24 35CL', 'CRT 24 35CL', 0),
(62, 'PAQ-6-35CL', 'PAQ 6 35CL', 'PAQ 6 35CL', 0),
(63, 'PAQ-8-35CL', 'PAQ 8 35CL', 'PAQ 8 35CL', 0),
(64, 'PAQ-10-35CL', 'PAQ 10 35CL', 'PAQ 10 35CL', 0),
(65, 'PAQ-12-35CL', 'PAQ 12 35CL', 'PAQ 12 35CL', 0),
(66, 'PAQ-20-35CL', 'PAQ 20 35CL', 'PAQ 20 35CL', 0),
(67, 'PAQ-24-35CL', 'PAQ 24 35CL', 'PAQ 24 35CL', 0),
(68, 'CRT-6-37CL', 'CRT 6 37CL', 'CRT 6 37CL', 0),
(69, 'CRT-8-37CL', 'CRT 8 37CL', 'CRT 8 37CL', 0),
(70, 'CRT-10-37CL', 'CRT 10 37CL', 'CRT 10 37CL', 0),
(71, 'CRT-12-37CL', 'CRT 12 37CL', 'CRT 12 37CL', 0),
(72, 'BAFLE-504d', 'BAFLE 50Wd', 'DESCd', 1),
(73, 'PC-500', 'PC', 'PC 500', 0),
(74, 't30', 't30', 't30', 0),
(75, '200po', 'd12', 'sdf', 1),
(76, 'w', 'w', 'w', 0),
(77, 'sdf', 'sf', 'sdf', 0),
(78, 'BUDON-20L', 'BUDON 20L', 'BUDON-20L', 0),
(79, 'BUDON-100', 'BUDON 100', 'BUDON 100 L', 0),
(80, 'BOUGI- PAQUET', 'bougi', 'BOUGI- PAQUET', 0);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `user_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `user_prenom` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `user_login` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `user_pwd` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_login` (`user_login`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`user_id`, `role_id`, `user_name`, `user_prenom`, `user_login`, `user_pwd`) VALUES
(1, 2, 'fidy', 'p', 'admin', '123');

-- --------------------------------------------------------

--
-- Structure de la table `user_access_vente`
--

DROP TABLE IF EXISTS `user_access_vente`;
CREATE TABLE IF NOT EXISTS `user_access_vente` (
  `id_access` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  PRIMARY KEY (`id_access`,`id_user`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `user_access_vente`
--

INSERT INTO `user_access_vente` (`id_access`, `id_user`) VALUES
(1, 1),
(2, 1);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
