<?php
abstract class Model
{
    // Informations de la base de données
    private $host = "localhost";
    private $db_name = "db_gest_stock_vent";
    private $username = "root";
    private $password = "";

    // Propriété qui contiendra l'instance de la connexion
    protected $_connexion;

    // Propriétés permettant de personnaliser les requêtes
    public $table;
    public $id;

    /**
     * Fonction d'initialisation de la base de données
     *
     * @return void
     */
    public function getConnection()
    {
        // On supprime la connexion précédente
        $this->_connexion = null;

        // On essaie de se connecter à la base
        try {
            $this->_connexion = new PDO("mysql:host=" . $this->host . ";dbname=" . $this->db_name, $this->username, $this->password);
            $this->_connexion->exec("set names utf8");
        } catch (PDOException $exception) {
            echo "Erreur de connexion : " . $exception->getMessage();
        }
    }

    /**
     * Méthode permettant d'obtenir un enregistrement de la table choisie en fonction d'un id
     *
     * @return void
     */
    public function getOne()
    {
        $sql = "SELECT * FROM " . $this->table . " WHERE id=" . $this->id;
        $query = $this->_connexion->prepare($sql);
        $query->execute();
        return $query->fetch();
    }

    /**
     * Méthode permettant d'obtenir tous les enregistrements de la table choisie
     *
     * @return array
     */
    public function getAll($des = NULL,$id=NULL)
    {
        if ($des != NULL && $id!=NULL) {
            $sql = "SELECT * FROM " . $this->table .' ORDER BY' .$id. 'DESC';
        } else {
            $sql = "SELECT * FROM " . $this->table;
        }
        $sql = "SELECT * FROM " . $this->table;
        $query = $this->_connexion->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    public function get_End_Id($table, $id)
    {
        $sql = "SELECT * FROM " . $table;
        $query = $this->_connexion->prepare($sql);
        $query->execute();
        $lists = $query->fetchAll();
        if (!empty($lists)) {
            foreach ($lists as $list) {
                $id_ = $list[$id];
            }
            return $id_;
        }
        $id_ = 0;
        return $id_;
    }
}
