<?php
abstract class Controller
{
    /**
     * Afficher une vue
     *
     * @param string $fichier
     * @param array $data
     * @return void
     */
    public function render(string $fichier, array $data = [])
    {
        // var_dump(rot.ds.'views/'.strtolower(get_class($this)).'/'.$fichier.'.php');die;
        extract($data);
        // On démarre le buffer de sortie
        ob_start();
        // On génère la vue
        // require_once(rot.ds.'util/css.php');
        require_once(rot . ds . 'views/' . strtolower(get_class($this)) . '/' . $fichier . '.php');



        // $URL__AJAX->explode_url($url_client_ajax);
        // require_once(rot.ds.'util/js.php');
        // On stocke le contenu dans $content
        $content = ob_get_clean();
        // On fabrique le "template"
        require_once(rot . ds . 'views/layout/default.php');
    }

    /**
     * Permet de charger un modèle
     *
     * @param string $model
     * @return void
     */
    public function loadModel(string $model)
    {
        // On va chercher le fichier correspondant au modèle souhaité
        require_once(rot . ds . 'models/' . $model . '.php');
        // On crée une instance de ce modèle. Ainsi "Article" sera accessible par $this->Article
        $this->$model = new $model();
    }
}
