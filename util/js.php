<script src="<?php echo URL; ?>/assets/js/jquery.min.js"></script>
<script src="<?php echo URL; ?>/assets/js/popper.min.js"></script>
<script src="<?php echo URL; ?>/assets/js/bootstrap.min.js"></script>
<!-- simplebar js -->
<script src="<?php echo URL; ?>/assets/plugins/simplebar/js/simplebar.js"></script>
<!-- waves effect js -->
<script src="<?php echo URL; ?>/assets/js/waves.js"></script>
<!-- sidebar-menu js -->
<script src="<?php echo URL; ?>/assets/js/sidebar-menu.js"></script>
<!-- Custom scripts -->
<script src="<?php echo URL; ?>/assets/js/app-script.js"></script>
<!--notification js -->
<script src="<?php echo URL; ?>/assets/plugins/notifications/js/lobibox.min.js"></script>
<script src="<?php echo URL; ?>/assets/plugins/notifications/js/notifications.min.js"></script>
<script src="<?php echo URL; ?>/assets/plugins/notifications/js/notification-custom-script.js"></script>
<script src="<?php echo URL; ?>/assets/plugins/alerts-boxes/js/sweetalert.min.js"></script>
<script src="<?php echo URL; ?>/assets/plugins/alerts-boxes/js/sweet-alert-script.js"></script>

<!-- <script src="<?php echo URL; ?>/assets/js_stock/client.js"></script> -->
