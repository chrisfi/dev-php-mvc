<?php
class Fonction_util
{
    public static function get_date_now__tiree($delimiteur)
    {
        $date = date('Y' . $delimiteur . 'm' . $delimiteur . 'd h:i:s', time());

        return $date;
    }
    public static function convert_date_to_FR_DMY_heurs__slash($date, $langue, $delimiteur)
    {
        $date = new DateTime($date);
        if ($langue == 'fr') {
            $date = date_format($date, 'd' . $delimiteur . 'm' . $delimiteur . 'Y H:i:s');
        } else {
            $date = date_format($date, 'Y' . $delimiteur . 'd' . $delimiteur . 'm H:i:s');
        }


        return $date;
    }

    public static function convert_date_to_FR_DMY($date, $delimiteur)
    {
        $date = new DateTime($date);
        $date = date_format($date, 'd' . $delimiteur . 'm' . $delimiteur . 'Y');

        return $date;
    }

    public static function convert_date_to_DM($date, $delimiteur)
    {
        $date = new DateTime($date);
        $date = date_format($date, 'd' . $delimiteur . 'm');

        return $date;
    }

    public static function convert_date_to_MY($date, $delimiteur)
    {
        $date = new DateTime($date);
        $date = date_format($date, 'm' . $delimiteur . 'Y');

        return $date;
    }

    public static function convert_date_to_Y($date)
    {
        $date = new DateTime($date);
        $date = date_format($date, 'Y');

        return $date;
    }

    public function explode_url($url_client_ajax)
    {

        $url_client_ajax = explode('/', $url_client_ajax);
        $url_client_ajax = $url_client_ajax[1] . '/' . $url_client_ajax[2] . '/ajax';
        return $url_client_ajax;
    }

    public function option_fr()
    {
        return '"language": {
            "sProcessing": "Traitement en cours ...",
            "sLengthMenu": "Afficher _MENU_ lignes",
            "sZeroRecords": "Aucun résultat trouvé",
            "sEmptyTable": "Aucune donnée disponible",
            "sInfo": "Lignes _START_ à _END_ sur _TOTAL_",
            "sInfoEmpty": "Aucune ligne affichée",
            "sInfoFiltered": "(Filtrer un maximum de_MAX_)",
            "sInfoPostFix": "",
            "sSearch": "Chercher:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Chargement...",
            "oPaginate": {
                "sFirst": "Premier",
                "sLast": "Dernier",
                "sNext": "Suivant",
                "sPrevious": "Précédent"
            },
            "oAria": {
                "sSortAscending": ": Trier par ordre croissant",
                "sSortDescending": ": Trier par ordre décroissant"
            }
        }';
    }




    public function converter_montant_to_letter($number)
    {
        $tab = explode('.', $number);
        $formatter = \NumberFormatter::create('fr_FR', \NumberFormatter::SPELLOUT);
        $formatter->setAttribute(\NumberFormatter::FRACTION_DIGITS, 4);
        $formatter->setAttribute(\NumberFormatter::ROUNDING_MODE, \NumberFormatter::ROUND_HALFUP);
        $chf1 = $formatter->format($tab[0]);
        $chf2 = '';
        if (isset($tab[1])) {
            $chf2 = ' virgule ' . $formatter->format($tab[1]);
        }
        $montant = $chf1 . '' . $chf2;
        return $montant;
    }
}
