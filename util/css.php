<link href="<?php echo URL; ?>/assets/css/style.min.css" rel="stylesheet" />
<!-- notifications css -->
<link rel="stylesheet" href="<?php echo URL; ?>/assets/plugins/notifications/css/lobibox.min.css" />
<!-- simplebar CSS-->
<link href="<?php echo URL; ?>/assets/plugins/simplebar/css/simplebar.css" rel="stylesheet" />
<!-- Bootstrap core CSS-->
<link href="<?php echo URL; ?>/assets/css/bootstrap.min.css" rel="stylesheet" />
<!-- animate CSS-->
<link href="<?php echo URL; ?>/assets/css/animate.css" rel="stylesheet" type="text/css" />
<!-- Icons CSS-->
<link href="<?php echo URL; ?>/assets/css/icons.css" rel="stylesheet" type="text/css" />
<!-- Sidebar CSS-->
<link href="<?php echo URL; ?>/assets/css/sidebar-menu.css" rel="stylesheet" />
<!-- Custom Style-->
<link href="<?php echo URL; ?>/assets/css/app-style.css" rel="stylesheet" />

<link rel="icon" href="<?php echo URL; ?>/webroot/assets/images/favicon.ico" type="image/x-icon">
<link href="<?php echo URL; ?>/assets/plugins/bootstrap-datatable/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo URL; ?>/assets/plugins/bootstrap-datatable/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css">

