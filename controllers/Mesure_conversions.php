<?php
class Mesure_conversions extends Controller
{
    public function index($res = null)
    {
        $this->loadModel('mesure_conversion');
        $mesure_conversions = $this->mesure_conversion->getAll();
        $this->render('index', compact('mesure_conversions', 'res'));
    }
    public function add()
    {
        if (isset($_POST['action'])) {
            $action = $_POST['action'];
            switch ($action) {
                case 'add_conversion':
                    $this->loadModel('mesure_conversion');
                    $res = $this->mesure_conversion->add(
                        $_POST['conversion_id'],
                        $_POST['unit_to_conver'],
                        $_POST['unite_mes'],
                        $_POST['user_id'],
                        $_POST['value']
                    );
                    $mes = [];
                    $mes['status'] = $res;
                    $mes['action'] = 'add';
                    $res = $res;
                    $this->index($mes);
            }
        } else {
            $this->render('add');
        }
    }
    public function add_unite_produit_Or_And_unite_Mesure()
    {
        $this->loadModel('mesure_conversion');
        $res = $this->mesure_conversion->add_unite_mesure_by_product_id(
            $_POST['MESURE_CONVERSION_ID'],
            $_POST['UNITEMESURE_FROM_ID'],
            $_POST['UNITEMESURE_TO_ID'],
            $_POST['UTILISATEUR_ID'],
            $_POST['CONVERSION_VALUE'],
            $_POST['PRODUIT_ID']
        );
        echo json_encode(['status' => $res]);
        die;
    }
}
