<?php

class Clients extends Controller
{
    /**
     * Cette méthode affiche la liste des articles
     *
     * @return void
     */
    public function index()
    {
        $this->loadModel('Client');
        $clients = $this->Client->getAll();
        $this->render('index', compact('clients'));
    }

    /**
     * Méthode permettant d'afficher un article à partir de son slug
     *
     * @param string $slug
     * @return void
     */
    public function lire(string $slug)
    {
        $this->loadModel('Client');
        $client = $this->Client->findBySlug($slug);
        $this->render('lire', compact('client'));
    }
    /**
     * Méthode permettant d'afficher un article à partir de son slug
     *
     * @param string $slug
     * @return void
     */
    public function add()
    {
  
        if (empty($_POST)) {
            $this->render('add');
        } else {
            $action = $_POST['btn_save_client'];
            switch ($action) {
                case 'add':
                    if (isset($_POST['CLIENT_NOM']) && isset($_POST['CLIENT_PRENOM']) && isset($_POST['CLIENT_NIF']) && isset($_POST['CLIENT_STAT']) && isset($_POST['CLIENT_ADRESSE']))
                        $this->loadModel('Client');
                    $clients = $this->Client->getAll();
                    if (empty($clients)) {
                        $numero_end = 'CLI0000000';
                    } else {
                        foreach ($clients as $client) {
                            $numero_end = $client['client_num'];
                        }
                    }
                    $numero_end++;
                    $CLIENT_ID = $_POST['CLIENT_ID'] != 0 ? $_POST['CLIENT_ID'] : 0;
                    $CLIENT_NUM = $_POST['CLIENT_NUM'] != 0 ? $_POST['CLIENT_NUM'] : $numero_end;
                    $CLIENT_NOM = $_POST['CLIENT_NOM'];
                    $CLIENT_PRENOM = $_POST['CLIENT_PRENOM'];
                    $CLIENT_NIF = $_POST['CLIENT_NIF'];
                    $CLIENT_STAT = $_POST['CLIENT_STAT'];
                    $CLIENT_ADRESSE = $_POST['CLIENT_ADRESSE'];
                    $CLIENT_DATE_UPD = date('Y-m-d h:i:s', time());
                    $result = $this->Client->save($CLIENT_ID, $CLIENT_NUM, $CLIENT_NOM, $CLIENT_PRENOM, $CLIENT_NIF, $CLIENT_STAT, $CLIENT_ADRESSE, $CLIENT_DATE_UPD);
                    if ($result == 'success') {
                        $data['add'] = $result;
                        $this->render('add', $data);
                    } else {
                        $data['add'] = $result;
                        $this->render('add', $data);
                    }
                    // echo json_encode(['status' => 'ok', 'save' => $result]);
                    die;
                    break;
                case 'edit':
                    $this->loadModel('Client');
                    $CLIENT_NUM = $_POST['CLIENT_NUM'];
                    $CLIENT_ID = $_POST['CLIENT_ID'] != 0 ? $_POST['CLIENT_ID'] : 0;
                    $CLIENT_NOM = $_POST['CLIENT_NOM'];
                    $CLIENT_PRENOM = $_POST['CLIENT_PRENOM'];
                    $CLIENT_NIF = $_POST['CLIENT_NIF'];
                    $CLIENT_STAT = $_POST['CLIENT_STAT'];
                    $CLIENT_ADRESSE = $_POST['CLIENT_ADRESSE'];
                    $CLIENT_DATE_UPD = date('Y-m-d h:i:s', time());
                    $result = $this->Client->save($CLIENT_ID, $CLIENT_NUM, $CLIENT_NOM, $CLIENT_PRENOM, $CLIENT_NIF, $CLIENT_STAT, $CLIENT_ADRESSE, $CLIENT_DATE_UPD);
                       echo json_encode(['status' => 'ok', 'save' =>  $result]);
                    die;
                default:
                    $this->render('add');
            }
        }



        // if (isset($_POST['btn_save_client']) && isset($_POST['CLIENT_NOM']) && isset($_POST['CLIENT_PRENOM']) && isset($_POST['CLIENT_NIF']) && isset($_POST['CLIENT_STAT']) && isset($_POST['CLIENT_ADRESSE']))
        //     $this->loadModel('Client');
        // $clients = $this->Client->getAll();
        // if (empty($clients)) {
        //     $numero_end = 'CLI0000000';
        // } else {
        //     foreach ($clients as $client) {
        //         $numero_end = $client['CLIENT_NUM'];
        //     }
        // }
        // $numero_end++;
        // $CLIENT_ID = $_POST['CLIENT_ID'] != 0 ? $_POST['CLIENT_ID'] : 0;
        // $CLIENT_NUM = $_POST['CLIENT_NUM'] != 0 ? $_POST['CLIENT_NUM'] : $numero_end;
        // $CLIENT_NOM = $_POST['CLIENT_NOM'];
        // $CLIENT_PRENOM = $_POST['CLIENT_PRENOM'];
        // $CLIENT_NIF = $_POST['CLIENT_NIF'];
        // $CLIENT_STAT = $_POST['CLIENT_STAT'];
        // $CLIENT_ADRESSE = $_POST['CLIENT_ADRESSE'];
        // $CLIENT_DATE_UPD = date('Y-m-d h:i:s', time());
        // $result = $this->Client->save($CLIENT_ID, $CLIENT_NUM, $CLIENT_NOM, $CLIENT_PRENOM, $CLIENT_NIF, $CLIENT_STAT, $CLIENT_ADRESSE, $CLIENT_DATE_UPD);
        // echo json_encode(['status' => 'ok', 'save' => $result]);
        // die;

    }
    public function delete()
    {
        if (isset($_POST['CLIENT_ID'])) {
            $this->loadModel('Client');
            $result = $this->Client->delete($_POST['CLIENT_ID']);
            echo json_encode(['status' => 'ok', 'delete' => $result]);
            die;
        }
    }

    public function get_cli()
    {
        if (isset($_POST['CLIENT_ID'])) {
            $this->loadModel('Client');
            $result = $this->Client->get_cli($_POST['CLIENT_ID']);
            if (!empty($result)) {
                echo json_encode(['status' => 'ok', 'client' => $result]);
                die;
            } else {
                echo json_encode(['status' => 'ko', 'error' => $result]);
            }
        }
    }

    /**
     * Méthode permettant d'afficher un article à partir de son slug
     *
     * @param string $slug
     * @return void
     */
    public function save()
    {
        $this->loadModel('Client');
        $clients = $this->Client->getAll();
        $this->render('index', compact('clients'));
    }
}
