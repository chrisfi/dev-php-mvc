<?php

class Main extends Controller
{

    public function index()
    {
        $this->render('index');
    }
    public function page404()
    {
        $this->render('page404');
    }
}
