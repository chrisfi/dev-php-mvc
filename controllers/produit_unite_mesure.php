<?php

class Produit_unite_mesure extends Controller
{
    public function unite_mesure()
    {
        $this->loadModel('Produit_unitme');
        // $totalPage = ceil($totalPage / 5);
        $produits = $this->Produit_unitme->get_unite_mesure_pagination(0, 5);
        $totalPage = $this->Produit_unitme->count_nb_prod_with_unite_mesure();
        $this->render('produit_unite_mesure');
    }
    public function pagination()
    {
        $chercher = NULL;
        if (isset($_POST['searche'])) {
            $chercher = $_POST['searche'];
        }
        require_once(rot . ds . 'helpers/produit_unite_mesureHelpers.php');
        $produit_unite_mesureHelpers = new produit_unite_mesureHelpers();
        $produit_unite_mesureHelpers->list_pagination($_POST['page_no'], $_POST['limit'], $chercher);
        die;
        //  echo json_decode('ok');
    }
    public function chercher()
    {
        // var_dump($_POST);
        // $this->loadModel('Produit_unitme');
        // $res= $this->pagination->chercher($_POST['search_value']);
        // $res = $this->pagination($_POST['page_no'], $_POST['limit'], $_POST['searche']);
        $chercher = NULL;
        if (isset($_POST['searche'])) {
            $chercher = $_POST['searche'];
        }
        require_once(rot . ds . 'helpers/produit_unite_mesureHelpers.php');
        $produit_unite_mesureHelpers = new produit_unite_mesureHelpers();
        $produit_unite_mesureHelpers->list_pagination($_POST['page_no'], $_POST['limit'], $chercher);
        die;
    }

    public function gerer_unite()
    {
        // var_dump($_POST);
        $mesure_conversion = [];
        $mes = [];
        if (
            isset($_POST['produit_id']) && isset($_POST['unite_mesure_id_unite']) && isset($_POST['unite_mesure_id_a_converter'])
            && isset($_POST['valeur_converser'])
        ) {
            $this->loadModel('Mesure_conversion');
            $mesure_conversion = $this->Mesure_conversion->check_mesure_conversion_by_from_id_To_id($_POST['unite_mesure_id_a_converter'], $_POST['unite_mesure_id_unite']);
            /**test si unite de conversion existe */
            $this->loadModel('Produit');
            $Produit_correspond = $this->Produit->get_produit_by_id($_POST['produit_id']);
            $this->loadModel('Produit_unitme');
            /**test si unite de conversion n'existe pas */
            if (empty($mesure_conversion)) {
                $res = $this->Mesure_conversion->add(0, $_POST['unite_mesure_id_a_converter'], $_POST['unite_mesure_id_unite'], USER_ID, $_POST['valeur_converser']);
                if ($res) {
                    $prod_unit_prix_achat = $Produit_correspond['produit_prix_achat'] * $_POST['valeur_converser'];
                    $prod_unit_prix_unit_ucodis = $Produit_correspond['produit_prix_unit_ucodis'] * $_POST['valeur_converser'];
                    $prod_unit_prix_unit_detaille = $Produit_correspond['produit_unit_detaille'] * $_POST['valeur_converser'];
                    $prod_unit_prix_emballage = $Produit_correspond['produit_prix_emballage'] * $_POST['valeur_converser'];
                    $res_add_prod_unite = $this->Produit_unitme->save_produit_unite_mesure(
                        0,
                        $Produit_correspond['produit_id'],
                        $Produit_correspond['produit_libelle'],
                        $_POST['unite_mesure_id_a_converter'],
                        USER_ID,
                        $prod_unit_prix_achat,
                        $prod_unit_prix_unit_ucodis,
                        $prod_unit_prix_unit_detaille,
                        'Creer lors de creation conversion',
                        $prod_unit_prix_emballage
                    );
                    $mes = ['action' => 'add', 'mes' => 'Ajout unite de mesure produit', 'value' => $res_add_prod_unite];
                } else {
                    $mes = ['action' => 'add', 'mes' => 'Ajout unite de conversion', 'value' => $res];
                }
            } else {

                // $res = $this->Mesure_conversion->add(0, $_POST['unite_mesure_id_a_converter'],$_POST['unite_mesure_id_unite'],USER_ID,$_POST['valeur_converser']);
                // if ($res) {

                $prod_unit_prix_achat = $Produit_correspond['produit_prix_achat'] * $mesure_conversion['value'];
                $prod_unit_prix_unit_ucodis = $Produit_correspond['produit_prix_unit_ucodis'] * $mesure_conversion['value'];
                $prod_unit_prix_unit_detaille = $Produit_correspond['produit_unit_detaille'] * $mesure_conversion['value'];
                $prod_unit_prix_emballage = $Produit_correspond['produit_prix_emballage'] * $mesure_conversion['value'];
                /**save unite de produit */
                $res_add_prod_unite = $this->Produit_unitme->save_produit_unite_mesure(
                    0,
                    $Produit_correspond['produit_id'],
                    $Produit_correspond['produit_libelle'],
                    $_POST['unite_mesure_id_a_converter'],
                    USER_ID,
                    $prod_unit_prix_achat,
                    $prod_unit_prix_unit_ucodis,
                    $prod_unit_prix_unit_detaille,
                    'Creer lors de creation conversion',
                    $prod_unit_prix_emballage
                );
                $mes = ['action' => 'add', 'mes' => 'Ajout unité de mesure de produit', 'value' => $res_add_prod_unite];
            }
        }

        if (isset($_GET['id_produit'])) {
            $this->loadModel('Produit_unitme');
            $produit_unites = $this->Produit_unitme->get_Unite_Mesure_by_id_produit($_GET['id_produit']);
        } else {
            $produit_unites = [];
        }

        $this->render('gerer_unite_de_mesure', compact('produit_unites', 'mes'));
    }


    public function get_val_uniteMesureToConverser__unite_base()
    {
        $converser = $_POST['converser'];
        $base = $_POST['unite_mesure_code'];
        $this->loadModel('Mesure_conversion');
        $conversion = $this->Mesure_conversion->check_mesure_conversion_by_from_id_To_id($converser, $base);
        if (!empty($conversion)) {
            echo json_encode(["CONVERSION" => $conversion['value']]);
            die;
        } else {
            echo json_encode(["CONVERSION" => 0]);
            die;
        }
    }

    public function gerer_unite_mesure_produit()
    {
        switch ($_POST['action']) {
            case 'get_list_unite_mesure_produit':
                require_once(rot . ds . 'helpers/produit_unite_mesureHelpers.php');
                $produit_unite_mesureHelpers = new produit_unite_mesureHelpers();
                $produit_unite_mesureHelpers->get_product_unite($_POST['id_produit']);
                die;
                break;
            case 'get_unite_mesure_by_id_product':
                $this->loadModel('Unitemesure');
                $this->loadModel('Produit');
                $Produit = $this->Produit->get_produit_by_id($_POST['id_produit']);
                $unite_mesure = $this->Unitemesure->get_Unite_Mesure_by_id($Produit['produit_unit_mes_id']);
                // var_dump($unite_mesure);
                echo json_encode(["unite_mesure" => $unite_mesure]);
                break;
        }
    }
}
