<?php
class Utilisateurs extends Controller
{
    public function index()
    {
        // users', 'acces_ventes', 'roles
        $this->loadModel('Users');
        $this->loadModel('acces_vente');
        $this->loadModel('role');
        $users = $this->Users->getAll();
        $roles = $this->role->getAll();
        $acces_ventes = $this->acces_vente->getAll();
        $this->render('index', compact('users', 'roles', 'acces_ventes'));
    }

    public function add()
    {
        $this->render('add');
    }

    public function save_new_user()
    {
        $data = [];
        $data['user_name'] = $_POST['user_name'];
        $data['user_prenom'] = $_POST['user_prenom'];
        $data['user_login'] = $_POST['user_login'];
        $data['user_pwd'] = md5($_POST['pass']);
        $data['user_id'] = $_POST['user_id'];
        $this->loadModel('Users');
        $users = $this->Users->save_user($data);
        echo json_encode(['status' =>  $users, 'val' => $data]);
        die;
    }

    public function add_user_vente()
    {
        $data = [];
        $data['id_user'] = $_POST['id_user_vente'];
        $data['id_access'] = $_POST['id__type'];
        $this->loadModel('user_access_vente');
        $vente_users = $this->user_access_vente->save_user_access_Vente($data);
        echo json_encode(['status' => $vente_users]);
        die;
    }

    public function add_role()
    {
        $data = [];
        $data['user_id'] = $_POST['id_user_role'];
        $data['role_id'] = $_POST['id__role'];
        $this->loadModel('User_role');
        $users_roles = $this->User_role->save_role_user($data);
        echo json_encode(['status' => $users_roles]);
        die;
    }
}
