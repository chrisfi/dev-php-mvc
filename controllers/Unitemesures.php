<?php
class Unitemesures extends Controller
{
    public function index()
    {
        $this->loadModel('unitemesure');
        $unitemesures = $this->unitemesure->getAll();
        $reponse = [];
        $this->render('index', compact('unitemesures','reponse'));
    }
    public function get_action()
    {
        $action = $_POST['action'];
        switch ($action) {
            case 'get_client':
                $this->loadModel('unitemesure');
                $unitemesures = $this->unitemesure->get_Unite_Mesure_by_id($_POST['id']);
                echo json_encode(['unitemesures' => $unitemesures]);
                break;
            case 'save_edite':
                $this->loadModel('unitemesure');
                $this->unitemesure->id = 'UNITEMESURECODE';
                $result = $this->unitemesure->save(
                    $_POST['UNITEMESURE_ID'],
                    $_POST['UNITEMESURECODE'],
                    $_POST['UNITEMESURELIB'],
                    $_POST['UNITEMESURE_DESC'],
                    $_POST['UNIT_CAG']
                );
                echo json_encode($result);
                break;
            case 'delete':
                $this->loadModel('unitemesure');
                $result = $this->delete->save();
                echo json_encode($result);
                break;
        }
    }
    public function add()

    {
        $reponse = [];
        $reponse['action'] = '';
        $reponse['value'] = '';
        if (!empty(($_POST))) {
            $this->loadModel('unitemesure');
            $existe = $this->unitemesure->getOne();
            if (empty($existe)) {
                if (isset($_POST['UNIT_CAG'])) {
                    $UNIT_CAG = 1;
                } else {
                    $UNIT_CAG = 0;
                }
                $result = $this->unitemesure->save(
                    $_POST['UNITEMESURE_ID'],
                    $_POST['UNITEMESURECODE'],
                    $_POST['UNITEMESURELIB'],
                    $_POST['UNITEMESURE_DESC'],
                    $UNIT_CAG
                );
                // $mes = $this->unitemesure->mes;
                $this->loadModel('unitemesure');
                $reponse['action'] = 'ajout';
                $reponse['value'] = $result;
                $unitemesures = $this->unitemesure->getAll();
                $this->render('index', compact('unitemesures', 'reponse'));
            } else {
                $reponse['action'] = 'ajout';
                $reponse['value'] = 'Unite de mesure existe';
                $unitemesures = $this->unitemesure->getAll();
                $this->loadModel('unitemesure');
                $this->render('index', compact('unitemesures'));
            }
        } else {

            $this->render('add',compact('reponse'));
        }
    }
}
