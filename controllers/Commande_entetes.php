<?php

class Commande_entetes extends Controller
{
    public $object_cmd = array(array());
    /**
     * Cette méthode affiche la liste des articles
     *
     * @return void
     */
    public function index()
    {
        $this->loadModel('Commande_entete');
        $commande_entetes = $this->Commande_entete->get_all_CMD_by_type(type_vente);
        $this->render('index', compact('commande_entetes'));
    }




    public function detaille_cmd_ligne()
    {
        if (isset($_POST['action'])) {
            $action = $_POST['action'];
            switch ($action) {
                case 'valide_on_cmd_client':
                    $this->loadModel('cmd_line');
                    $this->loadModel('Produit');
                    $this->loadModel('Commande_entete');
                    $this->loadModel('Somme_cmd_client');
                    $this->loadModel('cmd_line');
                    require_once(rot . ds . 'util/fonction_util.php');
                    $fonction_utile = new Fonction_util();
                    $cmds_lines = $this->cmd_line->get_cmd_cli_by_id($_POST['cmd_id']);
                    $res_validates = $this->Produit->update_Qte_produit($cmds_lines);
                    $valise_cmd_tete = $this->Commande_entete->update_valide($_POST['cmd_id'], 1);

                    if ($valise_cmd_tete) {
                        $data = [];
                        $data['id_cmd'] = $_POST['cmd_id'];
                        $data['somme'] = $this->cmd_line->get_somme_montant_by_id_Cmd($_POST['cmd_id'])['somme'];
                        $data['reste'] = $this->cmd_line->get_somme_montant_by_id_Cmd($_POST['cmd_id'])['somme'];
                        $data['date_validation'] = $fonction_utile->get_date_now__tiree('-');
                        $data['user'] = USER_ID;
                        $somme = $this->Somme_cmd_client->save_somme_cmd_client($data);
                        echo json_encode(['status' =>  $somme, 'add somme' => $res_validates]);
                        die;
                    }
                    echo json_encode(['status' =>  $valise_cmd_tete, 'validation cmd' => $res_validates]);
                    die;
                    break;
                case 'pdf':

                    $this->loadModel('Commande_entete');
                    $this->loadModel('cmd_line');
                    $commande_entetes = $this->cmd_line->get_cmd_cli_by_id($_POST['id_cmd']);

                    $Commande_entetes_client = $this->Commande_entete->get_cmd_tete_client_by_id_cmd($_POST['id_cmd']);
                    require_once(rot . ds . 'helpers/pdf.php');
            }
        } else {


            $this->loadModel('cmd_line');
            $this->loadModel('Commande_entete');
            $commande_entetes = $this->cmd_line->get_cmd_cli_by_id(isset($_GET['cmd_id']) ? $_GET['cmd_id'] : 0);

            $Commande_entetes_client = $this->Commande_entete->get_cmd_tete_client_by_id_cmd(isset($_GET['cmd_id']) ? $_GET['cmd_id'] : 0);
            if (!$Commande_entetes_client) {
                $Commande_entetes_client = $this->Commande_entete->get_cmd_tete_id_cmd(isset($_GET['cmd_id']) ? $_GET['cmd_id'] : 0);
            }
            $this->render('detaille_cmd_ligne', compact('commande_entetes', 'Commande_entetes_client'));
        }
    }

    public function add()
    {
        require_once(rot . ds . 'models/Lign_cmd.php');
        $Lign_cmd = new Lign_cmd();
        $Lign_cmds = $Lign_cmd->get_all_cmd_suplement();
        if (isset($_POST['action'])) {
            $action = $_POST['action'];
            switch ($action) {

                    /** save or update one ligne de commande (produit)*/

                case 'lign_cmd':
                    $this->loadModel('Lign_cmd');
                    $cmd = $this->Lign_cmd->save($_POST['id_produit_cmd'], $_POST['unite_mes_cmd'], $_POST['quantite_cmd']);
                    require_once(rot . ds . 'helpers/commandeHelpers.php');
                    $helpers = new CommandeHelpers();
                    $helpers::get_lign_cmd();
                    die;
                    break;


                    /** get Unité de mesure correspondant by id produit */
                case 'ligne_unite_mesure_by_produit':
                    $this->loadModel('Produit_unitme');
                    require_once(rot . ds . 'helpers/produit_unite_mesureHelpers.php');
                    $Produit_unitite_mesure = new produit_unite_mesureHelpers();
                    $Produit_unitite_mesure = $Produit_unitite_mesure->get_select_option_unite($_POST['id_prod']);
                    die;

                    /** supprimer un ligne cmd */
                case 'delete_one_lign_cmd':
                    require_once(rot . ds . 'helpers/commandeHelpers.php');
                    $helpers = new CommandeHelpers();
                    $this->loadModel('Lign_cmd');
                    $res = $this->Lign_cmd->delete_on_lign_by_id($_POST['id_ligne']);
                    $helpers::get_lign_cmd();
                    die;
                    break;

                    /**validation brouillon commande ne modif pas le quantite du produit */
                case 'validation_cmd':
                    require_once(rot . ds . 'models/Lign_cmd.php');
                    require_once(rot . ds . 'models/Commande_entete.php');
                    $Lign_cmd = new Lign_cmd();
                    $cmd_tete_cli = new Commande_entete();
                    /**get fin id -commande client tete */
                    $CMD_TETE = $cmd_tete_cli->save_cmd_client($this->create_new_code(), $_POST['client_id']);

                    if ($CMD_TETE) {
                        $CMD_TETE = $cmd_tete_cli->get_cmd_end_save();
                        $id_cmd_client_tete = $CMD_TETE[0]['cmd_id'];
                        $Lign_cmds = $Lign_cmd->get_all_cmd_suplement();
                        $res = $this->valide_all_cmd($id_cmd_client_tete, $Lign_cmds);
                        $this->loadModel('Lign_cmd');
                        /**delete all brouillon  */
                        $delete_all_lign = $this->Lign_cmd->delete_all_lign_suplementaire();
                        echo json_encode(['status' => 'ok', 'mes' => 'ajout ligne de commande', 'id_cmd_client_tete' => $id_cmd_client_tete, 'nombre' => $res]);
                        die;
                    } else {
                        echo json_encode(['status' => 'ko', 'mes' => 'Erreur ajout commande client']);
                        die;
                    }

                    echo json_encode(['status' => 'ok', 'nb' => $Lign_cmds]);
                    die;
                    break;


                case 'get_unite_Qte_produit_Id':
                    $this->loadModel('Produit_unitme');
                    require_once(rot . ds . 'helpers/produit_unite_mesureHelpers.php');
                    $Produit_unitite_mesure = new produit_unite_mesureHelpers();
                    $Produit_unitite_mesure->get_unite_Qte_produit_Id($_POST['id_prod']);
                    die;
                    break;

                    /**annuler all lign commande client */
                case 'annule_all_cmd':
                    require_once(rot . ds . 'models/Lign_cmd.php');
                    $Lign_cmd = new Lign_cmd();
                    $res = $Lign_cmd->delete_all_lign_suplementaire();
                    echo json_encode(['status' => $res]);
                    die;
                    break;
            }
        } else {
            $this->loadModel('Lign_cmd');
            $this->Lign_cmd->delete_all_lign_suplementaire();
            $this->render('add');
        }
    }

    public function create_new_code()
    {
        require_once(rot . ds . 'models/Commande_entete.php');
        $cmd_tete_cli = new Commande_entete();
        $date = date('Ymd', time());
        $cmd_fin = $cmd_tete_cli->get_cmd_end_save();
        $NEW_CODE = '';
        if (count($cmd_fin) != 0) {
            $NEW_CODE =  $cmd_fin[0]['cmd_num'];
            $NEW_CODE++;
        } else {
            $NEW_CODE = 'CMD' . $date . '0000';
            $NEW_CODE++;
        }
        return $NEW_CODE;
    }

    public function valide_all_cmd($id_cmd_client, $data)
    {
        $resultat = array(count($data));
        // var_dump($data);
        // die;
        require_once(rot . ds . 'models/produit_unitme.php');
        require_once(rot . ds . 'models/Lign_cmd.php');
        if (!empty($data)) {
            foreach ($data as $key => $lign_cmd) {
                $data = [];
                $data['unit_mes_id'] = $unit_mes_id = $lign_cmd['unit_mes'];
                $data['prod_id'] = $prod_id = $lign_cmd['prod_id'];
                $Produit_unitme = new Produit_unitme();
                $Produit_unit_mesure = $Produit_unitme->get_Unite_Mesure_by_id_produit_To_Id($prod_id, $unit_mes_id);
                $data['cmd_id'] = $id_cmd_client;
                $data['cmd_line_qte'] = $unit_mes_id == -1 ? ((-1) * $lign_cmd['valeur']) : $lign_cmd['valeur'];
                $data['cmd_line_montant'] = type_vente == 1 ? ($Produit_unit_mesure['prod_unit_prix_unit_detaille'] * $lign_cmd['valeur']) : ($Produit_unit_mesure['prod_unit_prix_unit_ucodis'] * $lign_cmd['valeur']);
                $data['cmd_line_remise'] = 0.00;
                $data['cmd_line_date'] = date('Y-m-d h:i:s', time());
                $data['cmd_line_prix_unite'] = type_vente == 1 ? $Produit_unit_mesure['prod_unit_prix_unit_detaille'] : $Produit_unit_mesure['prod_unit_prix_unit_ucodis'];
                $data['cmd_line_prix_emballage'] =  $Produit_unit_mesure['prod_unit_prix_emballage'];
                $data['cmd_line_montant_emballage'] = $Produit_unit_mesure['prod_unit_prix_emballage'] * $lign_cmd['valeur'];
                $lign = new Lign_cmd();
                $res = $lign->save_lign_cmd_cli($data);
                $resultat[$key] = $res;
            }
            return $resultat;
        }
    }
}
