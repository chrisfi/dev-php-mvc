<?php

class Fournisseurs extends Controller
{

    public function index()
    {
        $this->loadModel('Fournisseur');
        $fournis = $this->Fournisseur->getAll();
        $this->render('index', compact('fournis'));
    }
    public function add()
    {

        if (isset($_POST['lib_fournisseur']) && isset($_POST['adress_fournisseur'])) {
            $data = [];
            $data['lib_fournisseur'] = $_POST['lib_fournisseur'];
            $data['adress_fournisseur'] = $_POST['adress_fournisseur'];
            $data['phon_fournisseur'] = $_POST['phon_fournisseur'];
            $data['id_fournisseur'] = $_POST['id_fournisseur'];
            $this->loadModel('Fournisseur');

            $status = $this->Fournisseur->save_fournisseur($data);
            // var_dump($status);die;
            $fournis = $this->Fournisseur->getAll();
            $this->render('index', compact('status', 'fournis'));
        } else {
            $this->render('add');
        }
    }

    public function edit()
    {
        if (isset($_POST['lib_fournisseur']) && isset($_POST['adress_fournisseur'])) {
            $data = [];
            $data['lib_fournisseur'] = $_POST['lib_fournisseur'];
            $data['adress_fournisseur'] = $_POST['adress_fournisseur'];
            $data['phon_fournisseur'] = $_POST['phon_fournisseur'];
            $data['id_fournisseur'] = $_POST['id_fournisseur'];
            $this->loadModel('Fournisseur');
            $status = $this->Fournisseur->update_fournisseur($data);
            $fournis = $this->Fournisseur->getAll();
            $this->render('index', compact('status', 'fournis'));
        } else {
            if ($_GET['id_fourni']) {
                $id_fourni = $_GET['id_fourni'];
                $this->loadModel('Fournisseur');
                $fournis = $this->Fournisseur->get_fournisseur($id_fourni);
                $this->render('add', compact('fournis'));
            }
        }
    }
}
