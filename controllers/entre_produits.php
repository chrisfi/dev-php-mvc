<?php

class Entre_produits extends Controller
{

    public $URL__AJAX;
    public function __construct()
    {
        require_once(rot . ds . 'util/fonction_util.php');
        $url_client_ajax = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
        $url = new Fonction_util();
        $URL__AJAX = $url->explode_url($url_client_ajax);
        $this->URL__AJAX = $URL__AJAX;
    }

    public function index()
    {
        $URL__AJAX = $this->URL__AJAX;
        $this->loadModel('entre_produit');
        $entre_produits = $this->entre_produit->getAll();
        $this->render('index', compact('entre_produits', 'URL__AJAX'));
    }

    public function add()
    {
        if (isset($_POST['date_entre']) && isset($_POST['qte_entre']) && isset($_POST['fournisseur_id']) && isset($_POST['id_produit']) && isset($_POST['unite_mesure_id']) && isset($_POST['prix_achat'])) {
            $data = [];
            require_once(rot . ds . 'util/fonction_util.php');
            $date = new Fonction_util();
            $now = $date->get_date_now__tiree('-');
            $now = explode(' ', $now);
            $Date_entre =  $date->convert_date_to_FR_DMY($_POST['date_entre'], '-') . ' ' . $now['1'];
            $Date_entre =  $date->convert_date_to_FR_DMY_heurs__slash($Date_entre, 'us', '-');
            $data['id_produit'] = $_POST['id_produit'];
            $data['unite_mesure_id'] = $_POST['unite_mesure_id'];
            $data['date_entre'] =  $Date_entre;
            $data['qte_entre'] = $_POST['qte_entre'];
            $data['prix_achat'] = $_POST['prix_achat'];
            $data['user_id'] = USER_ID;
            $data['fournisseur_id'] = $_POST['fournisseur_id'];
            $this->loadModel('Mesure_conversion');
            $this->loadModel('Produit');
            $this->loadModel('Entre_produit');
            $Produit = $this->Produit->get_produit_by_id($_POST['id_produit']);
            $produit_unit_mes_id = $Produit['produit_unit_mes_id'];
            $conversion = $this->Mesure_conversion->check_mesure_conversion_by_from_id_To_id($_POST['unite_mesure_id'], $produit_unit_mes_id);
            $nouveauQte = 0;
            if (!empty($conversion)) {
                $valeur = $conversion['value'];
                $nouveauQte = $_POST['qte_entre'] * $valeur;
                $data['qte_entre'] = $nouveauQte;
            } else {
                $data['qte_entre'] = $_POST['qte_entre'];
                $nouveauQte = $_POST['qte_entre'];
            }
            $Entre_produit = $this->Entre_produit->save_entre_produit($data);
            if ($Entre_produit) {
                $datas = [];
                $datas['qte_entre'] = $nouveauQte;
                $datas['prod_id'] = $_POST['id_produit'];
                $qte_entre = $this->Produit->update_Qte_produit_By_Entre($datas);
            }
        }
        $URL__AJAX = $this->URL__AJAX;
        $this->render('add', compact('URL__AJAX'));
    }

    public function ajax()
    {

        $action = isset($_POST['action']);
        switch ($action) {
            case 'ligne_unite_mesure_by_produit':
                $this->loadModel('Produit_unitme');
                require_once(rot . ds . 'helpers/produit_unite_mesureHelpers.php');
                $Produit_unitite_mesure = new produit_unite_mesureHelpers();
                $Produit_unitite_mesure = $Produit_unitite_mesure->get_select_option_unite($_POST['id_prod']);
                die;
                break;
            case 'get_unite_Qte_produit_Id':
                $this->loadModel('Produit_unitme');
                require_once(rot . ds . 'helpers/produit_unite_mesureHelpers.php');
                $Produit_unitite_mesure = new produit_unite_mesureHelpers();
                $Produit_unitite_mesure->get_unite_Qte_produit_Id($_POST['id_prod']);
                var_dump($_POST);
                die;
                break;
        }
    }

    public function get_unite_Qte_produit_Id()
    {
        $this->loadModel('Produit_unitme');
        require_once(rot . ds . 'helpers/produit_unite_mesureHelpers.php');
        $Produit_unitite_mesure = new produit_unite_mesureHelpers();
        $Produit_unitite_mesure->get_unite_Qte_produit_Id($_POST['id_prod']);
        die;
    }
}
