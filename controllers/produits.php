<?php

class Produits extends Controller
{
    private $produit_id;

    public function get_product($id)
    {
        $this->loadModel('produit');
        $produit = $this->produit->get_produit_by_id($id);
        return $produit;
    }
    /**
     * Cette méthode affiche la liste des articles
     *
     * @return void
     */
    public function index($mes = [])
    {
        $this->loadModel('produit');
        $produits = $this->produit->get_all_produit_join_unite_de_mesure();
        // $produits = $this->produit->getAll();
        // $produits = $this->produit->getAll_produit_non_delete_disponible();
        $this->render('index', compact('produits', 'mes'));
    }
    public function get_Produit_Ajax()
    {
        if (isset($_POST['get_produit_by_id'])) {
            require_once(rot . ds . 'helpers/produitHelpers.php');
            $form = new ProduitHelpers();
            $form::get_form_produit($_POST['get_produit_by_id']);
            die;
        }
        die;
    }

    public function gerer_Unite_Mesure_Produit_Ajax()
    {
        if (isset($_POST['id_product'])) {
            require_once(rot . ds . 'helpers/produitHelpers.php');
            $form = new ProduitHelpers();
            $form::gerer_Unite_mesure_product($_POST['id_product']);
            die;
        }
        die;
    }


    public function delete()
    {
        if (isset($_POST['id_product_to_delete'])) {
            $this->loadModel('produit');
            $prod = $this->produit->supprimer($_POST['id_product_to_delete']);
            echo json_encode($prod);
            die;
        }
        die;
    }

    public function edit()
    {

        if (isset($_POST['action'])) {
            $produit_is_emballage = 0;
            $produit_prix_emballage = 0;
            if (isset($_POST['produit_is_emballage'])) {
                $produit_is_emballage = 1;
                $produit_prix_emballage = $_POST['produit_prix_emballage'];
            }
            $produit_is_cageot = 0;
            $produit_cageot_id = 0;
            if (isset($_POST['produit_is_cageot'])) {
                $produit_is_cageot = 1;
                $produit_cageot_id = $_POST['produit_cageot_id'];
            }
            $PRODUIT_CODE = $_POST['PRODUIT_CODE'];
            $UNITEMESURE_ID = $_POST['unite_mesure_id'];
            $PRODUIT_GROUPE_ID = $_POST['groupe_id'];
            $famille_id = $_POST['famille_id'];
            $PRODUIT_CAG_ID = $produit_cageot_id;
            $produit_libelle = $_POST['produit_libelle'];
            $produit_prix_achat = $_POST['produit_prix_achat'];
            $PRODUIT_STK_MIN = $_POST['produit_stock_min'];
            $PRODUIT_ID = $_POST['PRODUIT_ID'];
            $UTILISATEUR_ID = $_POST['UTILISATEUR_ID'];
            $PRODUIT_PEMB = $produit_prix_emballage;
            $PRODUIT_DATE_UPD = date('Y-m-d h:i:s', time());
            $PRODUIT_ISEMBALL = $produit_is_emballage;
            $PRODUIT_ISCAG = $produit_is_cageot;
            $produit_unit_detaille = $_POST['produit_unit_detaille'];
            $produit_prix_unit_ucodis = $_POST['produit_prix_unit_ucodis'];
            $this->loadModel('Produit');
            $res = $this->Produit->add(
                $PRODUIT_ID,
                $UNITEMESURE_ID,
                $UTILISATEUR_ID,
                $PRODUIT_GROUPE_ID,
                $famille_id,
                $PRODUIT_CAG_ID,
                $PRODUIT_CODE,
                $produit_libelle,
                $PRODUIT_DATE_UPD,
                $produit_prix_achat,
                $produit_unit_detaille,
                $produit_prix_unit_ucodis,
                $PRODUIT_STK_MIN,
                $PRODUIT_ISEMBALL,
                $PRODUIT_PEMB,
                $PRODUIT_ISCAG
            );
            $mes = ['status' => $res, 'action' => 'edit'];
            $this->index($mes);
        } else {
            $this->render('add');
        }
    }

    public function add()
    {

        if (!empty($_POST)) {
            $numero_end = 'PR0';
            $produit_is_emballage = 0;
            $produit_prix_emballage = 0;
            $produit_is_cageot = 0;
            $produit_cageot_id = 0;
            if ($_POST['action'] == 'add_prod') {
              
                if (isset($_POST['produit_is_emballage'])) {
                    $produit_is_emballage = 1;
                    $produit_prix_emballage = $_POST['produit_prix_emballage'];
                }

                if (isset($_POST['produit_is_cageot'])) {
                    $produit_is_cageot = 1;
                    $produit_cageot_id = $_POST['produit_cageot_id'];
                }

                $this->loadModel('Produit');
                $prod = $this->Produit->getAll();


                if (empty($prod)) {
                    $produit_id = 0;
                } else {
                    foreach ($prod as $produit) {
                        $numero_end = $produit['produit_code'];
                        $produit_id = $produit['produit_id'];
                    }
                }
            }

            $numero_end++;
            $PRODUIT_CODE = $_POST['PRODUIT_CODE'] == 0 ? $numero_end : $_POST['PRODUIT_CODE'];
            $UNITEMESURE_ID = $_POST['unite_mesure_id'];
            $PRODUIT_GROUPE_ID = $_POST['groupe_id'];
            $famille_id = $_POST['famille_id'];
            $PRODUIT_CAG_ID = $produit_cageot_id;
            $produit_libelle = $_POST['produit_libelle'];
            $produit_prix_achat = $_POST['produit_prix_achat'];
            $PRODUIT_STK_MIN = $_POST['produit_stock_min'];
            $PRODUIT_ID = $_POST['PRODUIT_ID'];
            $UTILISATEUR_ID = $_POST['UTILISATEUR_ID'];
            $PRODUIT_PEMB = $produit_prix_emballage;
            $PRODUIT_DATE_UPD = date('Y-m-d h:i:s', time());
            $PRODUIT_ISEMBALL = $produit_is_emballage;
            $PRODUIT_ISCAG = $produit_is_cageot;
            $produit_unit_detaille = $_POST['produit_unit_detaille'];
            $produit_prix_unit_ucodis = $_POST['produit_prix_unit_ucodis'];
            $this->loadModel('Produit');
            $res = $this->Produit->add(
                $PRODUIT_ID,
                $UNITEMESURE_ID,
                $UTILISATEUR_ID,
                $PRODUIT_GROUPE_ID,
                $famille_id,
                $PRODUIT_CAG_ID,
                $PRODUIT_CODE,
                $produit_libelle,
                $PRODUIT_DATE_UPD,
                $produit_prix_achat,
                $produit_unit_detaille,
                $produit_prix_unit_ucodis,
                $PRODUIT_STK_MIN,
                $PRODUIT_ISEMBALL,
                $PRODUIT_PEMB,
                $PRODUIT_ISCAG
            );
            $mes = ['status' => $res, 'action' => 'add'];
            $this->index($mes);
        } else {
            $this->render('add');
        }
    }

    public function unite_mesure(){
        $this->loadModel('Produit_unitme');
        $produits = $this->Produit_unitme->get_unite_mesure_pagination(0,10);
        $totalPage= $this->Produit_unitme->count_nb_prod_with_unite_mesure();
        $this->render('unite_mesure_produit',compact('totalPage','produits'));
    }
}
