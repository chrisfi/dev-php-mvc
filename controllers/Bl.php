<?php
class Bl extends Controller
{
    public function index()

    {
        $this->loadModel('Somme_cmd_client');
        $Bl_valide = $this->Somme_cmd_client->get_all_cmd_Client_Valide();
        $this->render('index', compact('Bl_valide'));
    }

    public function BonL()
    {

        $action = isset($_POST['action']) ? $_POST['action'] : '';
        switch ($action) {
                /**get formulaire facture */
            case 'payer_facture':
                require_once(rot . ds . 'helpers/BlHelpers.php');
                $blhelpers = new BlHelpers();
                $blhelpers->get_Form_Payement($_POST['id_cmd']);
                die;
                break;
                /**save payer facture */
            case 'save_payement':
                require_once(rot . ds . 'util/fonction_util.php');
                $fonction_utile = new Fonction_util();
                $this->loadModel('Lign_payement');
                $data = [];
                $data['montant'] = $_POST['montant_A_payer'];
                $data['id_somme_cmd'] = $_POST['id_cmd_A_paye'];
                $data['date_paye'] = $fonction_utile->get_date_now__tiree('-');
                $paye = $this->Lign_payement->save_lign_payement($data);
                $this->loadModel('Somme_cmd_client');
                $Bl_valide = $this->Somme_cmd_client->get_all_cmd_Client_Valide();
                echo json_encode(['status' => $Bl_valide]);
                die;
                break;
        }
    }
}
