<?php
class Logins extends Controller
{
    // var_dump(md5('admin'));
    // $entre = md5('admin');
    // $val = '21232f297a57a5a743894a0e4a801fc3';

    public function __construct()
    {
        $this->index();
    }
    public function index()
    {
        // var_dump($_POST);
        if (isset($_POST['login']) && isset($_POST['password']) && isset($_POST['name'])) {
            $this->loadModel('Users');
            $user =  $this->Users->getUser($_POST['name'], $_POST['password']);
            if ($user) {
                $this->loadModel('user_access_vente');
                $type_access_vente_id = 0;
                $detaille = $_POST['detaille'];
                $ucodus = $_POST['ucodus'];
                if ($detaille == 'true') {
                    $type_access_vente_id = 1;
                }
                if ($ucodus == 'true') {
                    $type_access_vente_id = 2;
                }
            
                $user_access = $this->user_access_vente->check_user_access_vente($user['user_id'], $type_access_vente_id);
                if (!empty($user_access)) {
                    $_SESSION['login'] = $user['user_login'];
                    $_SESSION['user_id'] = $user['user_id'];
                    // $_SESSION['privilige_id'] = $user['role_id'];
                    $_SESSION['user_name'] = $user['user_name'];
                    $_SESSION['user_prenom'] = $user['user_prenom'];
                    $_SESSION['vente'] = $type_access_vente_id;

                    echo json_encode(["status" => 'ok', 'access' => 'ok','type_vente'=>$_SESSION['vente'], 'mes' => 'Succèss']);
                    die;
                } else {
                    echo json_encode(["status" => 'ko', 'access' => 'ko', 'mes' => "Vous n'avez pas le droit d'accèss"]);
                    die;
                }
            } else {
                echo json_encode(["status" => 'ko', 'access' => 'koo', 'mes' => "Mot de passe ou login incorrect"]);
                die;
            }
        } elseif (isset($_POST['logout'])) {
            session_destroy();
            echo json_encode(["status" => 'ok']);
            die;
        } else {
            require_once(rot . ds . 'views/login/login.php');
        }
    }
    public function get_id_user()
    {

        $id = $_SESSION['user_id'];
        return  $id;
    }
}
