<?php
class User_access_vente extends Model 
{
    public function __construct()
    {
        $this->table = "user_access_vente";
        $this->getConnection();
    }

    public function check_user_access_vente($id_user, $type_access_vente_id)
    {
        $stm = $this->_connexion->prepare("SELECT * FROM " . $this->table . " WHERE id_user=? and id_access= ?");
        $stm->bindValue(1, $id_user);
        $stm->bindValue(2, $type_access_vente_id);
        $stm->execute();
        $res = $stm->fetchAll();
        return !empty($res) ? $res : [];
    }

    public function save_user_access_Vente($data)
    {
        $sql = "INSERT INTO user_access_vente(id_access,id_user) VALUES (?,?)";
        $result = $this->_connexion->prepare($sql);
        $result->bindParam(1, $data['id_access']);
        $result->bindParam(2, $data['id_user']);
        $res = $result->execute();
        return $res;
    }


    public function get_All_Access_Vente_by_id_user($id)
    {
        $sql = " SELECT * FROM user_access_vente user_vente INNER JOIN acces_vente axe ON axe.id=user_vente.id_access INNER JOIN user user ON user.user_id=user_vente.id_user
        WHERE user_vente.id_user=? ";
        $query = $this->_connexion->prepare($sql);
        $query->bindParam(1, $id);
        $query->execute();
        return $query->fetchAll();
    }
}
