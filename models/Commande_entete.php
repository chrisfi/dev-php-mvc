<?php
class Commande_entete extends Model
{

    public function __construct()
    {
        $this->table = "commande_client";
        $this->getConnection();
    }
    public function get_all_CMD_by_type($id)
    {
        $sql = " SELECT * FROM commande_client where type_vente=?";
        $stm = $this->_connexion->prepare($sql);
        $stm->bindValue(1, $id);
        $stm->execute();
        $res = $stm->fetchAll();
        return !empty($res) ? $res : [];
    }



    public function save_cmd_client($cmd_num, $client_id = null)
    {
        $cmd_id = null;
        $cmd_date = date('Y-m-d h:i:s', time());
        $type_vente = type_vente;
        $cmd_valide = 0;
        $user_id = USER_ID;
        $sql = "INSERT INTO commande_client(cmd_id, cmd_num, client_id, user_id, cmd_date, cmd_valide,type_vente)
        VALUES (NULL,?,?,?,?,?,?)";
        $result = $this->_connexion->prepare($sql);

        $result->bindParam(1, $cmd_num);
        $result->bindParam(2, $client_id);
        $result->bindParam(3, $user_id);
        $result->bindParam(4, $cmd_date);
        $result->bindParam(5, $cmd_valide);
        $result->bindParam(6, $type_vente);
        $res = $result->execute();
        return $res;
    }

    public function get_cmd_end_save()
    {
        $sql = " SELECT * FROM commande_client ORDER BY commande_client.cmd_id DESC ";
        $query = $this->_connexion->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    public function get_cmd_tete_client_by_id_cmd($id)
    {
        $sql = "SELECT * FROM commande_client cmd_cli inner join client cli on cli.client_id=cmd_cli.client_id where cmd_cli.cmd_id=?";
        $query = $this->_connexion->prepare($sql);
        $query->bindParam(1, $id);
        $query->execute();
        return $query->fetch(PDO::FETCH_ASSOC);
    }

    public function get_cmd_tete_id_cmd($id)
    {
        $sql = "SELECT * FROM commande_client cmd_cli where cmd_cli.cmd_id=?";
        $query = $this->_connexion->prepare($sql);
        $query->bindParam(1, $id);
        $query->execute();
        return $query->fetch(PDO::FETCH_ASSOC);
    }

    public function update_valide($id, $value)
    {
        $sql = 'UPDATE commande_client SET cmd_valide = ? WHERE cmd_id= ? ';
        $stm = $this->_connexion->prepare($sql);
        $stm->bindValue(1, $value);
        $stm->bindValue(2, $id);
        $res = $stm->execute();
        return $res;
    }
}
