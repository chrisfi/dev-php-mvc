<?php
class User_role extends Model
{
    public function __construct()
    {
        $this->table = "user_role";
        $this->getConnection();
    }

    public function save_role_user($data)
    {
        $sql = "INSERT INTO user_role(user_id,role_id) VALUES (?,?)";
        $result = $this->_connexion->prepare($sql);
        $result->bindParam(1, $data['user_id']);
        $result->bindParam(2, $data['role_id']);
        $res = $result->execute();
        return $res;
    }

    public function get_All_role_by_id_user($id)
    {
        $sql = " SELECT * FROM user_role user_role INNER JOIN role rol ON rol.role_id=user_role.role_id INNER JOIN user user ON user.user_id=user_role.user_id  WHERE user.user_id=? ";
        $query = $this->_connexion->prepare($sql);
        $query->bindParam(1, $id);
        $query->execute();
        return $query->fetchAll();
    }
}
