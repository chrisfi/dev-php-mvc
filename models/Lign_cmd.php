<?php
class Lign_cmd extends Model
{

    public function __construct()
    {
        $this->table = "save_lign_cmd_prod";

        $this->getConnection();
    }
    public function save($prod_id, $unit_mes, $valeur)
    {
        // $valeur = $unit_mes == -1 ? (-1 * $valeur) : $valeur;

        $mes = [];
        $check = $this->check_lign_cmd($prod_id, $unit_mes);
        if (!empty($check)) {
            $sql = "UPDATE save_lign_cmd_prod SET valeur=? WHERE id=?";
            $result = $this->_connexion->prepare($sql);
            $result->bindParam(1, $valeur);
            $result->bindParam(2, $check[0]['id']);
            $res = $result->execute();
            $mes['status'] = $res;
            $mes['action'] = 'update';
            return $mes;
        } else {
            $sql = 'CALL save_lign_cm(?,?,?)';
            $result = $this->_connexion->prepare($sql);
            $result->bindParam(1, $prod_id);
            $result->bindParam(2, $unit_mes);
            $result->bindParam(3, $valeur);
            $res = $result->execute();
            $mes['status'] = $res;
            $mes['action'] = 'add';
            return $mes;
        }
    }

    public function check_lign_cmd($prod_id, $unit_mes)
    {
        $stm = $this->_connexion->prepare("SELECT * FROM " . $this->table . " WHERE prod_id= ? AND unit_mes=?");
        $stm->bindValue(1, $prod_id);
        $stm->bindValue(2, $unit_mes);
        $stm->execute();
        $res = $stm->fetchAll();
        return !empty($res) ? $res : [];
    }

    public function get_all_cmd_suplement()
    {
        $sql = "SELECT * FROM save_lign_cmd_prod";
        $query = $this->_connexion->prepare($sql);
        $query->execute();
        $res = $query->fetchAll();
        return !empty($res) ? $res : [];
    }

    public function delete_on_lign_by_id($id)
    {
        $stm = $this->_connexion->prepare("DELETE FROM " . $this->table . " WHERE id=?");
        $stm->bindValue(1, $id);
        return $stm->execute();
    }

    public function delete_all_lign_suplementaire()
    {
        $stm = $this->_connexion->prepare("TRUNCATE " . $this->table);
        return $stm->execute();
    }


    public function save_lign_cmd_cli($data)
    {
        var_dump($data);
        $sql = "INSERT INTO cmd_line(cmd_line_id, cmd_id, prod_id, unit_mes_id, cmd_line_qte, cmd_line_montant, cmd_line_remise, cmd_line_date, cmd_line_prix_unite, cmd_line_prix_emballage,cmd_line_montant_emballage)
         VALUES (NULL,?,?,?,?,?,?,?,?,?,?)";
        $result = $this->_connexion->prepare($sql);
        $result->bindParam(1, $data['cmd_id']);
        $result->bindParam(2, $data['prod_id']);
        $result->bindParam(3, $data['unit_mes_id']);
        $result->bindParam(4, $data['cmd_line_qte']);
        $result->bindParam(5, $data['cmd_line_montant']);
        $result->bindParam(6, $data['cmd_line_remise']);
        $result->bindParam(7, $data['cmd_line_date']);
        $result->bindParam(8, $data['cmd_line_prix_unite']);
        $result->bindParam(9, $data['cmd_line_prix_emballage']);
        $result->bindParam(10, $data['cmd_line_montant_emballage']);
        $res = $result->execute();
        return $res;
    }
}
