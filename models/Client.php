<?php
class Client extends Model
{
    private $CLIENT_ID;
    private $CLIENT_NUM;
    private $CLIENT_NOM;
    private $CLIENT_PRENOM;
    private $CLIENT_NIF;
    private $CLIENT_STAT;
    private $CLIENT_ADRESSE;
    private $CLIENT_DATE_UPD;
    public function __construct()
    {
        $this->table = "client";

        // Nous ouvrons la connexion à la base de données
        $this->getConnection();
    }
    public function save($CLIENT_ID = 0, $CLIENT_NUM, $CLIENT_NOM, $CLIENT_PRENOM, $CLIENT_NIF, $CLIENT_STAT, $CLIENT_ADRESSE, $CLIENT_DATE_UPD)
    {
        if ($CLIENT_ID != 0) {
            $sql = 'UPDATE client  SET client_nom=?,client_prenom=?,client_nif=?,client_stat=?,client_adress=?,client_date_update=? where client_id=?';
            $result = $this->_connexion->prepare($sql);
            $result->bindParam(1, $CLIENT_NOM);
            $result->bindParam(2, $CLIENT_PRENOM);
            $result->bindParam(3, $CLIENT_NIF);
            $result->bindParam(4, $CLIENT_STAT);
            $result->bindParam(5, $CLIENT_ADRESSE);
            $result->bindParam(6, $CLIENT_DATE_UPD);
            $result->bindParam(7, $CLIENT_ID);
            if ($result->execute()) {
                return 'success';
            } else {
                return 'error';
            }
        } else {
            $sql = 'INSERT INTO client(client_num,client_nom,client_prenom,client_nif,client_stat,client_adress,client_date_update) VALUE(?,?,?,?,?,?,?)';
            $result = $this->_connexion->prepare($sql);
            $result->bindParam(1, $CLIENT_NUM);
            $result->bindParam(2, $CLIENT_NOM);
            $result->bindParam(3, $CLIENT_PRENOM);
            $result->bindParam(4, $CLIENT_NIF);
            $result->bindParam(5, $CLIENT_STAT);
            $result->bindParam(6, $CLIENT_ADRESSE);
            $result->bindParam(7, $CLIENT_DATE_UPD);
            if ($result->execute()) {
                return 'success';
            } else {
                return 'error';
            }
        }
    }

    /**
     * Retourne un article en fonction de son slug
     *
     * @param string $slug
     * @return void
     */
    public function findBySlug(string $slug)
    {
        $sql = "SELECT * FROM " . $this->table . " WHERE `slug`='" . $slug . "'";
        $query = $this->_connexion->prepare($sql);
        $query->execute();
        return $query->fetch(PDO::FETCH_ASSOC);
    }

    public function Delete($id)
    {
        $stm = $this->_connexion->prepare("delete from " . $this->table . " where CLIENT_ID = ?");
        $stm->bindValue(1, $id);
        return $stm->execute();
    }
    public function get_cli($id)
    {
        $stm = $this->_connexion->prepare("SELECT * FROM " . $this->table . " WHERE client_id= ?");
        $stm->bindValue(1, $id);
        $stm->execute();
        return $stm->fetch(PDO::FETCH_ASSOC);
    }
    /**
     * Set the value of CLIENT_DATE_UPD
     *
     * @return  self
     */
    public function setCLIENT_DATE_UPD($CLIENT_DATE_UPD)
    {
        $this->CLIENT_DATE_UPD = $CLIENT_DATE_UPD;

        return $this;
    }

    /**
     * Get the value of CLIENT_ID
     */
    public function getCLIENT_ID()
    {
        return $this->CLIENT_ID;
    }

    /**
     * Set the value of CLIENT_ID
     *
     * @return  self
     */
    public function setCLIENT_ID($CLIENT_ID)
    {
        $this->CLIENT_ID = $CLIENT_ID;

        return $this;
    }

    /**
     * Get the value of CLIENT_NUM
     */
    public function getCLIENT_NUM()
    {
        return $this->CLIENT_NUM;
    }

    /**
     * Set the value of CLIENT_NUM
     *
     * @return  self
     */
    public function setCLIENT_NUM($CLIENT_NUM)
    {
        $this->CLIENT_NUM = $CLIENT_NUM;

        return $this;
    }

    /**
     * Get the value of CLIENT_NOM
     */
    public function getCLIENT_NOM()
    {
        return $this->CLIENT_NOM;
    }

    /**
     * Set the value of CLIENT_NOM
     *
     * @return  self
     */
    public function setCLIENT_NOM($CLIENT_NOM)
    {
        $this->CLIENT_NOM = $CLIENT_NOM;

        return $this;
    }

    /**
     * Get the value of CLIENT_PRENOM
     */
    public function getCLIENT_PRENOM()
    {
        return $this->CLIENT_PRENOM;
    }

    /**
     * Set the value of CLIENT_PRENOM
     *
     * @return  self
     */
    public function setCLIENT_PRENOM($CLIENT_PRENOM)
    {
        $this->CLIENT_PRENOM = $CLIENT_PRENOM;

        return $this;
    }

    /**
     * Get the value of CLIENT_NIF
     */
    public function getCLIENT_NIF()
    {
        return $this->CLIENT_NIF;
    }

    /**
     * Set the value of CLIENT_NIF
     *
     * @return  self
     */
    public function setCLIENT_NIF($CLIENT_NIF)
    {
        $this->CLIENT_NIF = $CLIENT_NIF;

        return $this;
    }

    /**
     * Get the value of CLIENT_STAT
     */
    public function getCLIENT_STAT()
    {
        return $this->CLIENT_STAT;
    }

    /**
     * Set the value of CLIENT_STAT
     *
     * @return  self
     */
    public function setCLIENT_STAT($CLIENT_STAT)
    {
        $this->CLIENT_STAT = $CLIENT_STAT;

        return $this;
    }

    /**
     * Get the value of CLIENT_ADRESSE
     */
    public function getCLIENT_ADRESSE()
    {
        return $this->CLIENT_ADRESSE;
    }

    /**
     * Set the value of CLIENT_ADRESSE
     *
     * @return  self
     */
    public function setCLIENT_ADRESSE($CLIENT_ADRESSE)
    {
        $this->CLIENT_ADRESSE = $CLIENT_ADRESSE;

        return $this;
    }
    /**
     * Get the value of CLIENT_DATE_UPD
     */
    public function getCLIENT_DATE_UPD()
    {
        return $this->CLIENT_DATE_UPD;
    }
}
