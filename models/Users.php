<?php
class Users extends Model
{
    public function __construct()
    {
        // Nous définissons la table par défaut de ce modèle
        $this->table = "user";

        // Nous ouvrons la connexion à la base de données
        $this->getConnection();
    }
    public function getUser($login, $pass)
    {
        // $sql = "SELECT * FROM " . $this->table . " WHERE `UTILISATEUR_LOGIN`='" . $login . "'";

        $sql = "SELECT * FROM user WHERE user_login='" . $login . "' AND user_pwd='" . $pass . "'";
        $query = $this->_connexion->prepare($sql);
        $query->execute();
        return $query->fetch(PDO::FETCH_ASSOC);
    }

    public function getUserById($id)
    {

        $sql = "SELECT * FROM user WHERE user_id=?";
        $query = $this->_connexion->prepare($sql);
        $query->bindParam(1, $id);
        $query->execute();
        return $query->fetch(PDO::FETCH_ASSOC);
    }

    public function save_user($data)
    {
        if ($data['user_id'] == 0) {
            $sql = "INSERT INTO user(user_name,user_prenom,user_login,user_pwd) VALUES (?,?,?,?)";
            $result = $this->_connexion->prepare($sql);
            $result->bindParam(1, $data['user_name']);
            $result->bindParam(2, $data['user_prenom']);
            $result->bindParam(3, $data['user_login']);
            $result->bindParam(4, $data['user_pwd']);
            $res = $result->execute();
            return $res;
        } else {
            $sql = "UPDATE user SET user_name=?,user_prenom=?,user_login=?,user_pwd=? WHERE user_id=?";
            $result = $this->_connexion->prepare($sql);
            $result->bindParam(1, $data['user_name']);
            $result->bindParam(2, $data['user_prenom']);
            $result->bindParam(3, $data['user_login']);
            $result->bindParam(4, $data['user_pwd']);
            $result->bindParam(5, $data['user_id']);
            $res = $result->execute();
            return $res;
        }
    }
}
