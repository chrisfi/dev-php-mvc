<?php
class Produit extends Model
{

    public function __construct()
    {
        $this->table = "produit";
        $this->getConnection();
    }
    public function add(
        $PRODUIT_ID,
        $UNITEMESURE_ID,
        $UTILISATEUR_ID,
        $PRODUIT_GROUPE_ID,
        $famille_id,
        $PRODUIT_CAG_ID,
        $PRODUIT_CODE,
        $produit_libelle,
        $PRODUIT_DATE_UPD,
        $produit_prix_achat,
        $produit_unit_detaille,
        $produit_prix_unit_ucodis,
        $PRODUIT_STK_MIN,
        $PRODUIT_ISEMBALL,
        $PRODUIT_PEMB,
        $PRODUIT_ISCAG
    ) {
        if ($PRODUIT_ID == 0) {
            $produit_stock_courent = 0;
            $sql = 'INSERT INTO produit( produit_unit_mes_id, user_id, famille_id, groupe_id, produit_cageot_id, produit_code,
             produit_libelle,produit_date_update, produit_prix_achat, produit_prix_unit_ucodis, produit_unit_detaille, produit_stock_min, 
             produit_stock_courent, produit_is_emballage, produit_prix_emballage, produit_is_cageot)VALUE(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';
            $result = $this->_connexion->prepare($sql);
            $result->bindParam(1, $UNITEMESURE_ID);
            $result->bindParam(2, $UTILISATEUR_ID);
            $result->bindParam(3, $famille_id);
            $result->bindParam(4, $PRODUIT_GROUPE_ID);
            $result->bindParam(5, $PRODUIT_CAG_ID);
            $result->bindParam(6, $PRODUIT_CODE);
            $result->bindParam(7, $produit_libelle);
            $result->bindParam(8, $PRODUIT_DATE_UPD);
            $result->bindParam(9, $produit_prix_achat);
            $result->bindParam(10, $produit_prix_unit_ucodis);
            $result->bindParam(11, $produit_unit_detaille);
            $result->bindParam(12, $PRODUIT_STK_MIN);
            $result->bindParam(13, $produit_stock_courent);
            $result->bindParam(14, $PRODUIT_ISEMBALL);
            $result->bindParam(15, $PRODUIT_PEMB);
            $result->bindParam(16, $PRODUIT_ISCAG);
            // var_dump( $sql);
            $val = $result->execute();
            return ['status' => $val];
        } else {
            $produit_stock_courent = 0;
            $sql = 'UPDATE produit SET produit_unit_mes_id=?,user_id=?,famille_id=?,groupe_id=?,produit_cageot_id=?,
            produit_code=?,produit_libelle=?,produit_date_update=?,produit_prix_achat=?,produit_prix_unit_ucodis=?, produit_unit_detaille=?,
            produit_stock_min=?,
            produit_is_emballage=b?,produit_prix_emballage=?,produit_is_cageot=? WHERE produit_id=?';
            $result = $this->_connexion->prepare($sql);
            $result->bindParam(1, $UNITEMESURE_ID);
            $result->bindParam(2, $UTILISATEUR_ID);
            $result->bindParam(3, $famille_id);
            $result->bindParam(4, $PRODUIT_GROUPE_ID);
            $result->bindParam(5, $PRODUIT_CAG_ID);
            $result->bindParam(6, $PRODUIT_CODE);
            $result->bindParam(7, $produit_libelle);
            $result->bindParam(8, $PRODUIT_DATE_UPD);
            $result->bindParam(9, $produit_prix_achat);
            $result->bindParam(10, $produit_prix_unit_ucodis);
            $result->bindParam(11, $produit_unit_detaille);
            $result->bindParam(12, $PRODUIT_STK_MIN);
            // $result->bindParam(13, $produit_stock_courent);
            $result->bindParam(13, $PRODUIT_ISEMBALL);
            $result->bindParam(14, $PRODUIT_PEMB);
            $result->bindParam(15, $PRODUIT_ISCAG);
            $result->bindParam(16, $PRODUIT_ID);
            $res = $result->execute();

            return $res;
        }
    }

    public function getAll_produit_non_delete_disponible()
    {
        $sql = 'SELECT * FROM  produit WHERE PRODUIT_ISDELETE !=1';
        $query = $this->_connexion->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    public function supprimer($id)
    {
        $delete = 1;
        $sql = 'UPDATE produit SET PRODUIT_ISDELETE=b? WHERE produit_is_emballage=?';
        $result = $this->_connexion->prepare($sql);
        $result->bindParam(1, $delete);
        $result->bindParam(2, $id);
        $res = $result->execute();
        return ['status' => $res];
    }
    public function Delete($id)
    {
        $stm = $this->_connexion->prepare("delete from " . $this->table . " where produit_is_emballage = ?");
        $stm->bindValue(1, $id);
        return $stm->execute();
    }
    public function get_cli($id)
    {
        $stm = $this->_connexion->prepare("SELECT * FROM " . $this->table . " WHERE produit_is_emballage= ?");
        $stm->bindValue(1, $id);
        $stm->execute();
        return $stm->fetch(PDO::FETCH_ASSOC);
    }

    public function get_produit_by_id($id)
    {
        $stm = $this->_connexion->prepare("SELECT * FROM " . $this->table . " WHERE produit_id= ?");
        $stm->bindValue(1, $id);
        $stm->execute();
        return $stm->fetch(PDO::FETCH_ASSOC);
    }
    public function get_produit_by_id_to_Mesure($id)
    {
        $stm = $this->_connexion->prepare("SELECT * FROM " . $this->table . " WHERE produit_is_emballage= ?");
        $stm->bindValue(1, $id);
        $stm->execute();
        return $stm->fetch(PDO::FETCH_ASSOC);
    }

    public function get_produit_by_PRODUIT_ISCAG_1($id)
    {

        $stm = $this->_connexion->prepare("SELECT * FROM " . $this->table . " WHERE produit_is_emballage= ?");
        $stm->bindValue(1, $id);
        $stm->execute();
        return $stm->fetchAll();
    }



    public function get_all_produit_join_unite_de_mesure()
    {
        $stm = $this->_connexion->prepare("SELECT * FROM " . $this->table . " produit INNER JOIN unite_mesure unite_mesure ON unite_mesure.unite_mesure_id= produit.produit_unit_mes_id ");
        $stm->execute();
        $total = $stm->fetchAll();
        return (!empty($total)) ? $total : [];
    }

    public function update_Qte_produit($datas = [])
    {
        $resultat = array(count($datas));
        foreach ($datas as $key => $cmd) {
            $sql = 'UPDATE produit SET produit_stock_courent = (produit_stock_courent-?)  WHERE produit_id=?';
            $stm = $this->_connexion->prepare($sql);
            $stm->bindValue(1, $cmd['cmd_line_qte']);
            $stm->bindValue(2, $cmd['prod_id']);
            $res = $stm->execute();
            $resultat[$key] = $res;
        }
        return $resultat;
    }

    public function update_Qte_produit_By_Entre($datas = [])
    {
        $sql = 'UPDATE produit SET produit_stock_courent = (produit_stock_courent+?)  WHERE produit_id=?';
        $stm = $this->_connexion->prepare($sql);
        $stm->bindValue(1, $datas['qte_entre']);
        $stm->bindValue(2, $datas['prod_id']);
        $res = $stm->execute();
        return $res;
    }
}
