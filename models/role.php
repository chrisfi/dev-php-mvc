<?php
class Role extends Model
{
    public function __construct()
    {
        $this->table = "role";
        $this->getConnection();
    }
    public function save($data)
    {
        $sql = "INSERT INTO role(role_libelle,role_is_admin) VALUES (?,?)";
        $result = $this->_connexion->prepare($sql);
        $result->bindParam(1, $data['role_libelle']);
        $result->bindParam(2, $data['role_is_admin']);
        $res = $result->execute();
        return $res;
    }
}
