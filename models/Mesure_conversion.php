<?php
class Mesure_conversion extends Model 
{
    function __construct()
    {
        $this->table = 'conversion';
        $this->getConnection();
    }
    public function add($MESURE_CONVERSION_ID, $unit_to_conver, $UNITEMESURE_TO_ID, $UTILISATEUR_ID, $CONVERSION_VALUE)
    {
        // if ($MESURE_CONVERSION_ID == 0) {
            $sql = 'INSERT INTO conversion(unit_to_conver,unite_mes,user_id,value)
          VALUE(:unit_to_conver,:unite_mes,:user_id,:value)';
            $result = $this->_connexion->prepare($sql);
            $val = $result->execute([
                ':unit_to_conver' => $unit_to_conver,
                ':unite_mes' => $UNITEMESURE_TO_ID,
                ':user_id' => $UTILISATEUR_ID,
                ':value' => $CONVERSION_VALUE
            ]);
            return $val;
        // }
        
        // else {
        //     $sql = 'UPDATE mesure_conversion set unit_to_conver=?,UNITEMESURE_TO_ID=?,UTILISATEUR_ID=?,CONVERSION_VALUE=? where conversion_id=?';
        //     $result = $this->_connexion->prepare($sql);
        //     $result->bindParam(1, $unit_to_conver);
        //     $result->bindParam(2, $UNITEMESURE_TO_ID);
        //     $result->bindParam(3, $UTILISATEUR_ID);
        //     $result->bindParam(4, $CONVERSION_VALUE);
        //     $result->bindParam(5, $MESURE_CONVERSION_ID);
        //     $res = $result->execute();
        // }
    }

    public function get_mesure_conversion_by_Id_From_and_Id_To($id_from, $id_to)
    {
        $stm = $this->_connexion->prepare("SELECT * FROM " . $this->table . " WHERE UNITEMESURE_FROM_ID= ? and UNITEMESURE_TO_ID=?");
        $stm->bindValue(1, $id_from);
        $stm->bindValue(2, $id_to);
        $stm->execute();
        return $stm->fetch(PDO::FETCH_ASSOC);
    }
    public function add_unite_mesure_by_product_id($MESURE_CONVERSION_ID, $UNITEMESURE_FROM_ID, $UNITEMESURE_TO_ID, $UTILISATEUR_ID, $CONVERSION_VALUE, $ID_PRODUIT)
    {
        require_once(rot . ds . 'models/produit_unitme.php');
        $produit_unitme = new Produit_unitme();
        $prod_unite = $produit_unitme->get_Unite_Mesure_by_id_produit_To_Id($ID_PRODUIT, $UNITEMESURE_TO_ID);

        require_once(rot . ds . 'models/Produit.php');
        $prod = new Produit();
        $produit = $prod->get_produit_by_id($ID_PRODUIT);
        // select @unite mesure by 2 ids
        $mesure = $this->check_mesure_conversion_by_from_id_To_id($UNITEMESURE_FROM_ID, $UNITEMESURE_TO_ID);
        // existe
        if (!empty($mesure)) {
            // valeur dans la base
            $CONVERSION_VALUE_exist = $mesure['CONVERSION_VALUE'];
            // existe on check aussi produit unite
            // produit unite existe
            if ($prod_unite != null) {
                return ['status' => 'existe', 'value' => 'unite de produit existe'];
            } else {
                $prix_U = $produit['PRODUIT_PU'] * $CONVERSION_VALUE_exist;
                $prix_A = $produit['PRODUIT_PA'] * $CONVERSION_VALUE_exist;
                $prix_Emballage = (int)($produit['PRODUIT_PEMB']) == 0 ? 0 : $produit['PRODUIT_PEMB'] * $CONVERSION_VALUE_exist;
                $PRO_UNIT_COMMENT = 'Créer lors de ajout des unites de mesures.';
                $PRODUIT_UNITMES_PA = $prix_A;
                $PRODUIT_UNITMES_PU = $prix_U;
                $PRODUIT_UNITMES_PEMB = $prix_Emballage;
                $res =  $produit_unitme->save($ID_PRODUIT, $UNITEMESURE_TO_ID, $UTILISATEUR_ID, $PRODUIT_UNITMES_PA, $PRODUIT_UNITMES_PU, $PRODUIT_UNITMES_PEMB, $PRO_UNIT_COMMENT);
                return ['status' => $res, 'value' => 'ajout unite de produit'];
            }
        } else {
            $res = $this->add($MESURE_CONVERSION_ID, $UNITEMESURE_FROM_ID, $UNITEMESURE_TO_ID, $UTILISATEUR_ID, $CONVERSION_VALUE);
            if ($res == true) {
                $prix_U = $produit['PRODUIT_PU'] * $CONVERSION_VALUE;
                $prix_A = $produit['PRODUIT_PA'] * $CONVERSION_VALUE;
                $prix_Emballage = (int)($produit['PRODUIT_PEMB']) == 0 ? 0 : $produit['PRODUIT_PEMB'] * $CONVERSION_VALUE;
                $PRO_UNIT_COMMENT = 'Créer lors de ajout des unites de mesures.';
                $PRODUIT_UNITMES_PA = $prix_U;
                $PRODUIT_UNITMES_PU = $prix_A;
                $PRODUIT_UNITMES_PEMB = $prix_Emballage;
                $res = $produit_unitme->save($ID_PRODUIT, $UNITEMESURE_TO_ID, $UTILISATEUR_ID, $PRODUIT_UNITMES_PA, $PRODUIT_UNITMES_PU, $PRODUIT_UNITMES_PEMB, $PRO_UNIT_COMMENT);
                return ['status' => $res, 'value' => 'ajout conversion de mesure et unite de produit'];
            } else {
                return ['status' => $res, 'value' => 'ajout conversion de mesure racontre un probleme et produit_unitme'];
            }
        }
    }
    public function check_mesure_conversion_by_from_id_To_id($unit_to_conver, $unite_mes)
    {
        $sql = 'SELECT * FROM conversion WHERE unit_to_conver =? AND unite_mes=?';
        $stm = $this->_connexion->prepare($sql);
        $stm->bindValue(1, $unit_to_conver);
        $stm->bindValue(2, $unite_mes);
        $stm->execute();
        $mesure = $stm->fetch(PDO::FETCH_ASSOC);
        return !empty($mesure) ? $mesure : [];
    }
}
