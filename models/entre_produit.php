<?php

class Entre_produit extends Model  
{
    public function __construct()
    {
        $this->table = "entre_produit";
        $this->getConnection();
    }

    public function save_entre_produit($data)
    {
       
        $sql = "INSERT INTO entre_produit(id_produit,unite_mesure_id,date_entre,qte_entre,prix_achat,user_id,fournisseur_id) VALUES (?,?,?,?,?,?,?)";
        $result = $this->_connexion->prepare($sql);
        $result->bindParam(1, $data['id_produit']);
        $result->bindParam(2, $data['unite_mesure_id']);
        $result->bindParam(3, $data['date_entre']);
        $result->bindParam(4, $data['qte_entre']);
        $result->bindParam(5, $data['prix_achat']);
        $result->bindParam(6, $data['user_id']);
        $result->bindParam(7, $data['fournisseur_id']);
        $res = $result->execute();
        return $res;
    }
}
