<?php

class Fournisseur extends Model
{
    public function __construct()
    {
        $this->table = "fournisseur";
        $this->getConnection();
    }

    public function save_fournisseur($data)
    {
     
        $sql = "INSERT INTO fournisseur(id_fournisseur,lib_fournisseur, adress_fournisseur, phon_fournisseur) VALUES (NULL,?,?,?)";
        $result = $this->_connexion->prepare($sql);
        $result->bindParam(1, $data['lib_fournisseur']);
        $result->bindParam(2, $data['adress_fournisseur']);
        $result->bindParam(3, $data['phon_fournisseur']);
        $res = $result->execute();
        return $res;
    }

    public function update_fournisseur($data)
    {
        $sql = " UPDATE fournisseur SET lib_fournisseur=?, adress_fournisseur=?, phon_fournisseur=? WHERE id_fournisseur=? ";
        $result = $this->_connexion->prepare($sql);
        $result->bindParam(1, $data['lib_fournisseur']);
        $result->bindParam(2, $data['adress_fournisseur']);
        $result->bindParam(3, $data['phon_fournisseur']);
        $result->bindParam(4, $data['id_fournisseur']);
        $res = $result->execute();
        return $res;
    }

    public function get_fournisseur($id)
    {
        $stm = $this->_connexion->prepare("SELECT * FROM " . $this->table . " WHERE id_fournisseur= ?");
        $stm->bindValue(1, $id);
        $stm->execute();
        return $stm->fetch(PDO::FETCH_ASSOC);
    }
}
