<?php
class Produit_unitme extends Model
{
    private $PRODUIT_UNITMES_ID;
    private $PRODUIT_ID;
    private $UNITEMESURE_ID;
    private $UTILISATEUR_ID;
    private $PRODUIT_UNITMES_PA;
    private $PRODUIT_UNITMES_PU;
    private $PRODUIT_UNITMES_PEMB;
    private $PRO_UNIT_COMMENT;
    public function __construct()
    {
        $this->table = "produit_unite_mesure";
        $this->getConnection();
    }

    public function save(
        $PRODUIT_ID,
        $UNITEMESURE_ID,
        $UTILISATEUR_ID,
        $PRODUIT_UNITMES_PA,
        $PRODUIT_UNITMES_PU,
        $PRODUIT_UNITMES_PEMB,
        $PRO_UNIT_COMMENT
    ) {
        $sql = 'INSERT INTO produit_unitmes(PRODUIT_ID,UNITEMESURE_ID,UTILISATEUR_ID,
        PRODUIT_UNITMES_PA,PRODUIT_UNITMES_PU,PRODUIT_UNITMES_PEMB,PRO_UNIT_COMMENT) 
        VALUE(:PRODUIT_ID,:UNITEMESURE_ID,:UTILISATEUR_ID,:PRODUIT_UNITMES_PA,
        :PRODUIT_UNITMES_PU,:PRODUIT_UNITMES_PEMB,:PRO_UNIT_COMMENT)';
        $result = $this->_connexion->prepare($sql);
        if ($val = $result->execute([
            ':PRODUIT_ID' => $PRODUIT_ID,
            ':UNITEMESURE_ID' => $UNITEMESURE_ID,
            ':UTILISATEUR_ID' => $UTILISATEUR_ID,
            ':PRODUIT_UNITMES_PA' => $PRODUIT_UNITMES_PA,
            ':PRODUIT_UNITMES_PU' => $PRODUIT_UNITMES_PU,
            ':PRODUIT_UNITMES_PEMB' => $PRODUIT_UNITMES_PEMB,
            ':PRO_UNIT_COMMENT' => $PRO_UNIT_COMMENT
        ])) {
            return ['status' => $val];
        }
        return  ['status' => $val];
    }

    public function get_Unite_Mesure_by_id_produit($id_produit)
    {
        $sql = "SELECT * FROM produit_unite_mesure produnit inner join unite_mesure unite on unite.unite_mesure_id =produnit.unit_mes_id where produnit.prod_id=?";
        // $stm = $this->_connexion->prepare("SELECT * FROM " . $this->table . " PROD INNER JOIN unite_mesure UNITE ON UNITE.unite_mesure_id=PROD.prod_unit_mes_id WHERE prod_id= ?");
        $stm = $this->_connexion->prepare($sql);
        $stm->bindValue(1, $id_produit);
        $stm->execute();
        $res = $stm->fetchAll();
        if (!empty($res)) {
            return $res;
        }
        return [];
    }
    /**
     * Méthode permettant d'obtenir tous les enregistrements de la table unite produit by idpro et to id
     *
     * @return array
     */
    public function get_Unite_Mesure_by_id_produit_To_Id($prod_id, $unit_mes_id)
    {
        $stm = $this->_connexion->prepare("SELECT * FROM " . $this->table . " WHERE prod_id= ? and unit_mes_id=?");
        $stm->bindValue(1, $prod_id);
        $stm->bindValue(2, $unit_mes_id);
        $stm->execute();
        $res = $stm->fetchAll();
        if (!empty($res)) {
            return $res[0];
        }
        return null;
    }

    public function count_nb_prod_with_unite_mesure()
    {
        $stm = $this->_connexion->prepare("SELECT * FROM " . $this->table);
        $stm->execute();
        return count($stm->fetchAll());
    }


    public function get_All_unite_mesure_by_produit_Id($id)
    {
        $stm = $this->_connexion->prepare(" SELECT * FROM produit_unite_mesure PROD_UNITE 
        INNER JOIN unite_mesure UNITE ON UNITE.unite_mesure_id=PROD_UNITE.unit_mes_id where PROD_UNITE.prod_id = ?");
        $stm->bindParam(1, $id);
        $stm->execute();
        $res = $stm->fetchAll();
        return !empty($res) ? $res : [];
    }


    public function get_unite_mesure_pagination($offset, $limit)
    {
        $stm = $this->_connexion->prepare("SELECT * FROM " . $this->table . " TAB INNER JOIN unite_mesure UM ON UM.unite_mesure_id= TAB.unit_mes_id  LIMIT $offset ,$limit");
        $stm->execute();
        $res = $stm->fetchAll();
        return !empty($res) ? $res : [];
    }

    public function chercher($search)
    {
        $query = " SELECT * FROM produit_unite_mesure pro_uni INNER JOIN unite_mesure uni_mes ON pro_uni.unit_mes_id=uni_mes.unite_mesure_id WHERE
         pro_uni.prod_libell LIKE '%" . $search . "%' OR uni_mes.unite_mesure_code LIKE '%" . $search . "%' OR uni_mes.unite_mesure_libelle LIKE '%" . $search . "%'
        OR uni_mes.unite_mesure_description LIKE '%" . $search . "%' ";
        $stm = $this->_connexion->prepare($query);
        $stm->execute();
        $res = $stm->fetchAll();
        return !empty($res) ? $res : [];
    }

    public function save_produit_unite_mesure(
        $prod_unit_mes_id,
        $prod_id,
        $prod_libell,
        $unit_mes_id,
        $user_id,
        $prod_unit_prix_achat,
        $prod_unit_prix_unit_ucodis,
        $prod_unit_prix_unit_detaille,
        $prod_comment,
        $prod_unit_prix_emballage
    ) {
        $sql = "INSERT INTO produit_unite_mesure(prod_unit_mes_id, prod_id,prod_libell, unit_mes_id, user_id,
         prod_unit_prix_achat, prod_unit_prix_unit_ucodis, prod_unit_prix_unit_detaille, prod_comment, prod_unit_prix_emballage)
        VALUES (NULL,?,?,?,?,?,?,?,?,?)";
        $result = $this->_connexion->prepare($sql);
        $result->bindParam(1, $prod_id);
        $result->bindParam(2, $prod_libell);
        $result->bindParam(3, $unit_mes_id);
        $result->bindParam(4, $user_id);
        $result->bindParam(5, $prod_unit_prix_achat);
        $result->bindParam(6, $prod_unit_prix_unit_ucodis);
        $result->bindParam(7, $prod_unit_prix_unit_detaille);
        $result->bindParam(8, $prod_comment);
        $result->bindParam(9, $prod_unit_prix_emballage);
        // var_dump( $sql);
        return $val = $result->execute();
    }
    // Créer lors de la création du produit

    public function get_all_produit_distict()
    {
        $query = "SELECT DISTINCT prod_id,prod_libell FROM produit_unite_mesure";
        $stm = $this->_connexion->prepare($query);
        $stm->execute();
        $res = $stm->fetchAll();
        return !empty($res) ? $res : [];
    }
}
