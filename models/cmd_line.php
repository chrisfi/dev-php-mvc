<?php
class cmd_line extends Model
{
    public function __construct()
    {
        $this->table = "cmd_line";
        $this->getConnection();
    }

    public function get_cmd_cli_by_id($id)
    {
        $sql = " SELECT * FROM " . $this->table . " where cmd_id=?";
        $stm = $this->_connexion->prepare($sql);
        $stm->bindValue(1, $id);
        $stm->execute();
        $res = $stm->fetchAll();
        return !empty($res) ? $res : [];
    }

    public function get_somme_montant_by_id_Cmd($id)
    {
        $sql = "CALL get_somm_cm_by_id(?)";
        $stm = $this->_connexion->prepare($sql);
        $stm->bindValue(1, $id);
        $stm->execute();
        $res = $stm->fetch();
        return $res;
    }
}
