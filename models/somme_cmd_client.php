<?php
class Somme_cmd_client extends Model
{
    public function __construct()
    {
        $this->table = "somme_cmd_client";
        $this->getConnection();
    }

    public function save_somme_cmd_client($data = [])
    {
        $sql = "INSERT INTO somme_cmd_client(id_cmd, somme,reste,date_validation,user) VALUES (?,?,?,?,?)";
        $result = $this->_connexion->prepare($sql);
        $result->bindParam(1, $data['id_cmd']);
        $result->bindParam(2, $data['somme']);
        $result->bindParam(3, $data['reste']);
        $result->bindParam(4, $data['date_validation']);
        $result->bindParam(5, $data['user']);
        $res = $result->execute();
        return $res;
    }

    public function get_all_cmd_Client_Valide()
    {
        $sql = 'SELECT * FROM somme_cmd_client somme_cmd INNER JOIN commande_client cmd_cli ON cmd_cli.cmd_id=somme_cmd.id_cmd';
        $query = $this->_connexion->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    public function get_somme_cmd_by_id($id)
    {
        $sql = 'SELECT * FROM somme_cmd_client somme_cmd where somme_cmd.id_cmd=?';
        $query = $this->_connexion->prepare($sql);
        $query->bindParam(1, $id);
        $query->execute();
        return $query->fetch();
    
    }
}
