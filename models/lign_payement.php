<?php
class Lign_payement extends Model
{
    public function __construct()
    {
        $this->table = "lign_payement";
        $this->getConnection();
    }

    public function save_lign_payement($data = [])
    {
        $user_saisi = USER_ID;
        $sql = "INSERT INTO lign_payement(id_somme_cmd, montant,date_paye,user_saisi) VALUES (?,?,?,?)";
        $result = $this->_connexion->prepare($sql);
        $result->bindParam(1, $data['id_somme_cmd']);
        $result->bindParam(2, $data['montant']);
        $result->bindParam(3, $data['date_paye']);
        $result->bindParam(4, $user_saisi);
        $res = $result->execute();
        return $res;
    }
}
