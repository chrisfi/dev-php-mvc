<?php
class Unitemesure extends Model
{
    private $UNITEMESURE_ID;
    private $UNITEMESURECODE;
    private $UNITEMESURELIB;
    private $UNITEMESURE_DESC;
    public $mes=[];
    public function __construct()
    {
        $this->table = "unite_mesure";
        $this->getConnection();
         $this->mes=[];
    }
    public function get_all()
    {
        return $this->getAll();
    }
    public function save($UNITEMESURE_ID, $UNITEMESURECODE, $UNITEMESURELIB, $UNITEMESURE_DESC, $UNIT_CAG)
    {
        if ($UNITEMESURE_ID == 0) {
            $sql = 'INSERT INTO unite_mesure(unite_mesure_code,unite_mesure_libelle,unite_mesure_description,unite_mesure_cageot)
            VALUE(?,?,?,?)';
            $result = $this->_connexion->prepare($sql);
            $result->bindParam(1, $UNITEMESURECODE);
            $result->bindParam(2, $UNITEMESURELIB);
            $result->bindParam(3, $UNITEMESURE_DESC);
            $result->bindParam(4, $UNIT_CAG);
            $res = $result->execute();
            $this->mes['value']='ajout';
            return ['status' => $res, 'action' => 'add', 'UNIT_CAG' => $UNIT_CAG];
        } else {
            $sql = 'UPDATE unite_mesure SET unite_mesure_code=?,unite_mesure_libelle=?,unite_mesure_description=?,unite_mesure_cageot=? WHERE unite_mesure_id=?';
            $result = $this->_connexion->prepare($sql);
            $result->bindParam(1, $UNITEMESURECODE);
            $result->bindParam(2, $UNITEMESURELIB);
            $result->bindParam(3, $UNITEMESURE_DESC);
            $result->bindParam(4, $UNIT_CAG);
            $result->bindParam(5, $UNITEMESURE_ID);
            $res = $result->execute();
            return ['status' => $res, 'action' => 'update'];
        }
    }

    public function get_Unite_Mesure_by_id($id)
    {
        $stm = $this->_connexion->prepare("SELECT * FROM " . $this->table . " WHERE unite_mesure_id= ?");
        $stm->bindValue(1, $id);
        $stm->execute();
        return $stm->fetch(PDO::FETCH_ASSOC);
    }

    public function delete($id){
        $stm = $this->_connexion->prepare("DELETE * FROM " . $this->table . " WHERE unite_mesure_id= ?");
        $stm->bindValue(1, $id);
       $res= $stm->execute();
       return $res;
    }
}
